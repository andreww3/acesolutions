 // Include Gulp & grab our packages
var gulp =   require('gulp'),
             watch = require('gulp-watch'),
             concat = require('gulp-concat'),
             sass = require('gulp-sass'),
             sourcemaps = require('gulp-sourcemaps');

var install = require("gulp-install");
var elixir = require('laravel-elixir');
var rename = require('gulp-rename');
/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

// define the default task and add the watch task to it
gulp.task('watch', ['watch']);


gulp.task("copyfiles", function() {

  // Copy clean-blog less files
  gulp.src("vendor/bower_components/clean-blog/less/**")
      .pipe(gulp.dest("resources/assets/less/clean-blog"));
      
  // -> Copy Bootstrap Fontawesome
  gulp.src("vendor/bower_components/font-awesome/less/**")
      .pipe(gulp.dest("resources/assets/less/fontawesome"));
  gulp.src("vendor/bower_components/font-awesome/fonts/**")
      .pipe(gulp.dest("public/assets/fonts"));


  // Copy datatables
  var dtDir = 'vendor/bower_components/datatables-plugins/integration/';

  gulp.src("vendor/bower_components/datatables/media/js/jquery.dataTables.js")
      .pipe(gulp.dest('resources/assets/js/'));

  gulp.src(dtDir + 'bootstrap/3/dataTables.bootstrap.css')
      .pipe(rename('dataTables.bootstrap.less'))
      .pipe(gulp.dest('resources/assets/less/others/'));

  gulp.src(dtDir + 'bootstrap/3/dataTables.bootstrap.js')
      .pipe(gulp.dest('resources/assets/js/'));

  
  // Copy selectize
  gulp.src("vendor/bower_components/selectize/dist/css/**")
      .pipe(gulp.dest("public/assets/js/selectize/css"));

  gulp.src("vendor/bower_components/selectize/dist/js/standalone/selectize.min.js")
      .pipe(gulp.dest("public/assets/js/selectize/"));

  
  // Copy pickadate
  gulp.src("vendor/bower_components/pickadate/lib/compressed/themes/**")
      .pipe(gulp.dest("public/assets/js/pickadate/themes/"));

  gulp.src("vendor/bower_components/pickadate/lib/compressed/picker.js")
      .pipe(gulp.dest("public/assets/js/pickadate/"));

  gulp.src("vendor/bower_components/pickadate/lib/compressed/picker.date.js")
      .pipe(gulp.dest("public/assets/js/pickadate/"));

  gulp.src("vendor/bower_components/pickadate/lib/compressed/picker.time.js")
      .pipe(gulp.dest("public/assets/js/pickadate/"));

});


/**
 * Default gulp is to run this elixir stuff
 */

gulp.task('js-bundle', elixir(function(mix) {

  // Combine scripts
  mix.scripts([
      './vendor/bower_components/jquery/dist/jquery.js',
      './vendor/bower_components/bootstrap/dist/js/bootstrap.js',
      './vendor/bower_components/angular/angular.js',
      './vendor/bower_components/angular-route/angular-route.js',
      './vendor/bower_components/angular-bootstrap/ui-bootstrap.js',
      './vendor/bower_components/angular-bootstrap/ui-bootstrap-tpls.js',
      './vendor/bower_components/angular-file-upload/dist/angular-file-upload.js',
      './vendor/bower_components/angular-sanitize/angular-sanitize.js',
      './vendor/bower_components/modernizr/modernizr.js',
      './resources/assets/js/jquery.dataTables.js',
      './resources/assets/js/dataTables.bootstrap.js'
    ],
    'public/assets/javascript/bundle.js'
  );

  // Combine scripts
  mix.scripts([
      './vendor/bower_components/datatables/media/js/jquery.dataTables.js',
      './vendor/bower_components/datatables/media/js/dataTables.bootstrap.js',
    ],
    'public/assets/javascript/blogBundle.js'
  );
}));


gulp.task('angular', elixir(function(mix) {
  
  // Combine scripts
  mix.scripts([
      './resources/assets/js/app.js',
      './resources/assets/js/directives/*.js',
      './resources/assets/js/services/*.js',
      './resources/assets/js/controllers/*.js',
      './resources/assets/js/filters/*.js'

    ],
    'public/assets/javascript/app.js'
  );

}));


gulp.task('styles', elixir(function(mix) {
  
  // Combine scripts
  mix.stylesIn('resources/assets/css', 'public/assets/style/main.css');

}));


/**
 * Default gulp is to run this elixir stuff
 */
gulp.task('copyfonts', function() {
    gulp.src('./vendor/bower_components/bootstrap/fonts/**/*.{eof,svg,ttf,woff,woff2}')
    .pipe(gulp.dest('./public/assets/fonts/'));  
});


gulp.task('css-bundle', elixir(function(mix) {
    gulp.src('./vendor/bower_components/bootstrap/**/*.css')
    .pipe(sourcemaps.init())
      .pipe(concat('bundle.css'))
      //only uglify if gulp is ran with '--type production'
      //.pipe(gutil.env.type === 'production' ? uglify() : gutil.noop()) 
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('public/assets/style'));

}));


