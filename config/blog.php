<?php
return [
    'title' => 'eSolutions Blog',
	'description' => 'Your learning tool on how to successfully use Internet Marketing. Here you will find information about how to use the online media to reach your target audience and grow your conversion rates.',
	'posts_per_page' => 6,
  	'uploads' => [
	    'storage' => 's3',
	    'webpath' => 'https://s3-us-west-2.amazonaws.com/acesolutions'
	    ]
];