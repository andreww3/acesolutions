<?php

namespace App;

use App\Client;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;


class User extends Model implements AuthenticatableContract,
    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword, SoftDeletes;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'role', 'password', 'phone', 'city', 'country'];

    /**
      * Get the roles a user has
      */   
    
    public function client()
    {
        return $this->belongsTo('App\Client', 'client_users');
    }    

    /* *
      * Get key in array with corresponding value
     *  @return int
    */
    private function getIdInArray($array, $term)
    {
        foreach ($array as $key => $value) {
            if ($value == $term) {
                 return $key;
            }
        }

        throw new UnexpectedValueException;
    }

    protected $hidden = [
        'password', 'remember_token',
    ];
}
