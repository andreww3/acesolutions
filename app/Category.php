<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
  	
  	protected $fillable = [
	    'title', 'subtitle', 'page_image', 'meta_description', 
	    'reverse_direction',
	  ];

	/* *
	  * The one-to-many relationship between categories and posts.
	 *
	* @return BelongsToMany
	*/
	public function posts()
	{
	    return $this->hasMany('App\Post', 'post_category_pivot');
	}


	/* *
	  * Add any tags needed from the list
	 *
    * @param array $tags List of tags to check/add
	*/
  	public static function addCategory(array $tags)
	{
	    if (count($tags) === 0) {
	      return;
		}

		$found = static::whereIn('tag', $tags)->lists('tag')->all();

		foreach (array_diff($tags, $found) as $tag) {
		    static::create([
		        'tag' => $tag,
		        'title' => $tag,
		        'subtitle' => 'Subtitle for '.$tag,
		        'page_image' => '',
		        'meta_description' => '',
		        'reverse_direction' => false,
		    ]);
		}
	  }
}
