<?php

namespace App;

use App\BaseModel;

use Illuminate\Database\Eloquent\SoftDeletes;

class Client extends BaseModel
{

    protected $fillable = ['company', 'description', 'website'];

    public function user()
    {
        return $this->belongsToMany('App\User', 'client_users');
    }
 
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
    
    public function tasks()
    {
        return $this->hasMany('App\Task');
    }
}
