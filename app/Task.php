<?php

namespace App;

use App\BaseModel;

use Illuminate\Database\Eloquent\SoftDeletes;

class Task extends BaseModel
{
    use SoftDeletes;

    protected $fillable = ['title', 'type', 'description', 'initiator_id', 'assignee_id', 'deadline', 'hours', 'status'];
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    public function initiator()
    {
        return $this->belongsTo('App\User');
    }
    
}
