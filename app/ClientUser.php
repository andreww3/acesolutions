<?php

namespace App;


class ClientUser extends BaseModel
{
    protected $fillable = ['client_id', 'user_id', 'role'];

    public function client()
    {
        return $this->belongsTo('App\Client');
    }

    public function user()
    {
    	return $this->hasMany('App\User', 'client_users');
    }

}
