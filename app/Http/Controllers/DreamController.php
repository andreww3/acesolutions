<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Dream;

class DreamController extends Controller
{
    
    public function showAll ()
    {
		return Dream::all();
    }

    public function showLatest () 
    {
        $dreams = Dream::orderBy('id', 'desc')->take(6)->get();

        return $dreams;
    }

    public function showRandom ()
    {
        $dreams = Dream::all()->random(3);
        
        return $dreams;
    }

    public function add (Request $request)
    {

      $input = $request->all();

        $dream = new Dream;

        $dream->fill([
            'dreamer' => $input['dreamer'],
            'dream' => $input['dream']
        ]);

        $dream->save();

        return 'dream added';

    }

    public function delete (Request $request)
    {

        $input = $request->all();

        if ( ! $dream = Dream::find($input['id']) )
            die('Dream not found');
    
        $dream->delete();

    }
}
