<?php

namespace App\Http\Controllers\Auth;

use Auth;
use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
/*use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

namespace App\Http\Controllers\Auth;

use Auth;
use App\User;
use App\Client;
use App\ClientUser;
use Validator;
use Illuminate\Support\Facades\Input;
//use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Auth\Authenticatable;
use Illuminate\Http\RedirectResponse;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
*/
class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use  ThrottlesLogins;

    /* Where users get redirected after login */
    protected $redirectTo = '/';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware($this->guestMiddleware(), ['except' => 'logout']);
    }

    protected function authenticated()
    {
        return redirect('dashboard');        
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        $user = new User;
        
        if ($data['role'] == 'cMindz') {
            $user->name = $data['name'];
            $user->email = $data['email'];
            $user->role = $data['role'];
            $user->password = bcrypt($data['password']); 
            
            $user->save();           
        
        } elseif ($data['role'] == 'client') {

            $user->name = $data['name'];
            $user->email = $data['email'];
            $user->role = $data['role'];
            $user->password = bcrypt($data['password']);             
            $user->phone = $data['phone'];
            $user->city = $data['city'];
            $user->country = $data['country'];
            $user->save();

            $client = new Client;
            $client->company = $data['company'];
            $client->description = $data['description'];
            $client->website = $data['website'];
            $client->save();

            $relation = ClientUser::create([
                'client_id' => $client->id,
                'user_id' =>$user->id
            ]);
        }

        return $user;

    }
}
