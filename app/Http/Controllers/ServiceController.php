<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Service;

use File;

class ServiceController extends Controller
{

    public function show () 
    {
		return Service::all();
    }  	

    public function uploadIcon(Request $request) {


        $filename = $_FILES[ 'file' ][ 'name' ];
        $tempPath = $_FILES[ 'file' ][ 'tmp_name' ];

        $destinationPath = public_path().'/assets/img/services';
        $uploadPath = $destinationPath . DIRECTORY_SEPARATOR .$filename;

        move_uploaded_file( $tempPath, $uploadPath );
        
    }

    public function add(Request $request)
    {

      $input = $request->all();

        if (!$serviceExists = Service::find($input['name'])) {

            $service = new Service;

            $service->fill([
                'name' => $input['name'],
                'description' => $input['description'],
                'description' => $input['description'],
                'icon' => $input['icon']
            ]);
            
            //$project->user()->associate($user);
            
            $service->save();

            return redirect('/dashboard');
        
        } else {

            die('Service Exists');
        }
        
    }

    public function edit (Request $request)
    {

      $input = $request->all();

        if ( ! $service = Service::find($input['id']) )
            die('Service not found');

        $service->name = $input['name'];
        $service->description = $input['description'];
        $service->icon = $input['icon'];

		// Tralalalala
        
        $service->save();

    }

    public function removeIcon (Request $request)
    {

      $input = $request->all();

        if ( ! $service = Service::find($input['id']) )
            die('Service not found');

        $filename = $input['icon'];

        if (File::exists($filename)) {
            File::delete($filename);
            $service->icon = '';
        } 

        // Tralalalala
        
        $service->save();

    }

    public function delete (Request $request)
    {

        $input = $request->all();

        if ( ! $service = Service::find($input['id']) )
            die('Service not found');
    
        $service->delete();

    }
}
