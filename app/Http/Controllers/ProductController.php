<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Product;

use File;

class ProductController extends Controller
{

    public function showAll ()
    {
		return Product::orderBy('created_at', 'desc')->get();
    }  	

    public function getNecklaces()
    {
        return Product::where('type', 'necklace')->orderBy('created_at', 'desc')->get();
    }   

    public function uploadImage (Request $request) {

        $filename = $_FILES[ 'file' ][ 'name' ];
        $tempPath = $_FILES[ 'file' ][ 'tmp_name' ];

        $destinationPath = public_path().'/assets/img/products/';
        $uploadPath = $destinationPath . DIRECTORY_SEPARATOR .$filename;

        move_uploaded_file( $tempPath, $uploadPath );
        
    }

    public function add (Request $request)
    {

      	$input = $request->all();
        $image = $input['image'];
        if (!File::exists($image)) {
           $image = '';  
        }

        $desc = '';
        if (!empty($input['description'])) $desc = $input['description'];

        $product = new Product;
        $product->fill([
            'name' => $input['name'],
            'type' => $input['type'],
            'image' => $image,
            'price' => $input['price'],
            'description' => $desc
        ]);
        $product->save();
    }

    public function edit (Request $request)
    {

      $input = $request->all();

        if ( ! $product = Product::find($input['id']) )
            die('Product not found');

        $image = $input['image'];
        if (!File::exists($image)) {
           $image = '';  
        } 
        $product->image = $image;

        if (!empty($input['type']))
            $product->type = $input['type'];

        if ($input['name'])
            $product->name = $input['name'];

        if ($input['description'])
            $product->description = $input['description'];

        if ($input['price'])
            $product->price = $input['price'];

        $product->save();

    }


    public function removeImage (Request $request)
    {

      $input = $request->all();

        if ( ! $product = Product::find($input['id']) )
            die('Product not found');

        $filename = $input['image'];

        if (File::exists($filename)) {
            File::delete($filename);
            $product->image = '';
        }
        
        $product->save();

    }

    public function delete (Request $request)
    {

        $input = $request->all();

        if ( ! $product = Product::find($input['id']) )
            die('Product not found');
        
        if (File::exists($product->image)) {
            File::delete($product->image);
        }

        $product->delete();

    }

}
