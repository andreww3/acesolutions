<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Post;
use App\Category;
use Carbon\Carbon;

use App\Http\Requests;

class BlogController extends Controller
{
    public function index()
    {
        /* * Using angular instead
        $posts = Post::where('published_at', '<=', Carbon::now())
            ->orderBy('published_at', 'desc')
            ->paginate(config('blog.posts_per_page'));

        return view('blog.index', compact('posts'));
        */
        return view('blog.index');
    }

    public function showPost($slug)
    {
        $post = Post::whereSlug($slug)->firstOrFail();

        return view('blog.post')->withPost($post);
    }

    public function show6 ()
    {
        return Post::orderBy('published_at', 'desc')->limit(6)->get();
    }  

    public function showAll ()
    {
		return Post::with('tags')->orderBy('published_at', 'desc')->get();
    }  	

    public function showCategories ()
    {
        return Category::all();
    } 
}
