<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Project;

use File;

class ProjectController extends Controller
{
    public function show6 ()
    {
        return Project::orderBy('delivered_at', 'desc')->limit(6)->get();
    }  

    public function showAll ()
    {
		return Project::orderBy('delivered_at', 'desc')->get();
    }  	

    public function uploadImage (Request $request) {

        $filename = $_FILES[ 'file' ][ 'name' ];
        $tempPath = $_FILES[ 'file' ][ 'tmp_name' ];

        $destinationPath = public_path().'/assets/img/projects/';
        $uploadPath = $destinationPath . DIRECTORY_SEPARATOR .$filename;

        move_uploaded_file( $tempPath, $uploadPath );
        
    }

    public function add (Request $request)
    {

      $input = $request->all();

        $project = new Project;
        $desc = '';
        if (!empty($input['description'])) $desc = $input['description'];
        $project->fill([
            'title' => $input['title'],
            'link' => $input['link'],
            'image' => $input['image'],
            'summary' => $input['summary'],
            'description' => $desc,
            'started_at' => $input['started_at'],
            'delivered_at' => $input['delivered_at']
        ]);

        $project->save();

        return redirect('/dashboard/projects');

    }

    public function edit (Request $request)
    {

      $input = $request->all();

        if ( ! $project = Project::find($input['id']) )
            die('Project not found');

        $project->title = $input['title'];
        $project->summary = $input['summary'];

        if ($input['image'])
            $project->image = $input['image'];

        if ($input['link'])
            $project->link = $input['link'];

        if ($input['description'])
            $project->description = $input['description'];

        if ($input['started_at'])
            $project->started_at = $input['started_at'];

        if ($input['delivered_at'])
            $project->delivered_at = $input['delivered_at'];

        $project->save();

        return redirect('/dashboard/projects');

    }


    public function removeImage (Request $request)
    {

      $input = $request->all();

        if ( ! $project = Project::find($input['id']) )
            die('Service not found');

        $filename = $input['image'];

        if (File::exists($filename)) {
            File::delete($filename);
            $project->image = '';
        } 
        
        $project->save();

    }

    public function delete (Request $request)
    {

        $input = $request->all();

        if ( ! $project = Project::find($input['id']) )
            die('Project not found');
    
        $project->delete();

    }

}
