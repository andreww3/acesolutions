<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use File;
use App\Role;

use App\Http\Requests;
use App\Http\Controllers\Controller;


class UserController extends Controller
{
    public function showAll() 
    {
		return User::all();
    }  	

    public function showClients() 
    {
        return User::where('role','=','client')->get();
    }  

    public function showMembers() 
    {
        return User::where('role','=','author')->orWhere('role','=','admin')->get();
    } 

    public function add(Request $request)
    {

        $input = $request->all();

        if ( sizeof(User::where('email','=',$input['email'])->get()) > 0 )
            die('There is already a user with this email');

        $user = new User;

        $user->fill([
            'name' => $input['name'],
            'email' => $input['email'],
            'role' => $input['role'],
        ]);

        $user->makeMember($input['role']);
        
        //$project->user()->associate($user);
        
        if ( $user->save() )
         return redirect('dashboard/users');
        
        
    }


    public function update (Request $request)
    {

        $input = $request->all();

        if ( ! $user = User::find($input['id']) )
            die('User not found');

        $user->name = $input['name'];
        $user->role = $input['role'];       
        $user->email = $input['email'];
        $user->phone = $input['phone'];

        $user->save();

        return redirect('/dashboard/users');

    }


    public function delete (Request $request)
    {

        $input = $request->all();

        if ( ! $project = User::find($input['id']) )
            die('User not found');
    
        $project->delete();

        return 'User deleted';
    }
}
