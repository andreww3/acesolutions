<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Task;

use File;

class TaskController extends Controller
{
    public function show6 ()
    {
        return Task::orderBy('created_at', 'desc')->limit(6)->get();
    }  

    public function showAll ()
    {
		return Task::orderBy('created_at', 'desc')->get();
    }  	

    public function uploadAttachment(Request $request) {

        $input = $request->all();
        
        $filename = $_FILES[ 'file' ][ 'name' ];
        $tempPath = $_FILES[ 'file' ][ 'tmp_name' ];

        $destinationPath = public_path().'/assets/img/tasks/tmp/';
        $uploadPath = $destinationPath . DIRECTORY_SEPARATOR .$filename;

        move_uploaded_file( $tempPath, $uploadPath );
        
    }

    public function add (Request $request)
    {

      $input = $request->all();

        $task = new Task;

        $task->fill([
            'title' => $input['title'],
            'type' => $input['type'],
            'description' => $input['description'],
            'initiator_id' => $input['initiator'],
            'deadline' => $input['deadline'],
            'status' => 'new',
        ]);

        $task->save();

        return redirect('/dashboard');

    }

    public function edit (Request $request)
    {

      $input = $request->all();

        if ( ! $task = Task::find($input['id']) )
            die('Task not found');

        if ($input['title'])
        	$task->title = $input['title'];

        if ($input['image'])
            $task->image = $input['image'];

        if ($input['description'])
            $task->description = $input['description'];

        if ($input['deadline'])
            $task->started_at = $input['deadline'];

        $task->save();

        return redirect('/dashboard');

    }


    public function removeImage (Request $request)
    {

      $input = $request->all();

        if ( ! $task = Task::find($input['id']) )
            die('Ticket not found');

        $filename = $input['image'];

        if (File::exists($filename)) {
            File::delete($filename);
            $task->image = '';
        } 
        
        $task->save();

    }

    public function delete (Request $request)
    {

        $input = $request->all();

        if ( ! $task = Task::find($input['id']) )
            die('Ticket not found');
    
        $task->delete();

    }

}
