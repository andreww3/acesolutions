<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\ContactFormRequest;
use App\Http\Requests\QuoteFormRequest;

class ContactController extends Controller
{
   	public function create()
    {
        return view('forms.contact');
    }	

  	public function store(ContactFormRequest $request)
  	{

	    \Mail::send('emails.contact',
	        array(
	            'name' => $request->get('name'),
	            'email' => $request->get('email'),
	            'subject' => $request->get('subject'),
	            'bodyMessage' => $request->get('message')
	        ), function($message)
	    {
	        $message->from('adm@acesolutions.co', "AceSolutions.co");
	        $message->to('cristian.andrei@live.com', 'Admin')->subject('Message from contact form');
	    });
	    
    	return \Redirect::route('contact')
      		->with('message', 'Thanks for contacting us!');

  	}

  	public function storeQuote(QuoteFormRequest $request)
  	{

	    \Mail::send('emails.quote',
	        array(
	            'name' => $request->get('name'),
	            'email' => $request->get('email'),
	            'projectName' => $request->get('projectName'),
	            'description' => $request->get('description'),
	            'task' => $request->get('task'),
	            'budget' => $request->get('budget'),
	            'comment' => $request->get('comment')
	        ), function($message)
	    {
	        $message->from('adm@acesolutions.co', "AceSolutions.co");
	        $message->to('cristian.andrei@live.com', 'Admin')->subject('Application for Quote');
	    });
	    
    	return \Redirect::route('contact')
      		->with('message', 'Thanks for your application! I personally check for applications daily and will get back to you in max 2 working days after I have an insight on how your task can be solved, how long it will take, and how much it will cost.');

  	}

}
