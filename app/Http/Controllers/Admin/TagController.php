<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Tag;
use App\Http\Requests;
use App\Http\Controllers\Controller;


class TagController extends Controller
{

  	protected $fields = [
	    'title' => '',
		'meta_description' => '',
		'layout' => 'blog.layouts.index',
  	];

  	public function index()
  	{
    	$tags = Tag::all();

    	return view('admin.tag.index')
            ->withTags($tags);
  	}

  	/**
   	 * Show form for creating new tag
   	*/
  	public function create()
  	{
	    $data = [];
	    foreach ($this->fields as $field => $default) {
	      $data[$field] = old($field, $default);
	    }

	    return view('admin.tag.create', $data);
  	}

  	/* *
   	  * Store the newly created tag in the database.
     *
    * @param Request $request
    * @return Response
    */
  	public function store(Request $request)
  	{
	    $tag = new Tag();
	    foreach (array_keys($this->fields) as $field) {
	      $tag->$field = $request->get($field);
	    }
	    $tag->save();

	    return redirect('/dashboard/tag')
	        ->withSuccess("The tag '$tag->title' was created.");
  	}

  	/* *
   	  * Show the form for editing a tag
     *
    * @param int $id
    * @return Response
    */
  	public function edit($id)
  	{
	    $tag = Tag::findOrFail($id);
	    $data = ['id' => $id];
	    foreach (array_keys($this->fields) as $field) {
	      $data[$field] = old($field, $tag->$field);
	    }

	  	return view('admin.tag.edit', $data);
	}

  	/* *
   	  * Update the tag in storage
     *	
    * @param Request $request, int $id
    * @return Response
    */
  	public function update(Request $request, $id)
  	{
	    $tag = Tag::findOrFail($id);

	    foreach (array_keys(array_except($this->fields, ['tag'])) as $field) {
	      $tag->$field = $request->get($field);
	    }
	    $tag->save();

	    return redirect("/dashboard/tag/$id/edit")
	        ->withSuccess("Changes saved.");
	}

  	/* *
   	  * Delete the tag
     *	
    * @param Request $request, int $id
    * @return Response
    */
  	public function destroy($id)
  	{
	    $tag = Tag::findOrFail($id);
	    $tag->delete();

	    return redirect('/dashboard/tag')
	        ->withSuccess("The '$tag->title' tag has been deleted.");
  	}

}
