<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Category;
use App\Http\Requests;
use App\Http\Controllers\Controller;


class CategoryController extends Controller
{

  	protected $fields = [
	    'title' => '',
	    'subtitle' => '',
	    'page_image' => '',
		'meta_description' => '',
		'layout' => 'blog.layouts.index',
		'reverse_direction' => 0,
  	];


  	public function index()
  	{
    	$categories = Category::all();

    	return view('admin.category.index')
            ->withCategories($categories);
  	}

  	/**
   	 * Show form for creating new category
   	*/
  	public function create()
  	{
	    $data = [];
	    foreach ($this->fields as $field => $default) {
	      $data[$field] = old($field, $default);
	    }

	    return view('admin.category.create', $data);
  	}


  	/* *
   	  * Store the newly created category in the database.
     *
    * @param Request $request
    * @return Response
    */
  	public function store(Request $request)
  	{
	    $category = new Category();
	    foreach (array_keys($this->fields) as $field) {
	      $category->$field = $request->get($field);
	    }
	    $category->save();

	    return redirect('/dashboard/categories')
	        ->withSuccess("The category '$category->title' was created.");
  	}


  	/* *
   	  * Show the form for editing a category
     *
    * @param int $id
    * @return Response
    */
  	public function edit($id)
  	{
	    $category = Category::findOrFail($id);
	    $data = ['id' => $id];
	    foreach (array_keys($this->fields) as $field) {
	      $data[$field] = old($field, $category->$field);
	    }

	  	return view('admin.category.edit', $data);
	}

  	/* *
   	  * Update the category in storage
     *	
    * @param Request $request, int $id
    * @return Response
    */
  	public function update(Request $request, $id)
  	{
	    $category = Category::findOrFail($id);

	    foreach (array_keys(array_except($this->fields, ['category'])) as $field) {
	      $category->$field = $request->get($field);
	    }

	    $category->save();

      return redirect('/dashboard/categories')
          ->withSuccess("The category '$category->title' was changed.");
	}


  	/* *
   	  * Delete the category
     *	
    * @param Request $request, int $id
    * @return Response
    */
  	public function destroy($id)
  	{
	    $category = Category::findOrFail($id);
	    $category->delete();

	    return redirect('/dashboard/categories')
	        ->withSuccess("The '$category->title' category has been deleted.");
  	}


}
