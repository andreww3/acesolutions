<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Paypal;

class PaypalController extends Controller
{
    private $_apiContext;

    public function __construct()
    {
        $this->_apiContext = Paypal::ApiContext(
            config('services.paypal.client_id'),
            config('services.paypal.secret'));

        $this->_apiContext->setConfig(array(
            'mode' => 'sandbox',
            'service.EndPoint' => 'https://api.sandbox.paypal.com',
            'http.ConnectionTimeOut' => 30,
            'log.LogEnabled' => true,
            'log.FileName' => storage_path('logs/paypal.log'),
            'log.LogLevel' => 'FINE'
        ));

    }

    public function makeInvoice(Request $request)
	{
		$cURL = "curl -v -X POST https://api.sandbox.paypal.com/v1/invoicing/invoices";
		$cHead = 'Content-Type:application/json; Authorization: Bearer <Access-Token>';



	    $payer = Paypal::Payer();
	    $payer->setPaymentMethod('paypal');

	    $amount = Paypal:: Amount();
	    $amount->setCurrency('USD');
	    $amount->setTotal($request->input('pay'));

	    $transaction = Paypal::Transaction();
	    $transaction->setAmount($amount);
	    $transaction->setDescription('Buy Premium '.$request->input('type').' Plan on '.$request->input('pay'));

	    $redirectUrls = Paypal:: RedirectUrls();
	    $redirectUrls->setReturnUrl(route('getDone'));
	    $redirectUrls->setCancelUrl(route('getCancel'));

	    $payment = Paypal::Payment();
	    $payment->setIntent('sale');
	    $payment->setPayer($payer);
	    $payment->setRedirectUrls($redirectUrls);
	    $payment->setTransactions(array($transaction));

	    $response = $payment->create($this->_apiContext);
	    $redirectUrl = $response->links[1]->href;

	    return redirect()->to( $redirectUrl );
	}


    public function getCheckout(Request $request)
	{

	    $payer = Paypal::Payer();
	    $payer->setPaymentMethod('paypal');
	    $transactions = $request->input('items');
	    $amount = Paypal:: Amount();
	    $amount->setCurrency('USD');
	    $amount->setTotal($request->input('pay'));

	    $desc = 'You have ordered the following goods: ';
	    foreach ($transactions as $t){
	    	$desc .= $t.', ';
	    }

	    $desc .= 'which sum op to a total of $'.$request->input('pay');
    	$transaction = Paypal::Transaction();
    	$transaction->setAmount($amount);
    	$transaction->setDescription($desc);	

	    $redirectUrls = Paypal:: RedirectUrls();
	    $redirectUrls->setReturnUrl(route('getDone'));
	    $redirectUrls->setCancelUrl(route('getCancel'));

	    $payment = Paypal::Payment();
	    $payment->setIntent('sale');
	    $payment->setPayer($payer);
	    $payment->setRedirectUrls($redirectUrls);
	    $payment->setTransactions(array($transaction));
	    /*
		try {
		    $payment->create($this->_apiContext);
		} catch (PayPal\Exception\PayPalConnectionException $e) {
		    echo $e->getData(); // This will print a JSON which has specific details about the error.
		    exit;
		}*/

	    $response = $payment->create($this->_apiContext);

	    $redirectUrl = $response->links[1]->href;

	    return redirect()->to( $redirectUrl );
	}

	public function getDone(Request $request)
	{
	    $id = $request->get('paymentId');
	    $token = $request->get('token');
	    $payer_id = $request->get('PayerID');

	    $payment = Paypal::getById($id, $this->_apiContext);

	    $paymentExecution = Paypal::PaymentExecution();

	    $paymentExecution->setPayerId($payer_id);
 
	    $executePayment = $payment->execute($paymentExecution, $this->_apiContext);

	    
	    print_r($executePayment);
	}

	public function getCancel()
	{
	    return redirect()->route('conscious-mindz');
	}
}
http://acesolutions.app/getDone?paymentId=PAY-2TB426321L137874FLAPOCXY&token=EC-16J05536U22412410&PayerID=56WG2PKVACXJG