<?php
use Illuminate\Support\Facades\Auth;
/*
|--------------------------------------------------------------------------
| Register/Login routes Routes
|----------r----------------------------------------------------------------
*/
/*
Route::get('register', 'Auth\AuthController@getRegister');
Route::post('register', 'Auth\AuthController@getRegister');

Route::get('login', 'Auth\AuthController@getLogin');
Route::post('login', 'Auth\AuthController@postLogin');
Route::get('logout', 'Auth\AuthController@getLogout');*/

/*
|--------------------------------------------------------------------------
| This route group applies the "auth" middleware group to every route
| it contains. The "auth" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/
Route::group(['middleware' => 'auth'], function () {

	Route::auth();
	Route::get('logout', 'Auth\LoginController@logout');

	// --- Blog Admin routes |
	Route::group([
	  'namespace' => 'Admin',
	], function () {
	  	Route::resource('/post', 'PostController', ['except' => 'show']);
	  	Route::resource('/categories', 'CategoryController', ['except' => 'show']);
	  	Route::resource('/tag', 'TagController', ['except' => 'show']);
	  	Route::get('/media', 'MediaController@index');
		Route::post('/media/file', 'MediaController@uploadFile');
		Route::delete('/media/file', 'MediaController@deleteFile');
		Route::post('/media/folder', 'MediaController@createFolder');
		Route::delete('/media/folder', 'MediaController@deleteFolder');
	});


	Route::group(['prefix' => 'dashboard'], function () {
		
		Route::get('/projects', function () {
			return view('admin.projects');
		});

		Route::get('/tasks', function () {
			return view('admin.tasks');
		});

		Route::get('/users', function () {
			return view('admin.users');
		});

	    Route::get('/blog', function () {
			return view('admin.blog');
		});
	});
});

/*
|--------------------------------------------------------------------------
| Api Routes
|--------------------------------------------------------------------------
| For all the data fo be used in the front-end 
*/

Route::group(['prefix' => 'api'], function () {

    Route::get('tasks', ['uses' => 'TaskController@showAll']);
    
    Route::get('products', ['uses' => 'ProductController@showAll']);

    Route::get('6/projects', ['uses' => 'ProjectController@show6']);

    Route::get('6/posts', ['uses' => 'BlogController@show6']);
    Route::get('posts', ['uses' => 'BlogController@showAll']);
    Route::get('categories', ['uses' => 'BlogController@showCategories']);

    Route::get('projects', ['uses' => 'ProjectController@showAll']);
    Route::get('services', ['uses' => 'ServiceController@show']);

    Route::get('clients', ['uses' => 'UserController@showClients']);
    Route::get('members', ['uses' => 'UserController@showMembers']);

    Route::get('missio/dreams', ['uses' => 'DreamController@showAll']);
    Route::get('missio/latest', ['uses' => 'DreamController@showLatest']);
    Route::get('missio/random', ['uses' => 'DreamController@showRandom']);
}); 

/*
|--------------------------------------------------------------------------
| Paypal Routes
|--------------------------------------------------------------------------
| 
*/
Route::get('payPremium', ['as'=>'payPremium','uses'=>'PaypalController@payPremium']);
Route::post('getCheckout', ['as'=>'getCheckout','uses'=>'PaypalController@getCheckout']);
Route::get('getDone', ['as'=>'getDone','uses'=>'PaypalController@getDone']);
Route::get('getCancel', ['as'=>'getCancel','uses'=>'PaypalController@getCancel']);

/*\_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ - _ - _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ __ ___/*


/*--------------------------------------------------------------------------
| cMindz Routes
|--------------------------------------------------------------------------
*/
Route::group(['prefix' => 'conscious-mindz'], function () {
	Route::get('/', ['as'=>'conscious-mindz', function () {
		return view('cMindz.index');
	}]);
	Route::get('thanks', function () {
		return view('cMindz.thanks');
	});	
});

/*--------------------------------------------------------------------------
| Misso Routes
|--------------------------------------------------------------------------
| Temporary
*/

Route::get('missio', function () {
	return view('missio.index');
});

Route::post('missio/add',
	['as' => 'addDream', 'uses' => 'DreamController@add']);

Route::get('missio/add', function () {
	return view('missio.index');
});


/*--------------------------------------------------------------------------
| Facade Routes
|--------------------------------------------------------------------------
| Front-end
*/
Route::group(['middleware' => 'guest'], function () {

	Route::get('/', function () {
	    return view('facade.home');
	});

	Route::get('/services', function () {
	    return view('facade.services');
	});

	Route::get('/projects', function () {
	    return view('facade.projects');
	});

	Route::get('/about', function () {
	    return view('facade.about');
	});

	Route::get('/contact', function () {
	    return view('facade.contact');
	});


	// -- | Member routes | -- 
	Route::get('/Andrei-Cristian', function () {
	    return view('members.andrei');
	});

	// -- Contact/messaging routes
	Route::get('/quote', function () {
		return view('facade.quote');
	});

	Route::post('quote', 
	  ['as' => 'quote_store', 'uses' => 'ContactController@storeQuote']);

	Route::post('contact', 
	  ['as' => 'contact_store', 'uses' => 'ContactController@store']);


	// -- | Blog routes | -- //
	Route::get('blog', 'BlogController@index');
	Route::get('blog/{slug}', 'BlogController@showPost');

	// Admin options routes..
	Route::post('addPost',
		['as' => 'addPost', 'uses' => 'BlogController@addPost']);

	Route::post('updatePost',
		['as' => 'updatePost', 'uses' => 'BlogController@updatePost']);

	Route::post('deletePost',
		['as' => 'deletePost', 'uses' => 'BlogController@deletePost']);



	Route::post('addUser',
		['as' => 'addUser', 'uses' => 'UserController@add']);

	Route::post('updateUser',
		['as' => 'updateUser', 'uses' => 'UserController@update']);

	Route::post('deleteUser',
		['as' => 'deleteUser', 'uses' => 'UserController@delete']);


	Route::post('uploadProductImage',
		['as' => 'uploadProductImage', 'uses' => 'ProductController@uploadImage']);

	Route::post('addProduct',
		['as' => 'addProduct', 'uses' => 'ProductController@add']);

	Route::post('editProduct',
		['as' => 'editProduct', 'uses' => 'ProductController@edit']);

	Route::post('removeProductImage',
		['as' => 'removeProductImage', 'uses' => 'ProductController@removeImage']);

	Route::post('deleteProduct',
		['as' => 'deleteProduct', 'uses' => 'ProductController@delete']);

	Route::post('uploadProjectImage',
		['as' => 'uploadProjectImage', 'uses' => 'ProjectController@uploadImage']);

	Route::post('addProject',
		['as' => 'addProject', 'uses' => 'ProjectController@add']);

	Route::post('editProject',
		['as' => 'editProject', 'uses' => 'ProjectController@edit']);

	Route::post('removeProjectImage',
		['as' => 'removeProjectImage', 'uses' => 'ProjectController@removeImage']);

	Route::post('deleteProject',
		['as' => 'deleteProject', 'uses' => 'ProjectController@delete']);



	Route::post('addService',
		['as' => 'addService', 'uses' => 'ServiceController@add']);

	Route::post('uploadServiceIcon',
		['as' => 'uploadServiceIcon', 'uses' => 'ServiceController@uploadIcon']);

	Route::post('editService',
		['as' => 'editService', 'uses' => 'ServiceController@edit']);

	Route::post('removeServiceIcon',
		['as' => 'removeServiceIcon', 'uses' => 'ServiceController@removeIcon']);
	
	Route::post('deleteService',
		['as' => 'deleteService', 'uses' => 'ServiceController@delete']);


	/* * * * Client Routes * * * * */
	Route::post('taskAttachement',
		['as' => 'taskAttachement', 'uses' => 'TaskController@uploadAttachment']);

	Route::post('addTask',
		['as' => 'addTask', 'uses' => 'TaskController@add']);
	Route::post('editTask',
		['as' => 'editTask', 'uses' => 'TaskController@edit']);
	
	Route::get('dashboard', function () {

		if (Auth::user()) {
			
			$role = Auth::user()->role;
			if ($role == 'client') {
        		return view('client.dashboard');
        	
        	} elseif ($role == 'cMindz') {
        		return view('cMindz.products');
        	
        	} elseif ($role == 'admin') {
        		return view('admin.dashboard');
    		}

    	} else {
    		return redirect('login');
    	}
	});
});