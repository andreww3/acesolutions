<?php

namespace App\Http\Requests;

use Carbon\Carbon;
use App\Http\Requests\Request;

class PostCreateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          'title' => 'required',
          'category_id' => 'required',
          'content' => 'required',
          'publish_date' => 'required',
          'publish_time' => 'required',
          'layout' => 'required',
        ];
    }

    /**
     * Return the fields and values to create a new post from
    */
    public function postFillData()
    {
        $published_at = new Carbon(
          $this->publish_date.' '.$this->publish_time
        );
        $content_raw = html_entity_decode($this->get('content'));
        return [
          'title' => $this->title,
          'subtitle' => $this->subtitle,
          'category_id' => $this->category_id,
          'page_image' => $this->page_image,
          'content_raw' => $content_raw,
          'content_html' => $this->get('content'),
          'meta_description' => $this->meta_description,
          'is_draft' => (bool)$this->is_draft,
          'published_at' => $published_at,
          'layout' => $this->layout,
        ];
    }

}
