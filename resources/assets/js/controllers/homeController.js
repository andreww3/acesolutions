app.controller('homeCtrl',
	['$rootScope', '$scope', '$route', '$http',
	 function ($rootScope, $scope, $route, $http) {

	/* *
	  * * * Initialize projects/services * * * *
	   						                  */

    // ---> Get all projects <--- //
    $http({
        method: 'GET',
        url: '/api/projects'
    }).then(function (response) {
        $scope.projects = response.data;
        //console.log($scope.projects);
    }, function (response) {
        console.log(response)
    });

    // ---> Get all services <--- //
    $http({
        method: 'GET',
        url: '/api/services'
    }).then(function (response) {
        $scope.services = response.data;
        //console.log($scope.services);
    }, function (response) {
        console.log(response)
    });

	/* *
	  * End * *
	   		 */	

	 	$scope.showProjects = function () {

	 		if ($scope.showServicesList == true) {
	 			$scope.showServicesList = false;
	 		}

	 		$scope.showProjectsList = true;
	 	}

	 	$scope.showServices = function () {
	 		if ($scope.showProjectsList == true) {
	 			$scope.showProjectsList = false;
	 		}

	 		$scope.showServicesList = true;
	 	}

	 }]);