app.controller('clientCtrl',
	['$rootScope', '$scope', '$route', '$http', 'FileUploader',
	 function ($rootScope, $scope, $route, $http, FileUploader) {
	    
	/* *
	  * * * Initialize tasks * * * *
	   						      */

    $http({
        method: 'GET',
        url: '/api/tasks'
    }).then(function (response) {
        $scope.tasks = response.data;

    }, function (response) {
        console.log(response)
    });
    
    /* *
      * End * *
             */ 


	/* *
	  * * * Initialize datepicker * * * *
	   						           */
    $scope.task = {};
    $scope.format = 'yyyy-MM-dd';

    $scope.task.deadline = new Date();

	$scope.today = function() {
	    $scope.dt = new Date();
	};
	$scope.today();

	$scope.inlineOptions = {
	    customClass: getDayClass,
	    minDate: new Date(2014, 01, 01),
	    showWeeks: false
	  };


	var tomorrow = new Date();
	  tomorrow.setDate(tomorrow.getDate() + 1);

	var afterTomorrow = new Date();
	  afterTomorrow.setDate(tomorrow.getDate() + 1);

	$scope.events = [];
	    
	function getDayClass(data) {
		var date = data.date,
		  mode = data.mode;

		if (mode === 'day') {
		  var dayToCheck = new Date(date).setHours(0,0,0,0);

		  for (var i = 0; i < $scope.events.length; i++) {
		    var currentDay = new Date($scope.events[i].date).setHours(0,0,0,0);

		    if (dayToCheck === currentDay) {
		      return $scope.events[i].status;
		    }
		  }
		}

		return '';
	}

	/* *
	  * End datepicker initialization * *
	   		                           */	

    /* *
      * * * Prepare uploader * * * *
                                  * */

    $scope.openImageUploader = function () {
       
        $scope.addImage = true;

        var token = $('meta[name="csrf-token"]').attr('content');

        var uploader = $scope.uploader = new FileUploader({
            url: '/taskAttachement',
            formData: [{
                '_token': token
            }]
        });

        // FILTERS
        uploader.filters.push({
            name: 'imageFilter',
            fn: function(item /*{File|FileLikeObject}*/, options) {
                var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
                return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
            }
        });

        // CALLBACKS
        uploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/, filter, options) {
            console.info('onWhenAddingFileFailed', item, filter, options);
        };
        uploader.onAfterAddingFile = function(fileItem) {
            console.info('onAfterAddingFile', fileItem);
        };
        uploader.onAfterAddingAll = function(addedFileItems) {
            console.info('onAfterAddingAll', addedFileItems);
        };
        uploader.onBeforeUploadItem = function(item) {
            console.info('onBeforeUploadItem', item);
            //posting the user changes
        };
        uploader.onProgressItem = function(fileItem, progress) {
            console.info('onProgressItem', fileItem, progress);
        };
        uploader.onProgressAll = function(progress) {
            console.info('onProgressAll', progress);
        };
        uploader.onSuccessItem = function(fileItem, response, status, headers) {
            //console.info('onSuccessItem', fileItem, response, status, headers);

            $scope.newAttachement = undefined;
            console.info('onAfterAddingFile', fileItem);
            $scope.newAttachement = 'assets/img/tasks/tmp/'+ fileItem._file.name;
            console.log($scope.newAttachement);

        };
        uploader.onErrorItem = function(fileItem, response, status, headers) {
            console.info('onErrorItem', fileItem, response, status, headers);
        };
        uploader.onCancelItem = function(fileItem, response, status, headers) {
            console.info('onCancelItem', fileItem, response, status, headers);
        };
        uploader.onCompleteItem = function(fileItem, response, status, headers) {
            console.info('onCompleteItem', fileItem, response, status, headers);
        };
        uploader.onCompleteAll = function() {
            console.info('onCompleteAll');
        };

        console.info('uploader', uploader);
    }

    /* * *
        * * Upload files on click * */  
        
    $scope.fileChooser = function () {
        angular.element('#fileSelect').trigger('click'); //angular way
    };
    
    /* *
      * End Uploader scripts * *
                              */    
}]);