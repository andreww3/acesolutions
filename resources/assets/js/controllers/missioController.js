app.controller('missioCtrl',
	['$scope', '$http',
		function ($scope, $http){
		
	/* *
	  * * * Initialize projects/services * * * *
	   						                  */

    // ---> Get dreams <--- //

    $scope.getAllDreams = function () {
        
        $scope.dreams = [];
        
        $http({
            method: 'GET',
            url: '/api/missio/dreams'
        }).then(function (response) {
            $scope.dreams = response.data;
            //console.log($scope.projects);
        }, function (response) {
            console.log(response)
        });        
    }

    $scope.getLatestDreams = function () {
        
        $scope.dreams = [];

        $http({
            method: 'GET',
            url: '/api/missio/latest'
        }).then(function (response) {
            $scope.dreams = response.data;
            //console.log($scope.projects);
        }, function (response) {
            console.log(response)
        });
    }

    $scope.getLatestDreams();

    $scope.getRandomDreams = function () {
        
        $scope.dreams = [];

        $http({
            method: 'GET',
            url: '/api/missio/random'
        }).then(function (response) {
            $scope.dreams = response.data;
            //console.log($scope.projects);
        }, function (response) {
            console.log(response)
        });
    }


    // ---> Add dreams <--- //

	$scope.addDream = function(dream){

        //posting the user changes
        var data = dream;
        data['_token'] = $('meta[name="csrf-token"]').attr('content');
		
		console.log(data);
        
        $http.post("missio/add", data).then(
            function successCallback(response) {

                $http.get('/api/missio/dreams').then(
                    function successCallback(response) {

                        $scope.dreams = response.data;
                        
                }, function errorCallback(response){
                    console.log("Awww Error!" + response);
                });

            }, //error callback
            function errorCallback(response) {
                console.log("error: " + response);
            }
        );

	}	

}]);