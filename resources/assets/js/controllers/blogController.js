app.controller('blogCtrl',
	['$rootScope', '$scope', '$route', '$http', 'FileUploader', 'filterFilter', '$sce',
	 function ($rootScope, $scope, $route, $http, FileUploader, filterFilter, $sce) {

	/* *
	  * * * Initialize Posts * * *
	   						    * */

	$scope.getPosts = function () {
	    $http({
	        method: 'GET',
	        url: 'api/posts'
	    }).then(function (response) {
	        $scope.posts = response.data;
	        console.log($scope.posts);
	    }, function (response) {
	        console.log(response)
	    });		
	}

	$scope.getPosts();

	$scope.renderHTML = function(html_code) {
		return $sce.trustAsHtml(html_code);
	}

	$scope.get6Posts = function () {
	    $http({
	        method: 'GET',
	        url: 'api/6/posts'
	    }).then(function (response) {
	        $scope.posts = response.data;
	        console.log($scope.posts);
	    }, function (response) {
	        console.log(response)
	    });		
	}

	$scope.getPosts();

	/* *
	  * * * Initialize Categories * * *
	   						         * */

	$scope.getCategories = function () {
	    $http({
	        method: 'GET',
	        url: '/api/categories'
	    }).then(function (response) {
	        $scope.categories = response.data;
	        //console.log($scope.categories);
	    }, function (response) {
	        console.log(response)
	    });		
	}

	$scope.getCategories();

	/* *
	  * * * Change Categories * * */

	$scope.changeCategory = function ($id) {
	    $http({
	        method: 'GET',
	        url: '/api/posts'
	    }).then(function (response) {
	        $scope.posts = response.data;
			$scope.posts = filterFilter($scope.posts, {category_id:$id});

			console.log($scope.posts);

	        //console.log($scope.categories);
	    }, function (response) {
	        console.log(response)
	    });
	}
    /* *
      * End * *
             */ 
   						           
	 }]);