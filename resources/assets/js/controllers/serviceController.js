app.controller('serviceCtrl',
	['$rootScope', '$scope', '$route', '$http', 'FileUploader',
	 function ($rootScope, $scope, $route, $http, FileUploader) {
	    
	/* *
	  * * * Initialize services * * * *
	   						         */

    $http({
        method: 'GET',
        url: '/api/services'
    }).then(function (response) {
        $scope.services = response.data;
        //console.log($scope.services);
    }, function (response) {
        console.log(response)
    });


	/* *
	  * * * Prepare uploader * * * *
	   						      * */
       
    $scope.addIcon = false;

	$scope.openIconUploader = function () {
	   
        $scope.addIcon = true;

	    var uploader = $scope.uploader = new FileUploader({
	        url: '/uploadServiceIcon'
	    });

	    // FILTERS
	    uploader.filters.push({
	        name: 'imageFilter',
	        fn: function(item /*{File|FileLikeObject}*/, options) {
	            var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
	            return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
	        }
	    });

        // CALLBACKS
        uploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/, filter, options) {
            console.info('onWhenAddingFileFailed', item, filter, options);
        };
        uploader.onAfterAddingFile = function(fileItem) {
            console.info('onAfterAddingFile', fileItem);
        };
        uploader.onAfterAddingAll = function(addedFileItems) {
            console.info('onAfterAddingAll', addedFileItems);
        };
        uploader.onBeforeUploadItem = function(item) {
            item['_token'] = $('meta[name="csrf-token"]').attr('content');
            console.info('onBeforeUploadItem', item);
            //posting the user changes
        };
        uploader.onProgressItem = function(fileItem, progress) {
            console.info('onProgressItem', fileItem, progress);
        };
        uploader.onProgressAll = function(progress) {
            console.info('onProgressAll', progress);
        };
        uploader.onSuccessItem = function(fileItem, response, status, headers) {
            //console.info('onSuccessItem', fileItem, response, status, headers);

            $scope.newServiceIcon = undefined;
            console.info('onAfterAddingFile', fileItem);
            $scope.newServiceIcon = 'assets/img/services/'+ fileItem._file.name;
            console.log($scope.newServiceIcon);

        };
        uploader.onErrorItem = function(fileItem, response, status, headers) {
            console.info('onErrorItem', fileItem, response, status, headers);
        };
        uploader.onCancelItem = function(fileItem, response, status, headers) {
            console.info('onCancelItem', fileItem, response, status, headers);
        };
        uploader.onCompleteItem = function(fileItem, response, status, headers) {
            console.info('onCompleteItem', fileItem, response, status, headers);
        };
        uploader.onCompleteAll = function() {
            console.info('onCompleteAll');
        };

        console.info('uploader', uploader);
	}

    /* * *
        * * Upload files on click * */  
        
    $scope.fileChooser = function () {
        angular.element('#fileSelect').trigger('click'); //angular way
    };
   	
	/* *
	  * End Uploader scripts * *
	   		                  */	



    /* * 
      * * Save service function * *
      						     * */

   	$scope.saveService = function ($id, $icon, $name, $description) {

   		console.log($icon);

        var iconLink = '';

   		if ($scope.newServiceIcon != undefined) {
   			var iconLink = $scope.newServiceIcon;
   		}

   		console.log('Link:');
   		console.log(iconLink);

        $scope.service = {
            id : $id,
            name: $name,
            description: $description,
            icon: iconLink
        }

        //posting the user changes
        var data = $scope.service;
        data['_token'] = $('meta[name="csrf-token"]').attr('content');

        $http.post("/editService", data).then(
            function successCallback(response) {

                $http.get('api/services/').then(
                    function successCallback(response) {
                        
                        console.log(response.data);
                        $scope.services = response.data;
                        //$scope.user.experiences = angular.copy(response.data.experiences);

                        //$scope.userChanges.experiences = angular.copy($scope.user.experiences);

                        //Clear the user changes to avoid entry repetition
                        /*if( $scope.userChanges.newExperience ){
                            delete $scope.userChanges.newExperience;
                        }*/

                        
                }, function errorCallback(response){
                    console.log("Awww Error!" + response);
                });
            }, //error callback
            function errorCallback(response) {
                console.log("error: " + response);
            }
        );
        var uploader = undefined;
        $scope.editService = false;   		
   	}

    /* * 
      * * Delete service function * *
                                   * */
    
    $scope.deleteService = function ($id) {

        $scope.removeService = {
            id : $id,
            _token: $('meta[name="csrf-token"]').attr('content')
        }

        var data = $scope.removeService;

        console.log(data);
        $http.post("/deleteService", data).then(
            function successCallback(response) {
                console.log('deleted')

                $http.get('api/services/').then(
                    function successCallback(response) {
                        
                        console.log(response.data);
                        $scope.services = response.data;
                        //$scope.user.experiences = angular.copy(response.data.experiences);

                        //$scope.userChanges.experiences = angular.copy($scope.user.experiences);

                        //Clear the user changes to avoid entry repetition
                        /*if( $scope.userChanges.newExperience ){
                            delete $scope.userChanges.newExperience;
                        }*/

                        
                }, function errorCallback(response){
                    console.log("Awww Error!" + response);
                });                
            }, //error callback
            function errorCallback(response) {
                console.log("error: " + response);
            }
        );  
    }
   	
	/* *
	  * End * *
	   		 */	


    /* * 
      * * Remove service icon * *
      						   * */

   	$scope.removeServiceIcon = function ($id, $icon) {

   		console.log('ServiceIcon:');
   		console.log($icon);

        $scope.removeIcon = {
            id : $id,
            icon: $icon
        }

        //posting the user changes
        var data = $scope.removeIcon;
        data['_token'] = $('meta[name="csrf-token"]').attr('content');

        $http.post("/removeServiceIcon", data).then(
            function successCallback(response) {
            	$scope.iconRemoved = true; 

            }, //error callback
            function errorCallback(response) {
                console.log("error: " + response);
            }
        );   		
   	}

	/* *
	  * End * *
	   		 */		   						           
	 }]);