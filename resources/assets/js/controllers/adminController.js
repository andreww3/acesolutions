app.controller('adminCtrl',
	['$rootScope', '$scope', '$route', '$http', 'FileUploader',
	 function ($rootScope, $scope, $route, $http, FileUploader) {
	    
	/* *
	  * * * Initialize projects/services * * * *
	   						                  */

    // ---> Get all projects <--- //
    $http({
        method: 'GET',
        url: '/api/projects'
    }).then(function (response) {
        $scope.projects = response.data;
        //console.log($scope.projects);
    }, function (response) {
        console.log(response)
    });

    // ---> Get all services <--- //
    $http({
        method: 'GET',
        url: '/api/services'
    }).then(function (response) {
        $scope.services = response.data;
        //console.log($scope.services);
    }, function (response) {
        console.log(response)
    });

    // ---> Get all clients <--- //
    $http({
        method: 'GET',
        url: '/api/clients'
    }).then(function (response) {
        $scope.clients = response.data;
        //console.log($scope.services);
    }, function (response) {
        console.log(response)
    });

    // ---> Get all members <--- //
    $http({
        method: 'GET',
        url: '/api/members'
    }).then(function (response) {
        $scope.members = response.data;
        //console.log($scope.services);
    }, function (response) {
        console.log(response)
    });
    
    /* *
      * End * *
             */ 


    /* *
      * * * Initialize scopes * * * *
                                   * */

    $scope.project = {
        title: '',
        started_at: '',
        delivered_at: '',
    };

    
    /* *
      * End * *
             */


	/* *
	  * * * Initialize datepicker * * * *
	   						           */
	    $scope.format = 'yyyy-MM-dd';

	    $scope.project.started_at = new Date();
        $scope.project.delivered_at = new Date();

        console.log($scope.project);
		$scope.today = function() {
		    $scope.dt = new Date();
		  };
		$scope.today();

		$scope.inlineOptions = {
		    customClass: getDayClass,
		    minDate: new Date(2014, 01, 01),
		    showWeeks: false
		  };


		var tomorrow = new Date();
		  tomorrow.setDate(tomorrow.getDate() + 1);

		var afterTomorrow = new Date();
		  afterTomorrow.setDate(tomorrow.getDate() + 1);

		$scope.events = [];
		    
		function getDayClass(data) {
			var date = data.date,
			  mode = data.mode;

			if (mode === 'day') {
			  var dayToCheck = new Date(date).setHours(0,0,0,0);

			  for (var i = 0; i < $scope.events.length; i++) {
			    var currentDay = new Date($scope.events[i].date).setHours(0,0,0,0);

			    if (dayToCheck === currentDay) {
			      return $scope.events[i].status;
			    }
			  }
			}

			return '';
		}

	/* *
	  * End datepicker initialization * *
	   		                           */	


    /* * 
      * * Delete Client function * *
                                  * */
    
    $scope.deleteClient = function ($id) {

        $scope.removeClient = {
            id : $id,
            _token: $('meta[name="csrf-token"]').attr('content')
        }

        var data = $scope.removeClient;

        console.log(data);
        $http.post("/deleteUser", data).then(
            function successCallback(response) {
                console.log('deleted')
                
                $http.get('api/clients/').then(
                    function successCallback(response) {
                        $scope.clients = response.data;
                        
                }, function errorCallback(response){
                    console.log("Awww Error!" + response);
                });                
            }, //error callback
            function errorCallback(response) {
                console.log("error: " + response);
            }
        );  
    }
    
    /* *
      * End * *
             */ 


    /* * 
      * * Update Member info * *
                              * */
    
    $scope.updateMember = function ($id, $name, $role, $email, $phone) {

        $scope.member = {
            id : $id,
            name: $name,
            role: $role,
            email: $email,
            phone: $phone
        }


        //posting the user changes
        var data = $scope.member;
        data['_token'] = $('meta[name="csrf-token"]').attr('content');

        $http.post("/updateUser", data).then(
            function successCallback(response) {

                $http.get('api/members/').then(
                    function successCallback(response) {

                        $scope.members = response.data;
                        
                }, function errorCallback(response){
                    console.log("Awww Error!" + response);
                });

            }, //error callback
            function errorCallback(response) {
                console.log("error: " + response);
            }
        );
        $scope.editProject = false;  
    }
    
    /* *
      * End * *
             */ 


    /* * 
      * * Delete Client function * *
                                  * */
    
    $scope.deleteMember = function ($id) {

        $scope.removeMember = {
            id : $id,
            _token: $('meta[name="csrf-token"]').attr('content')
        }

        var data = $scope.removeMember;

        console.log(data);
        $http.post("/deleteUser", data).then(
            function successCallback(response) {
                console.log('deleted')
                
                $http.get('api/members/').then(
                    function successCallback(response) {
                        $scope.members = response.data;
                        
                }, function errorCallback(response){
                    console.log("Awww Error!" + response);
                });                
            }, //error callback
            function errorCallback(response) {
                console.log("error: " + response);
            }
        );  
    }
    
    /* *
      * End * *
             */ 
   						           
	 }]);