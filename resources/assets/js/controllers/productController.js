app.controller('productCtrl',
	['$rootScope', '$scope', '$route', '$http', '$window', '$location',
     'FileUploader', '$filter',
	 
    function ($rootScope, $scope, $route, $http, $window, $location,
                FileUploader, $filter) {
	    
	/* *
	  * * * Initialize projects * * * *
	   						         */

    $scope.getProducts = function () {

        $http({
            method: 'GET',
            url: '/api/products'
        }).then(function (response) {
            $scope.products = response.data;

        }, function (response) {
            console.log(response)
        });
    }

    /* *
      * End * *
             */ 

	/* *
	  * * * Prepare uploader * * * *
	   						      */
    $scope.addImage = false;
	$scope.openImageUploader = function () {

        $scope.addImage = true;

        var token = $('meta[name="csrf-token"]').attr('content');
        
	    var uploader = $scope.uploader = new FileUploader({
	        url: '/uploadProductImage',
            formData: [{
                '_token': token,
            }]
	        
        }); 

	    // FILTERS
	    uploader.filters.push({
	        name: 'imageFilter',
	        fn: function(item /*{File|FileLikeObject}*/, options) {
	            var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
	            return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
	        }
	    });
	    $scope.uploader = uploader;
        // CALLBACKS
        uploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/, filter, options) {
            console.info('onWhenAddingFileFailed', item, filter, options);
        };
        uploader.onAfterAddingFile = function(fileItem) {
            console.info('onAfterAddingFile', fileItem);
        };
        uploader.onAfterAddingAll = function(addedFileItems) {
            console.info('onAfterAddingAll', addedFileItems);
        };
        uploader.onBeforeUploadItem = function(item) {
            console.info('onBeforeUploadItem', item);
            //posting the user changes
        };
        uploader.onProgressItem = function(fileItem, progress) {
            console.info('onProgressItem', fileItem, progress);
        };
        uploader.onProgressAll = function(progress) {
            console.info('onProgressAll', progress);
        };
        uploader.onSuccessItem = function(fileItem, response, status, headers) {
            console.info('onSuccessItem', fileItem, response, status, headers);

            $scope.newProductImage = undefined;
            console.info('onAfterAddingFile', fileItem);
            $scope.newProductImage = 'assets/img/products/'+ fileItem._file.name;
            console.log($scope.newProductImage);

        };
        uploader.onErrorItem = function(fileItem, response, status, headers) {
            //console.info('onErrorItem', fileItem, response, status, headers);
        };
        uploader.onCancelItem = function(fileItem, response, status, headers) {
            //console.info('onCancelItem', fileItem, response, status, headers);
        };
        uploader.onCompleteItem = function(fileItem, response, status, headers) {
            //console.info('onCompleteItem', fileItem, response, status, headers);
        };
        uploader.onCompleteAll = function() {
            //console.info('onCompleteAll');
        };

        console.info('uploader', uploader);

	}

    /* * *
        * * Upload files on click * */  
        
    $scope.fileChooser = function () {
        angular.element('#fileSelect').trigger('click'); //angular way
    };
    
	/* *
	  * End Uploader scripts * *
	   		                  */	
                              
    /* * 
      * * Add product function * *
                                 * */
                                 
    $scope.addProduct = function ($name, $type, $price, $image, $description) {

        var imageLink = '';

        if ($scope.newProductImage != undefined) {
            var imageLink = $scope.newProductImage;
        }

        $scope.product = {
            name: $name,
            type: $type,
            price: $price,
            image: imageLink,
            description: $description
        }

        //posting the user changes
        var data = $scope.product;

        data['_token'] = $('meta[name="csrf-token"]').attr('content');

        $http.post("/addProduct", data).then(
            function successCallback(response) {

                $http.get('api/products/').then(
                    function successCallback(response) {
                        $scope.addingProduct = false;
                        $window.location.reload();

                }, function errorCallback(response){
                    console.log("Awww Error!" + response);
                });

            }, //error callback
            function errorCallback(response) {
                console.log("error: " + response);
            }
        );

    }


    /* * 
      * * Save product function * *
                                 * */
                                 
    $scope.saveProduct = function ($id, $name, $type, $price, $image, $description) {

        
        var imageLink = '';
        
        if ($image != undefined) {
            imageLink = $image;
        }

        if ($scope.newProductImage != undefined) {
            imageLink = $scope.newProductImage;
        } 

        $scope.product = {
            id : $id,
            name: $name,
            type: $type,
            price: $price,
            image: imageLink,
            description: $description
        }

        //posting the user changes
        var data = $scope.product;

        data['_token'] = $('meta[name="csrf-token"]').attr('content');

        $http.post("/editProduct", data).then(
            function successCallback(response) {

                $http.get('api/products/').then(
                    function successCallback(response) {

                        $scope.editingProduct = false;
                        $window.location.reload();
                        //$scope.products = response.data;
                        
                }, function errorCallback(response){
                    console.log("Awww Error!" + response);
                });

            }, //error callback
            function errorCallback(response) {
                console.log("error: " + response);
            }
        ); 
    }



    /* * 
      * * Remove product icon * *
                               * */

    $scope.removeProductImage = function ($id, $image) {

        $scope.removeImage = {
            id : $id,
            image: $image
        }

        //posting the user changes
        var data = $scope.removeImage;
        data['_token'] = $('meta[name="csrf-token"]').attr('content');

        $http.post("/removeProductImage", data).then(
            function successCallback(response) {
                $scope.imageRemoved = true; 

            }, //error callback
            function errorCallback(response) {
                console.log("error: " + response);
            }
        );          
    }

    /* *
      * End * *
             */ 

    /* * 
      * * Delete product function * *
                                   * */
    
    $scope.deleteProduct = function ($id) {

        $scope.removeProduct = {
            id : $id,
            _token: $('meta[name="csrf-token"]').attr('content')
        }

        var data = $scope.removeProduct;

        $http.post("/deleteProduct", data).then(
            function successCallback(response) {
                
                $http.get('api/products/').then(
                    function successCallback(response) {
                        $scope.products = response.data;
                        
                }, function errorCallback(response){
                    console.log("Awww Error!" + response);
                });                
            }, //error callback
            function errorCallback(response) {
                console.log("error: " + response);
            }
        );  
    }
    /* *
      * * Add to cart * *
                       */       

    $scope.addToCart = function ($name, $price, $image) {
        $scope.hideCart = false;
        var item = {
            name: $name,
            price: $price,
            image: $image,
        }
        $price = parseInt($price);
        $scope.cart.total += $price;
        $scope.cart.count += 1;
        $scope.cart.items.push(item);

        console.log($scope.cart);
    }

    $scope.rmItem = function ($image, $price) {
        
        var item = $filter('filter')($scope.cart.items, {image:$image}, true);
        if (item.length){
            var index = $scope.cart.items.indexOf(item);
            $scope.cart.items.splice(index, 1);
            $price = parseInt($price);
            $scope.cart.total -= $price;
            $scope.cart.count -= 1;
        }
    }

    /* *
      * * Paypal API  * *
                       */ 
    $scope.pApi = {
        username: 'adm_api1.acesolutions.co',
        pass: 'MA9ZJU8FFSC6WWPY',
        sign: 'AFcWxV21C7fd0v3bYYYRCpSSRl31AT3svlcTwFfLsoF9A5YCwPfJ7lj0',
    }

    $scope.getCheckout = function() {
        
        var token = $('meta[name="csrf-token"]').attr('content');
        
        $scope.data = {
            _token: token,
            pay: $scope.cart.total,
            items: $scope.cart.items,
        }

        var data = $scope.data;

        console.log(data);

        $http.post("/getCheckout", data).then(
            function successCallback(response) {
               
            }, //error callback
            function errorCallback(response) {
                console.log("error: " + response.error);
            }
        );         
    }
    
    /* *
      * * Open app * *
                    */     

    $scope.getProducts();
    $scope.openImageUploader();
    $scope.showNecklaces = true;
    $scope.showBracelets = false;
    $scope.showAll = false;
    $scope.cart = {};
    $scope.cart.total = 0;
    $scope.cart.count = 0;
    $scope.cart.items = [];
    
    /* *
      * End * *
             */ 

	   						           
}]);