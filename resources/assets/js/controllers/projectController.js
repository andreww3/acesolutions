app.controller('projectCtrl',
	['$rootScope', '$scope', '$route', '$http', 'FileUploader',
	 function ($rootScope, $scope, $route, $http, FileUploader) {
	    
	/* *
	  * * * Initialize projects * * * *
	   						         */

    $scope.get6Projects = function () {
        $http({
            method: 'GET',
            url: '/api/6/projects'
        }).then(function (response) {
            $scope.projects = response.data;
            //console.log($scope.projects);
        }, function (response) {
            console.log(response)
        });
       $scope.showingAllProjects = false;
    }

    $scope.get6Projects();

    $scope.getAllProjects = function () {

        $http({
            method: 'GET',
            url: '/api/projects'
        }).then(function (response) {
            $scope.projects = response.data;
            //console.log($scope.projects);
        }, function (response) {
            console.log(response)
        });
       $scope.showingAllProjects = true;
    }
    

    /* *
      * End * *
             */ 

    $scope.addImage = false;

	/* *
	  * * * Prepare uploader * * * *
	   						      */

	$scope.openImageUploader = function () {

        $scope.addImage = true;

        var token = $('meta[name="csrf-token"]').attr('content');
        
	    var uploader = $scope.uploader = new FileUploader({
	        url: '/uploadProjectImage',
            formData: [{
                '_token': token
            }]
	        
        }); 

	    // FILTERS
	    uploader.filters.push({
	        name: 'imageFilter',
	        fn: function(item /*{File|FileLikeObject}*/, options) {
	            var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
	            return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
	        }
	    });

        // CALLBACKS
        uploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/, filter, options) {
            console.info('onWhenAddingFileFailed', item, filter, options);
        };
        uploader.onAfterAddingFile = function(fileItem) {
            console.info('onAfterAddingFile', fileItem);
        };
        uploader.onAfterAddingAll = function(addedFileItems) {
            console.info('onAfterAddingAll', addedFileItems);
        };
        uploader.onBeforeUploadItem = function(item) {
            console.info('onBeforeUploadItem', item);
            //posting the user changes
        };
        uploader.onProgressItem = function(fileItem, progress) {
            console.info('onProgressItem', fileItem, progress);
        };
        uploader.onProgressAll = function(progress) {
            console.info('onProgressAll', progress);
        };
        uploader.onSuccessItem = function(fileItem, response, status, headers) {
            console.info('onSuccessItem', fileItem, response, status, headers);

            $scope.newProjectImage = undefined;
            console.info('onAfterAddingFile', fileItem);
            $scope.newProjectImage = 'assets/img/projects/'+ fileItem._file.name;
            console.log($scope.newProjectImage);

        };
        uploader.onErrorItem = function(fileItem, response, status, headers) {
            //console.info('onErrorItem', fileItem, response, status, headers);
        };
        uploader.onCancelItem = function(fileItem, response, status, headers) {
            //console.info('onCancelItem', fileItem, response, status, headers);
        };
        uploader.onCompleteItem = function(fileItem, response, status, headers) {
            //console.info('onCompleteItem', fileItem, response, status, headers);
        };
        uploader.onCompleteAll = function() {
            //console.info('onCompleteAll');
        };

        console.info('uploader', uploader);

	}
    
    /* * *
        * * Upload files on click * */  
        
    $scope.fileChooser = function () {
        angular.element('#fileSelect').trigger('click'); //angular way
    };
    
	/* *
	  * End Uploader scripts * *
	   		                  */	
                              
    /* * 
      * * Save project function * *
                                 * */
                                 
    $scope.addProject = function ($id, $title, $summary, $image, $link, $description, $started_at, $delivered_at) {

        var imageLink = '';

        if ($scope.newProjectImage != undefined) {
            var imageLink = $scope.newProjectImage;
        }

        $scope.project = {
            title: $title,
            summary: $summary,
            image: imageLink,
            link: $link,
            description: $description,
            started_at: $started_at,
            delivered_at: $delivered_at
        }

        //posting the user changes
        var data = $scope.project;

        data['_token'] = $('meta[name="csrf-token"]').attr('content');

        $http.post("/addProject", data).then(
            function successCallback(response) {

                $http.get('api/projects/').then(
                    function successCallback(response) {
                        
                        $scope.projects = response.data;
                        
                }, function errorCallback(response){
                    console.log("Awww Error!" + response);
                });

            }, //error callback
            function errorCallback(response) {
                console.log("error: " + response);
            }
        );
        var uploader = undefined;
        $scope.editProject = false; 

    }


    /* * 
      * * Save project function * *
                                 * */
                                 
    $scope.saveProject = function ($id, $title, $summary, $image, $link, $description, $started_at, $delivered_at) {

        var imageLink = '';

        if ($scope.newProjectImage != undefined) {
            var imageLink = $scope.newProjectImage;
        }

        $scope.project = {
            id : $id,
            title: $title,
            summary: $summary,
            image: imageLink,
            link: $link,
            description: $description,
            started_at: $started_at,
            delivered_at: $delivered_at
        }

        //posting the user changes
        var data = $scope.project;

        data['_token'] = $('meta[name="csrf-token"]').attr('content');

        $http.post("/editProject", data).then(
            function successCallback(response) {

                $http.get('api/projects/').then(
                    function successCallback(response) {
                        
                        $scope.projects = response.data;
                        
                }, function errorCallback(response){
                    console.log("Awww Error!" + response);
                });

            }, //error callback
            function errorCallback(response) {
                console.log("error: " + response);
            }
        );
        var uploader = undefined;
        $scope.editProject = false; 

    }



    /* * 
      * * Remove service icon * *
                               * */

    $scope.removeProjectImage = function ($id, $image) {

        console.log('Image..');
        console.log($image);

        $scope.removeImage = {
            id : $id,
            image: $image
        }

        //posting the user changes
        var data = $scope.removeImage;
        data['_token'] = $('meta[name="csrf-token"]').attr('content');

        $http.post("/removeProjectImage", data).then(
            function successCallback(response) {
                $scope.imageRemoved = true; 

            }, //error callback
            function errorCallback(response) {
                console.log("error: " + response);
            }
        );          
    }

    /* *
      * End * *
             */ 

    /* * 
      * * Delete Project function * *
                                   * */
    
    $scope.deleteProject = function ($id) {

        $scope.removeProject = {
            id : $id,
            _token: $('meta[name="csrf-token"]').attr('content')
        }

        var data = $scope.removeProject;

        console.log(data);
        $http.post("/deleteProject", data).then(
            function successCallback(response) {
                console.log('deleted')
                
                $http.get('api/projects/').then(
                    function successCallback(response) {
                        $scope.projects = response.data;
                        
                }, function errorCallback(response){
                    console.log("Awww Error!" + response);
                });                
            }, //error callback
            function errorCallback(response) {
                console.log("error: " + response);
            }
        );  
    }
    
    /* *
      * End * *
             */ 

	   						           
	 }]);