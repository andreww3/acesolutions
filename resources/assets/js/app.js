var app = angular.module('acesolutions', [
	'ngRoute',
	'ngSanitize',
	'ui.bootstrap',
	'angularFileUpload'
	])

.config(['$routeProvider', '$interpolateProvider', '$sceDelegateProvider', function ($routeProvider, $interpolateProvider, $sceDelegateProvider) {

    $interpolateProvider.startSymbol('{[');
    $interpolateProvider.endSymbol(']}');


    $sceDelegateProvider.resourceUrlWhitelist([
        'self'
    ]);
}]);
