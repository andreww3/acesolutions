@extends('layouts.main')


@section('title', 'Andrei Cristian')


@section('header')
    
    <!-- Raw & Google meta descriptions -->
    <meta name="description" content="The portfolio page of Andrei Cristian, a graduated web designer and developer based in Copenhagen.">
    <meta name="keywords" content="Andrei Cristian portfolio, Andrei Cristian, concept developer, web designer, web developer">

    <!-- Facebook meta descriptions -->
    <meta property="og:title" content="Andrei Cristian" /> 
    <meta property="og:image" content="http://acesolutions.co/assets/style/img/square-logo.png" /> 
    <meta property="og:description" content="This is my professional presentation page as a web designer and developer graduated from KEA, working in Copenhagen." /> 
    <meta property="og:url" content="http://acesolutions.co/Andrei-Cristian">

    <style>
        header.profile-header {
            margin-top:10px;
            border:none;
            background-color: transparent;
            text-align:center;          
        }

        header.profile-header nav {
            padding-top:5px;
            text-align:center;
        }

        header.profile-header nav a {
            color: #333 !important;
            font-family: 'LatoRegular';
        }    

        .skill-box {
            opacity:0.6; 
            width:75%; 
            margin:0 auto; 
            position:relative;
        }

        .skill-box:hover {
            opacity:0.9;
        }

        .skill-box a {
            position:absolute; 
            color:white;
            bottom:5px; 
            left:0px; 
            width:100%; 
            text-align:center;
        }

        h4.allProjectsLink {
            width:95.5%; 
            margin:0 auto; 
            margin-left:1.5%; 
            padding-top:6px;
            padding-bottom: 4px; 
            text-align:center; 
            border-top:2px solid #199d7e; 
            border-bottom:1px solid #199d7e; 
            font-size:16px; 
            color:#199d7e;
        }

        h4.allProjectsLink a{
            opacity:0.8; 
            color:white;
            font-family:LatoLight;
        }

        h4.allProjectsLink a:hover {
            opacity:1; 
        }
                
        embed.cvBox {
            width: 98.5%;
            margin-left: 1.5%;
            height:1200px;
        }

        .socialIcon img {
            margin:10px;
            width:35px;
            height:auto;   
        }

        a.socialIocn { opacity:0.7; }
        a.socialIcon:hover { opacity:0.9; } 


    </style>
@endsection

@section('menu')
    <header class="profile-header">
        <nav>
            <a class="darkLink" id="portfolioLink">Portfolio</a> |
            <a class="darkLink" id="cvLink" ng-click="showCV = true">Cv</a>|
            <a class="darkLink" id="skillsLink">Skills</a> |
            <a class="darkLink" id="contactLink">Contact</a>
        </nav>
    </header>
@endsection


@section('content')

    
    <div class="container">

        <div class="homeIntro text-left">
            <div class="col-md-2">

            </div>
            <div class="col-md-8 text-center">
                <div style="overflow:hidden; border-radius: 150px; 
                            height: 300px; width:300px; margin:0 auto;
                            border: 1px solid #333;
                            text-align:center;">
                    <img width="320" height="300" src="assets/img/members/extras/Andrei-square.png">
                </div>
                <div class="title">Andrei Cristian</div>
                <p class="description"> 
                    Hi! I'm Andrei, a web designer and developer from Romania. I have been living in Copenhagen for the last 5 years. About this time last year I have graduated from <a class="darkLink" href="http://www.kea.dk/da/" target="_blank">KEA</a>, with a degree in Multimedia Design and Communication, and one in E-Concept Development.
                    <br /><br />
                    
                    After graduating I have created the brand <a class="darkLink" target="_blank" href="/">Ace Solutions</a> and have delivered several websites and web applications. In July 2015 I have joined CSE to work as a web development coordinator for building up a brand and platform <a class="darkLink" target="_blank" href="http://cvideon.com">CVideon</a>.
                    <br /><br />

                    Currently I'm looking for a preferably part-time job, as I would like to continue working with Ace Solutions and CVideon on a side. 
                    <br />
                </p>

            </div>
            <div class="clearfix spacer"></div>   
        </div>
    </div>

    <div id="portfolio" class="dark-section text-left">
        
        <div class="col-md-2"></div>

        <div class="col-md-8 text-center">
            <div class="title">Portfolio</div>
            <p class="description"> 
                These are the projects I've been working on both independetly and in teams.  
            <br /><br />
            </p>

        </div>

        <div class="col-md-2"></div>
        <div class="clearfix spacer"></div>  

        <div class="insideContainer" ng-controller="projectCtrl">
            <div style="position:relative" ng-repeat="project in projects" class="col-md-4">

                <div class="card-container">

                      <div class="card">
                        <div class="side">
                            <img ng-if="project.image" ng-src="{[ project.image ]}">
                        </div>

                        <div class="side back">
                           <img ng-if="project.image" ng-src="{[ project.image ]}">
                            <h3>{[ project.title ]} <a ng-if="project.link" href="http://{[ project.link ]}" target="_blank" class="projectLink"><span class="glyphicon glyphicon-link"></span></a></h3>

                            <p>{[ project.summary ]}</p>
                        </div>
                      </div>

                </div>

            </div>
            <br />
            <div class="clearfix spacer"></div> 
            <h4 class="allProjectsLink" ng-hide="showingAllProjects">
            <a ng-click="getAllProjects()">...click here to see more...</a></h4>

            <h4 class="allProjectsLink" ng-show="showingAllProjects">
            <a ng-click="get6Projects()">---click here to see less---</a></h4>
        </div>
        <br /><br />
        <div class="clearfix spacer"></div> 
    </div>

    <div id="skills" class="container" style="padding-top:60px; margin-top:0;">

        <div class="homeIntro text-left">
            <div class="col-md-2">

            </div>
            <div class="col-md-8 text-center">
                <div class="title">Skills</div>
                <p class="description"> 
                Given my educational and working background, I have aquired competences around the whole porcess of creating a concept, developing and refining it, then design, develop and implement the tools or platforms needed. 
                </p>
                              
                <br />
                <p class="description">
                During my education we have been working in teams to deliver reports and concepts for clients like: WeMind, 3HiG and others. That helped me develop skills like: <br /><b>Concept Development</b>, <b>Brainstorming</b>, <b>Visual Identity</b>, <b>Reporting & Documentation</b>, <b>Pitching & Presentation Design</b>, and others. 
                </p> 

                <br />
                <p class="description">
                Working as a web designer and developer has helped me improve my skills within <b>UX & Web Design</b>, <b>Graphic Design</b>, <b>Illustrator</b>, <b>Photoshop</b>, <b>Angular</b>, <b>jQuery</b>, <b>Laravel</b>, <b>Php & MySQL</b>, and of course <b>HTML5 & CSS3</b>. 
                </p>
                
                <br />
                <p class="description">
                    See a full description of my skills and experience in <a class="darkLink" ng-click="showCV = true">my CV</a>. 
                </p>
                <div class="clearfix spacer"></div>

            </div>
            <div class="clearfix spacer"></div>  

            <div id="cv" ng-show="showCV">
                <div class="clearfix spacer"></div>
                <embed class="cvBox" ng-src="/assets/Andrei Cristian's Résumé.pdf">
            </div>
        </div>
    </div>


    <div id="contact" class="dark-section text-left">
        
        <div class="col-md-2"></div>

        <div class="col-md-8 text-center">
            <div class="title">Contact</div>
            <p class="description"> 
                If you have any job offers, collaboration proposals or questions feel free to <br />contact me on <a style="color:white; font-family:latoLight;" href="mailto:cristian.andrei@live.com">cristian.andrei@live.com</a>. 
            <br /><br />

              <a href="/"><img src="/assets/style/img/white-square-logo.png" style="width:85px; height:auto;"></a>
            </p>
            <p class="description">
                <i>Kind regards,<br /></i>
                Andrei Cristian<br /><br />

            </p>
            <br />
            <div class="clearfix spacer"></div>
            <p class="description">Find me also on..<br />
            <a class="socialIcon" target="_blank"
                                  href="https://dk.linkedin.com/in/andreww3">

                <img src="/assets/img/icons/linkedin.png">
                </a>
            <a class="socialIcon" target="_blank"
                                  href="http://cvideon.com/user/2">
                
                <img src="/assets/img/icons/cvideon.png">
                </a>
            <a class="socialIcon" target="_blank"
                                  href="https://www.behance.net/acesolutions">
                
                <img src="/assets/img/icons/behance.png">
                </a>

            <div class="clearfix spacer"></div>
            <br />
            </p>
        </div>

        <div class="col-md-2"></div>
        <div class="clearfix spacer"></div>  
    </div>
@endsection

@section('footer')
<footer>
    <div class="homeFooter">
        <nav style="text-align:center; padding-top:0px;">
            <a href="/" style="margin-left:12px">

            <img src="assets/style/img/white-logo.png" 
                style="width:180px; margin:0 auto; display:block; margin-top:-10px; "/></a>
            <div class="clearfix"></div>            
            @if (Auth::check()) 
                | -- {{strstr(Auth::user()->name, ' ', true)}} - - 
                <a href="/">Homepage</a> |
                <a href="/dashboard"> Dashboard</a> |
                <a href="/logout"> Log out </a> |
            @else
                | 
                <a href="/contact"> Contact us</a> |
                <a href="/quote"> Get a quote </a> |
            @endif

        </nav>        
    </div>
</footer>
@endsection

@section('javascript')
    <script>
    $(document).ready(function () {

        function goToByScroll (id) {
            id = id.replace("Link", "");

            $('html,body').animate({
                scrollTop: $("#"+id).offset().top},
                'slow');
        }         

        $("header > nav > a").click(function(e) {
            // Prevent a page reload when a link is pressed
            e.preventDefault(); 
            
            // Call the scroll function
            goToByScroll($(this).attr("id"));   
        });
    });
    </script>
@endsection
