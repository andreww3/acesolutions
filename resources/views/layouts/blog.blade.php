<!DOCTYPE html>
<html ng-app="acesolutions">
    <head>
        <meta charset="utf-8">
        <base href="/blog">
        
        <title>eSolutions | @yield('title')</title>


        <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css"
        rel="stylesheet">

        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        @include('shared.favicons')
        <!-- Link to helper stylesheets -->
        <link href="/assets/style/bundle.css" rel="stylesheet" type="text/css">
        <link href="/assets/style/main.css" rel="stylesheet" type="text/css"> 
        <meta name="csrf-token" content="{{ csrf_token() }}" />

        @yield('header')
      
    </head>

    <body>
        @yield('menu')

        @yield('content')

        <div class="clearfix"></div>

        @yield('footer')
        
        <!-- Javacript Helper Packages -->
        <script src="/assets/javascript/bundle.js" type="text/javascript"></script>  
        <script src="/assets/javascript/blogBundle.js" type="text/javascript"></script>
        
        @yield('javascript')

        <!-- Angular Application -->
        <script src="/assets/javascript/app.js" type="text/javascript"></script>   
            
    </body>
</html>