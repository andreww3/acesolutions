<!DOCTYPE html>
<html ng-app="acesolutions">
    <head>
        <meta charset="utf-8">
        <base href="/">

        @include('shared.favicons')
        <!-- Link to helper stylesheets -->
        <link href="/assets/style/bundle.css" rel="stylesheet" type="text/css">
        <link href="/assets/style/main.css" rel="stylesheet" type="text/css"> 
        <meta name="csrf-token" content="{{ csrf_token() }}" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        @yield('header')
        
        <title>Ace Solutions - @yield('title')</title>
    </head>

    <body>
        <div class="a-container">
        @yield('menu')

            @yield('content')

        <div class="clearfix"></div>

        @yield('footer')
        </div>

        <!-- Javacript Helper Packages -->
        <script src="/assets/javascript/bundle.js" type="text/javascript"></script>

        <!-- Angular Application -->
        <script src="/assets/javascript/app.js" type="text/javascript"></script>   

        @yield('javascript')
            
    </body>
</html>