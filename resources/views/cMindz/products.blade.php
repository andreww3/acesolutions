<!DOCTYPE html>
<html ng-app="acesolutions">
<head>
    <meta charset="utf-8">
    <base href="/">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    @include('shared.favicons')
    <!-- Link to helper stylesheets -->
    <link href="/assets/style/bundle.css" rel="stylesheet" type="text/css">
    <link href="/assets/style/main.css" rel="stylesheet" type="text/css"> 
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>
    #content{
    	width: 1260px; margin:0 auto;
    }
    h1{
    	margin-bottom: 0px;
    }
    a {opacity: 0.75;}
    a:hover {opacity: 1;}
    nav#here {
    	padding-top: 0px;
    	margin:0 auto;
    	margin-top: -10px;
    	width:100%;
		border-bottom:1px solid #34495e;
    	text-align:left; padding-bottom:5px;
    }
    nav#here a{
    	padding-bottom:7px;
    	color:#34495e !important;
    }

    nav#here a.onPage{
        opacity: 1;
        border-bottom:1px solid #16a085;
    }

    nav#here a:hover{
    	border-bottom:1px solid #16a085;
    	color:#16a085 !important;
    }
    a#login, a.editProd{
    	float:right;
    	margin-top: -4px;
    }
    .productBox{
        text-align: center;
        width:106%;
        margin-left:-3%;
        margin-bottom:15px;
        background: rgba(255,255,255, .7);

        opacity: .85;
        -webkit-box-shadow: 0.2px 0.1px 0.5px 0.5px gray;
           -moz-box-shadow: 0.2px 0.1px 0.5px 0.5px gray;
                box-shadow: 0.2px 0.1px 0.5px 0.5px gray;
    }
    .productBox:hover{
        background: rgba(255,255,255, .9);
        opacity: 1;
    }
    .productBox h3{
        margin-top:0px;
        padding-top: 15px;
    }
    .productBox p {
        font-size: 18px;
    }
    .productBox form p {
        margin-bottom: 4px;
    }
    .productBox img{
        margin:0 auto;
        margin-top:14px;
        width:90%;
    }
    .productBox .form-control {
        width:90%;
        margin: 0 auto;
    }
    .image-drop-zone { cursor:pointer; width:80%;}
    .addImageBox{ height:110px; }

    </style>
    
    <title>Handcrafted Beads by M'ster Lewis</title>
</head>

<body style="background-image: url('assets/img/beads-bg.png');" 
	ng-controller="productCtrl">
<div id="content">
    <h1 class="text-center"><hr/>Product Editor<hr/></h1>
    <nav id="here">
        <a  ng-class="showNecklaces ? 'onPage' : ''"
            ng-click="showNecklaces = true; showBracelets = false; showAll = false;" 
            >Necklaces</a>

        <a  ng-class="showBracelets ? 'onPage' : ''"
            ng-click="showBracelets = true; showNecklaces = false; showAll = false;" 
            >Bracelets</a>

        <a  ng-class="showAll ? 'onPage' : ''"
            ng-click="showAll = true; showBracelets = false; showNecklaces = false;" 
            >All</a>

    	@if ( Auth::guest() )
			<a href="auth/login" id="login">Login</a>
    	@endif

    	@if ( Auth::check() ) 
        	<a class="editProd" href="logout">Logout</a>
        	@if ( Auth::user()->role == 'cMindz' )
        		<a class="editProd" href="conscious-mindz">Preview</a>
        	@endif
        	<p style="float:right; margin-top: -3px;">
        	Logged in as <b>{{Auth::user()->name}}</b> -- - 
        	</p>
        @endif
	</nav>
    <div class="clearfix spacer"></div>
	
    <!-- Template for adding product -->
    <div class="col-md-3">
        <div class="productBox">
    		<div ng-hide="addingProduct">
                <br/><br/><div class="clearfix spacer"></div>
                <br/><div class="clearfix spacer"></div>
                <hr/>
                <a ng-click="addingProduct = true"><h3>+ Add Product</h3></a>
                <hr/>
                <br/><div class="clearfix spacer"></div>
                <br/><br/><div class="clearfix spacer"></div>
            </div>
     
            <div ng-show="addingProduct">
                <a class=" closeEditService" 
                    ng-click="addingProduct = false"
                    style="margin-right:3%;">
                    <span class="glyphicon glyphicon-remove"></span></a>
                <h3>New</h3>
                <hr/>
                @include('forms.products.add')
            </div>
        </div>
	</div>

    <!-- Template for necklace type products -->
    <div class="col-md-3" ng-show="showNecklaces" ng-repeat="product in products | filter:{ type: 'necklace'}">
        <div class="productBox">    
            <div ng-hide="editingProduct">
                <img ng-if="product.image" ng-src="/{[ product.image ]}" width="100%"/>
                <h3>{[ product.name ]}</h3>
                
                <p>${[ product.price ]}</p>
                <hr/>
                <p><a ng-click="editingProduct = true">
                    Edit ..                  
                    <span class="glyphicon glyphicon-pencil">
                    </span></a>
                </p>
                <hr/>
                <p ng-if="product.description">{[ product.description ]}</p>
            </div>
            
            <div ng-show="editingProduct">
                <a class="closeEditService" 
                    ng-click="editingProduct = false"
                    style="margin-right:3%;">
                    <span class="glyphicon glyphicon-remove"></span></a>

                <a class="deleteServiceBtn" ng-click="deleteProduct(product.id)" style="margin-left:3%;">
                    <span class="glyphicon glyphicon-trash" style="font-size:13px"></span></a>
            
                @include('forms.products.edit')
            </div>
            
        </div>
    </div>

    <!-- Template for bracelet type products -->
    <div class="col-md-3" ng-show="showBracelets" ng-repeat="product in products | filter:{type: 'bracelet'}">
        <div class="productBox">    
            <div ng-hide="editingProduct">
                <img ng-if="product.image" ng-src="/{[ product.image ]}" width="100%"/>
                <h3>{[ product.name ]}</h3>
                
                <p>${[ product.price ]}</p>
                <hr/>
                <p><a ng-click="editingProduct = true">
                    Edit ..                  
                    <span class="glyphicon glyphicon-pencil">
                    </span></a>
                </p>
                <hr/>
                <p ng-if="product.description">{[ product.description ]}</p>
            </div>
            
            <div ng-show="editingProduct">
                <a class="closeEditService" 
                    ng-click="editingProduct = false"
                    style="margin-right:3%;">
                    <span class="glyphicon glyphicon-remove"></span></a>

                <a class="deleteServiceBtn" ng-click="deleteProduct(product.id)" style="margin-left:3%;">
                    <span class="glyphicon glyphicon-trash" style="font-size:13px"></span></a>
            
                @include('forms.products.edit')
            </div>
            
        </div>
    </div>

    <!-- Template for All type products -->
    <div class="col-md-3" ng-show="showAll" ng-repeat="product in products">
        <div class="productBox">    
            <div ng-hide="editingProduct">
                <img ng-if="product.image" ng-src="/{[ product.image ]}" width="100%"/>
                <h3>{[ product.name ]}</h3>
                
                <p>${[ product.price ]}</p>
                <hr/>
                <p><a ng-click="editingProduct = true">
                    Edit ..                  
                    <span class="glyphicon glyphicon-pencil">
                    </span></a>
                </p>
                <hr/>
                <p ng-if="product.description">{[ product.description ]}</p>
            </div>
            
            <div ng-show="editingProduct">
                <a class="closeEditService" 
                    ng-click="editingProduct = false"
                    style="margin-right:3%;">
                    <span class="glyphicon glyphicon-remove"></span></a>

                <a class="deleteServiceBtn" ng-click="deleteProduct(product.id)" style="margin-left:3%;">
                    <span class="glyphicon glyphicon-trash" style="font-size:13px"></span></a>
            
                @include('forms.products.edit')
            </div>
            
        </div>
    </div>
    <div class="clearfix"></div>


<!-- Javacript Helper Packages -->
<script src="/assets/javascript/bundle.js" type="text/javascript"></script>

<!-- Angular Application -->
<script src="/assets/javascript/app.js" type="text/javascript"></script>   

<script type="text/javascript">
	$(document).ready(function(){ 

	});     	
</script>
        
</body>
</html>