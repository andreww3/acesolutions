<!DOCTYPE html>
<html ng-app="acesolutions">
<head>
    <meta charset="utf-8">
    <base href="/">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    @include('shared.favicons')
    <!-- Link to helper stylesheets -->
    <link href="/assets/style/bundle.css" rel="stylesheet" type="text/css">
    <link href="/assets/style/main.css" rel="stylesheet" type="text/css"> 
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>
    .content{
    	margin:0 auto;
    }
    h1{
    	margin-bottom: 0px;
        margin-top: -13px;
        background: rgba(255,255,255, .4);
    }
    a {opacity: 0.75;}
    a:hover {opacity: 1;}
    nav#here {
    	padding-top: 7px;
    	margin:0 auto;
        padding-left: 7px;
    	margin-top: -15px;
    	width:100%;
		border-bottom:1px solid #34495e;
    	text-align:left; padding-bottom:5px;
        background: rgba(255,255,255, .6);
    }
    nav#here a{
    	padding-bottom:9px;
    	color:#34495e !important;
    }
    nav#here a.onPage{
        border-bottom:1px solid #16a085;
        opacity: 1;
    }

    nav#here a:hover{
    	border-bottom:1px solid #16a085;
    	color:#16a085 !important;
    }
    a#login, a#cartBtn{
    	float:right;
    	margin-top: -3px;
    }
    footer{
        width:100%;
        background:transparent;
        position:relative;
        bottom:0;
        border-top: 1px solid black;
    }
    .productBox{
        text-align: center;
        width:106%;
        margin-left:-3%;
        margin-bottom:15px;
        background: rgba(255,255,255, .7);
        opacity: .85;
        -webkit-box-shadow: 0.2px 0.1px 0.5px 0.5px gray;
           -moz-box-shadow: 0.2px 0.1px 0.5px 0.5px gray;
                box-shadow: 0.2px 0.1px 0.5px 0.5px gray;
    }
    .productBox:hover{
        background: rgba(255,255,255, .9);
        opacity: 1;
    }
    .productBox h3{
        margin-top: 15px;
    }
    .productBox p {
        font-size: 18px;
    }
    .productBox img{
        margin:0 auto;
        margin-top:14px;
        width:90%;
    }
    .slide-down{
        position:relative;
        width:100%;
        text-align: center;
        height:470px;
        opacity:.7;
        background-color:white;
        -webkit-box-shadow: 1px 4px 5px 0px rgba(0,0,0,0.75);
        -moz-box-shadow: 1px 4px 5px 0px rgba(0,0,0,0.75);
        box-shadow: 1px 4px 5px 0px rgba(0,0,0,0.75);
    }
    .slide-down .stuff-box{
        width: 100%;
        position: absolute;
        bottom:0;
    }
    .cart-box{
        padding-top: 10px;
        width: 100%;
        margin:0 auto;
    }
    #cart .cart-title {
        margin-left:20px;
        margin-right:20px;
        margin-bottom:-7px;
    }
    #cart h3 span, #cart h4 span {
        opacity: .75;
    }
    #cart h3.text-left span {
        margin-right:7px;
    }
    #cart h4.text-right span {
        margin-right:7px;
        margin-left:7px;
    }
    #cart .item {
        margin-left:7px;
        width:19%;
        float:left;
    }
    #cart .item h3{
        font-size:16px;
        margin-left:0px;
        margin-right:0px;
        margin-top:7px;
        margin-bottom:4px;
    }
    #cart .item p{
        font-size:14px;
        margin-top:0px;
        margin-bottom:-4px;
    }
    #cart .item img{
        margin-top:-10px;
    }

    #cart .checkout ul li{
        list-style-type: none;
    }
    @media screen and (min-width: 980px) /* Desktop */ {
      .content {
        width: 1200px;
      }
    }
    @media screen and (max-width: 500px) /* Mobile */ {
      .content {
        width: 96%;
      }
      h1{
        font-size:24px;
      }
     a#login, a#cartBtn{
        float:none;
     }
      .productBox {
        width:100%;
      }
    }
    </style>
    
    <title>Conscious Mindz</title> 
</head>

<body style="background-image: url('assets/img/beads-bg.png');">
    
    <!-- Template for slide-down screen -->
    <div class="slide-down" style="display: none;">
        <div class="stuff-box">
            <div class="content">
            <div class="col-md-6 text-left">
                
                <h2>Get Title of the Ebook!</h2>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text...</p>
                <h3>Your email</h3>
                <input type="text" class="form-control" style="text-align:center;" placeholder="..."><br/>
                <a class="btn button" style="width:150px;">Submit</a>
                <br /><br />
                <br /><br />

            </div>
            <div class="col-md-1"></div>
            <div class="col-md-5 text-left" style="border-left: 1px solid #eee; padding-left: 25px;">
                <h2>Also</h2>    
                <p>Download my songs at ... and check out my videos at ..<br />.. (social icons here maybe)</p>
                <br />
                <p><i>Signature - motto - or something, <br />
                by Lewis</i></p>
            </div>
            </div>
            <a onclick="closeSlide()">
               <img src="assets/img/slide-close.png" 
               style="width:100%;">
            </a>
        </div>
    </div>

    <div class="content" ng-controller="productCtrl">
    	<a href="conscious-mindz">
            <h1 class="text-center"><hr/>Conscious Mindz<hr/></h1>
    	</a>
    	<nav id="here" class="productBox">
    		<a href id="necklaces">About</a>
    		<a href="conscious-mindz">Store</a>
            <a href>Music</a>

            @if ( Auth::guest() )
            <a href id="cartBtn" onclick="toCart()">
                <b><span style="top:2px;" class="glyphicon glyphicon-shopping-cart"></span></b> 
                ({[cart.count]})</a>
            @endif

            @if ( Auth::check() ) 
            <a class="editProd" href="auth/logout">Logout</a>
            @if ( Auth::user()->role == 'cMindz' )
                <a class="editProd" href="dashboard">Edit products</a>
            @endif
            <p style="float:right; margin-top: -3px;">
            Logged in as <b>{{Auth::user()->name}}</b> -- - 
            </p>
            @endif
    	</nav>
        <div class="productBox cart-box" style="padding-top: 3px;">
        </div>
        <div class="clearfix spacer"></div>

        <!-- Template for products cart -->
        <div id="cart">
            <hr/>
            <div class="col-md-12">
	            <h3 class="text-left cart-title">
	                <span class="glyphicon glyphicon-thumbs-up"></span>
	                Your order has been completed.</h3>
            </div>
            <div class="clearfix"></div>
            <hr/>
            <div class="productBox cart-box">
                <div class="col-md-8">
	                <div ng-show="cart.items.length < 1">
	                    <h4 class="text-left" style="margin:20px;">
	                    Thanks for your order. <br/>The goods will be delivered to you in up do 1 week, depending on your location.</h4>
	                </div>
                </div>

                <div class="col-md-4 text-left">
                    <h3 style="margin-left:-5px;">Your ordered:</h3>
                    <table style="width:90%;">
                        <tr ng-repeat="p in cart.items">
                        <td style="width:70%">{[p.name]}</td>
                        <td class="text-right">${[p.price]}</td>
                        <td></td>
                        <td class="text-right">
                            <a ng-click="rmItem(p.image, p.price)">
                            <span class="glyphicon glyphicon-remove" style="color:#660000;"></span>
                            </a>
                        </tr>
                        <tr ng-show="cart.items.length < 1">
                        <td>Empty</td>
                        <td class="text-right">-</td>
                        </tr>
                        <tr>
                        <td class="text-left"><strong>Total: </strong></td>
                        <td class="text-right"><strong>${[cart.total]}</strong></td>
                        </tr>                     
                    </table>
                </div>
                <br /><br/><div class="clearfix"></div>
                <br/>
                <div class="clearfix"></div>
            </div>
        </div>
        <br /><div class="clearfix spacer"></div>
        <br /><br/>
        <br/><div class="clearfix spacer"></div>

        <footer>
        <nav>
            <a style="float:left; margin-top:-10px;" href="/" target="_blank">
            Created by <img style="width:140px;" src="assets/style/img/b-logo.png" /></a>
            @if ( Auth::guest() )
                <a href="auth/login" id="login">Login</a>
            @endif
            @if ( Auth::check() ) 
                <a class="editProd" href="auth/logout">Logout</a>
                @if ( Auth::user()->role == 'admin')
                    <a class="editProd" href="conscious-mindz/products">Edit products</a>
                @endif
                @if ( Auth::user()->role == 'cMindz' )
                    <a class="editProd" href="dashboard">Edit products</a>
                @endif
                <p style="float:right; margin-top: -3px;">
                Logged in as <b>{{Auth::user()->name}}</b> -- - 
                </p>
            @endif
        </nav>
    </footer>
    </div>

    <div class="clearfix"></div>


<!-- Javacript Helper Packages -->
<script src="/assets/javascript/bundle.js" type="text/javascript"></script>

<!-- Angular Application -->
<script src="/assets/javascript/app.js" type="text/javascript"></script>   

<script type="text/javascript">
	$(document).ready(function(){ 
        /*
        setTimeout(function(){
            $('.slide-down').slideToggle("slow");
        }, 1000);
	   */
    });     	
</script>
        
</body>
</html>