<style type="text/css">

.db-pricing-eleven ul {
    list-style: none;
    margin: 0;
    text-align: center;
    padding-left: 0px;
}
.db-pricing-eleven ul li {
    padding-top: 10px;
    padding-bottom: 10px;
    cursor: pointer;
}
.db-pricing-eleven ul li i {
    margin-right: 5px;
}
.db-pricing-eleven .price {
    background-color: rgba(0, 0, 0, 0.5);
    padding: 40px 20px 20px 20px;
    font-size: 60px;
    font-weight: 900;
    color: #FFFFFF;
}
.db-pricing-eleven .price small {
    color: #B8B8B8;
    display: block;
    font-size: 12px;
    margin-top: 22px;
}
.db-pricing-eleven .type {
    background-color: #52E89E;
    padding: 40px 10px;
    font-weight: 900;
    text-transform: uppercase;
    font-size: 30px;
}
.db-pricing-eleven .pricing-footer {
    padding: 10px;
}
.db-pricing-eleven.popular {
    margin-top: 10px;
}
.db-pricing-eleven.popular .price {
	padding-top: 50px;
}
</style>
<div class="container">
   <div class="row text-center">
        <div class="col-md-12">
            <h3>Payment Using Paypal</h3>
        
            <div class="row db-padding-btm db-attached">
                <div class="db-wrapper">
                    {!! Form::open(array('route' => 'getCheckout')) !!}
                        
                        <input type="hidden" name="pay" ng-value="cart.total">
                        <input type="text" name="items[]" ng-repeat="p in cart.items"" ng-value="p.name"/>

                        <div s="db-pricing-eleven db-bk-color-one">
                            <div class="price">
                                <span class="text-left">Total:</span>
                                <span class="text-right">
                                    <sup>$</sup>{[cart.total]}                                
                                </span>
                            </div>
                            <div class="type">
                                Items
                            </div>
                            <ul>
                                <li><i class="glyphicon glyphicon-print"></i>30+ Accounts </li>
                                <li><i class="glyphicon glyphicon-time"></i>150+ Projects </li>
                                <li><i class="glyphicon glyphicon-trash"></i>Lead Required</li>
                            </ul>
                            <div class="pricing-footer">
                                <button class="btn db-button-color-square btn-lg">BOOK ORDER</button>
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>

</div>