<div class="col-md-6">
    <h3 class="text-left cart-title">
        <span class="glyphicon glyphicon-check"></span>
        Checkout with PayPal</h3>
</div>
<div class="col-md-6">
    <a onclick="toTop()">
        <h4 class="text-right cart-title">
            <span class="glyphicon glyphicon-menu-up"></span>
            Still up for shopping 
            <span class="glyphicon glyphicon-menu-up"></span>
        </h4>
    </a>
</div>
<div class="clearfix"></div>
<hr/>
<div class="productBox cart-box">
    <div class="col-md-6">
        <h4> Delivery address </h4>
        <input type="text" ng-model="address" />
    </div>
    <div class="col-md-6">
         @include('cMindz.checkout.payment')
    </div>
    <div class="clearfix spacer"></div>
</div>