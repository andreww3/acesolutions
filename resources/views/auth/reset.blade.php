@extends('layouts.default')

@section('title', 'Reset password')

@section('content')

<div id="home-bg-cover" style="background-image:url('/assets/images/video-frame.png'); background-size:cover; height:600px;">
</div>

<nav class="navbar navbar-default">
	<div class="container-fluid">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header ">
			<button type="button" class="navbar-toggle collapsed home-menu-top-margin" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<div class="navbar-brand col-lg-3 col-md-3 col-sm-3 hidden-xs">
			<a href="{{ url('/') }}"><img class="img-responsive" src="/assets/images/CVideon-logo-white.png"></a></div>
		</div>

		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav navbar-right home-menu-top-margin">
				<li><a href="#login-box" class="login-window">Log in</a></li>
			</ul>
		</div><!-- /.navbar-collapse -->
	</div><!-- /.container-fluid -->
</nav>

<div class="clearfix spacer"></div>
<div class="clearfix spacer"></div>

    @include('auth.login-panel')

<div class="container-fluid" style="min-height:240px;">
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<div class="panel panel-default register-panel">
				<div class="panel-heading">Reset Password</div>

				<div class="panel-body">
					@if (count($errors) > 0)
						<div class="alert alert-danger">
							<strong>Whoops!</strong> There were some problems with your input.<br><br>
							<ul>
								@foreach ($errors->all() as $error)
									<li>{{ $error }}</li>
								@endforeach
							</ul>
						</div>
					@endif

					<form class="form-horizontal" role="form" method="POST" action="{{ url('/password/reset') }}">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<input type="hidden" name="token" value="{{ $token }}">

						<div class="form-group">
							<label class="col-md-4 control-label">E-Mail Address</label>
							<div class="col-md-6">
								<input type="email" class="form-control" name="email" value="{{ old('email') }}">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Password</label>
							<div class="col-md-6">
								<input type="password" class="form-control" name="password">
							</div>
						</div>

						<div class="form-group">
							<label class="col-md-4 control-label">Confirm Password</label>
							<div class="col-md-6">
								<input type="password" class="form-control" name="password_confirmation">
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-6 col-md-offset-4">
								<input type="submit" class="button" value="Save new password">

								</input>
							</div>
						</div>
					</form>
				</div>			
			</div>
		</div>
	</div>
</div>

<div class="clearfix spacer"></div>
@endsection

@section('footer')

	@include('shared.footer')

@endsection

@section('javascript')
	<script>
		$(document).ready(function() {
			$('a.login-window').click(function() {

				//Getting the variable's value from a link
				var loginBox = $(this).attr('href');

				//Fade in the Popup
				$(loginBox).fadeIn(300);

				//Set the center alignment padding + border see css style
				var popMargTop = ($(loginBox).height() + 24) / 2;
				var popMargLeft = ($(loginBox).width() + 24) / 2;

				$(loginBox).css({
					'margin-top' : -popMargTop,
					'margin-left' : -popMargLeft
				});

				// Add the mask to body
				$('body').append('<div id="mask"></div>');
				$('#mask').fadeIn(300);

				return false;
			});

			// When clicking on the button close or the mask layer the popup closed
			$('a.close-login, #mask').on('click', function() {
				$('#mask , .login-popup').fadeOut(300 , function() {
					$('#mask').remove();
				});
				return false;
			});

		});
	</script>
@endsection