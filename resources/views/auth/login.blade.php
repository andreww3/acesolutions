@extends('layouts.main')

@section('title', 'Login')

@section('header')
	<!-- Fonts -->
	<link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>
@endsection

@section('menu')
    <header>
        <div class="mainHeader">
            <nav>
				<a href="/"><img class="menuLogo" src="assets/style/img/logo.png" /></a>
            	-- -
                <a href="register"> Register </a> |
                <a href="quote"> Get a quote</a> |
            </nav>
        </div>
    </header>
	<div class="clearfix spacer"></div>
	<div class="spacer"></div>
@endsection

@section('content')
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-6 col-md-offset-3">
				<div class="panel panel-default loginPannel">
					<div class="panel-heading text-center">Login</div>
					<div class="panel-body">

						@include('errors.list')

						<form class="form-horizontal" role="form" method="POST" action="{{ url('login') }}">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">

							<div class="form-group">
								<input type="email" class="form-control" name="email" value="" placeholder="Your email address">
							</div>

							<div class="form-group">
								<input type="password" class="form-control" name="password" placeholder="Your password">
								<label style="float:right; font-family:'latoLight'; padding-top:7px; margin-right:5px;">
									<input type="checkbox" name="remember"> Remember Me
								</label>
								<input type="submit" class="btn btn-md authBtn" value="Login" style="margin-top:20px;">	<br />
								<a class="" style="float:right; font-family:'latoLight'; margin-top:7px; margin-right:5px;" href="{{ url('/password/email') }}">Forgot Your Password?</a>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection 

@section('footer')

    @include('shared.footer')
    
@endsection