@extends('layouts.main')

@section('title', 'Retrieve Password')

@section('header')
	<!-- Fonts -->
	<link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>
@endsection

@section('menu')
    <header>
        <div class="mainHeader">
            <nav>
				<a href="/"><img class="menuLogo" src="assets/style/img/logo.png" /></a>
            	-- -
                <a href="auth/register"> Register </a> |
                <a href="/quote"> Get a quote</a> |
            </nav>
        </div>
    </header>
	<div class="clearfix spacer"></div>
	<div class="spacer"></div>
@endsection

@section('content')

<div id="home-bg-cover">

	<div class="clearfix spacer"></div>
	<div class="container-fluid" style="min-height:450px;">
		<div class="row">
			<div class="col-md-6 col-md-offset-3">
				<div class="panel panel-default  loginPannel">
					<div class="panel-heading" style="text-align:center;">Reset password</div>
					<div class="panel-body">
						@if (count($errors) > 0)
							<div class="alert alert-danger">
								<strong>Whoops!</strong> There were some problems with your input.<br><br>
								<ul>
									@foreach ($errors->all() as $error)
										<li>{{ $error }}</li>
									@endforeach
								</ul>
							</div>
						@endif

						<form class="form-horizontal" role="form" method="POST" action="{{ url('/auth/login') }}">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">

							<div class="form-group">
								<label class="col-md-4 control-label">E-Mail Address</label>
								<div class="col-md-6">
									<input type="email" class="form-control" name="email" value="{{ old('email') }}">
								</div>
							</div>

							<div class="form-group">
							    <div class="col-md-4"></div>
							    <div class="col-md-6">
									<input type="submit" class="btn btn-md authBtn" value="Send reset link">
								</div>
								<br />
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection 
 
@section('footer')

	    @include('shared.footer')
	    
@endsection
