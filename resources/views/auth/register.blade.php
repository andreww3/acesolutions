@extends('layouts.main')

@section('title', 'Registration')

@section('header')
	<!-- Fonts -->
	<link href='//fonts.googleapis.com/css?family=Roboto:400,300' rel='stylesheet' type='text/css'>
	<style>
	.form-control{text-align: center;}
	</style>
@endsection



@section('menu')
    <header>
        <div class="mainHeader">
            <nav>
				<a href="/"><img class="menuLogo" src="assets/style/img/logo.png" /></a>
            	-- - <a href="quote"> Get a quote</a> |
            	<a ng-click="clientRegistration = true"
            		ng-hide="clientRegistration">Client registration</a>
            	<a ng-click="clientRegistration = false"
            		ng-show="clientRegistration">Sign up as user</a> |

            	<a href="login"> Log in </a> |
            </nav>
        </div>
    </header>
	<div class="clearfix spacer"></div>
	<div class="spacer"></div>
@endsection

@section('content')
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-3"></div>
			<div class="col-md-6">

				<div ng-hide="clientRegistration" 
					class="panel panel-default registerPanel">
					<div class="panel-heading text-center">Registration</div>
					<div class="panel-body">
						<div class="col-md-10 col-md-push-1">
							@include('errors.list')
												
							<form name="userSignUpForm" class="form-horizontal" role="form" method="POST" action="{{ url('register') }}">
								<input type="hidden" name="_token" value="{{ csrf_token() }}">
								<input type="hidden" name="role" value="cMindz">
								<div class="form-group">
									<input type="text" class="form-control" name="name" value="{{ old('name') }}"
		  							placeholder=".. your name .." 
		  							required>
								</div>
								<div class="form-group">
									<input type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder=".. emaill address .." 
									required>
								</div>
								<div class="form-group">
									<input type="password" class="form-control" name="password"
										placeholder=".. password .." 
										required>
								</div>
								<div class="form-group">
									<input type="password" class="form-control" name="password_confirmation"
										placeholder=".. repeat password .." 
										required>
								</div>
								<div class="spacer"></div>
								<div class="form-group text-center">
									<input type="submit" class="btn btn-md authBtn" value="Register">
								</div>
							</form>
						</div>
					</div>
				</div>
				
				<div ng-show="clientRegistration" 
					class="panel panel-default registerPanel">
					<div class="panel-heading text-center">Register as client</div>
					<div class="panel-body">
						<div class="text-center">
							<p >Get a client account to receive support for your website.</p>
						</div> 
						<div class="col-md-10 col-md-push-1">
							@include('errors.list')
												
							<form name="userSignUpForm" class="form-horizontal" role="form" method="POST" action="{{ url('/auth/register') }}">
								<input type="hidden" name="_token" value="{{ csrf_token() }}">
								<input type="hidden" name="role" value="client">
								<div class="form-group">
									<input type="text" class="form-control" name="company" value="{{ old('company') }}"
		  							placeholder=".. company .." 
		  							required>
								</div>
								<div class="form-group">
									<textarea type="text" class="form-control" name="description" value="{{ old('description') }}"
		  							placeholder=".. a short description of the company .." 
		  							required></textarea>
								</div>
								<div class="form-group">
									<input type="text" class="form-control" name="website" value="{{ old('website') }}"
		  							placeholder=".. website .." 
		  							required>
								</div>
								<div class="form-group">
									<input type="text" class="form-control" name="name" value="{{ old('name') }}"
		  							placeholder=".. your name .." 
		  							required>
								</div>
								<div class="form-group">
									<input type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder=".. emaill address .." 
									required>
								</div>
								<div class="form-group">
									<input type="phone" class="form-control" name="phone" value="{{ old('phone') }}" placeholder=".. phone number .." 
									required>
								</div>
								<div class="form-group">
									<!--<div class="col-md-12">
										<select class="form-control" name="country"	required>
											<option value default>select a country..</option>
											<option ng-repeat="country in allCountries" ng-value="country.id">
												{[country.name]}
											</option>
										</select>
									</div>-->
									<div class="col-md-6">
										<input type="text" class="form-control" name="country" value="{{ old('email') }}" placeholder=".. country ..">	
									</div>
									<div class="col-md-6">
										<input type="text" class="form-control" name="city" value="{{ old('email') }}" placeholder=".. city ..">	
									</div>
								</div>
								<div class="form-group">
									<input type="password" class="form-control" name="password"
										placeholder=".. password .." 
										required>
								</div>
								<div class="form-group">
									<input type="password" class="form-control" name="password_confirmation"
										placeholder=".. repeat password .." 
										required>
								</div>
								<div class="spacer"></div>
								<div class="form-group text-center">
									<input type="submit" class="btn btn-md authBtn" value="Register">
								</div>
							</form>
						</div>
					</div>
				</div>
				<div class="visible-xs clearfix spacer"></div>
			</div>
		</div>
	</div>

@endsection

@section('footer')

    @include('shared.footer')
    
@endsection