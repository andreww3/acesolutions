<h5>
From: <b> {{ $name }} </b>
</h5>

<h5>
Email: <b>{{ $email }}</b>
</h5>
<br />
<h4>
Subject: <b> {{ $subject }} </b>
</h4>

<p>
{{ $bodyMessage }}
</p>