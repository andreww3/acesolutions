<!doctype html>
<html lang="da" ng-app="acesolutions" ng-controller="missioCtrl">
	<head>
		<title>Missio | Dreams</title>
		
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="/assets/style/bundle.css" rel="stylesheet" type="text/css">
        <link href="/assets/style/main.css" rel="stylesheet" type="text/css">

		<script src="assets/javascript/bundle.js"></script>
		<script src="assets/javascript/app.js"></script>				
	</head>
	
	<body class="missio">

		<header>
			<div class="container">

				<img class="topHeart" src="assets/style/missio/logoheart.jpg" />
				<img class="topLogo" src="assets/style/missio/logo.jpg" />

				<img class="topSlogan" src="assets/style/missio/slogan.jpg" />

			</div>
			<div class="clearfix"></div>
		</header>
		<br />
		<div class="clearfix spacer"></div>

		<div class="container">		

			<h1>. .. ... Dreams ... .. .</h1>

			<div class="dream-box" ng-repeat="dream in dreams">
				<br/>
				<div class="dream-dream"><span> <i>"{[ dream.dream ]}" </i></span></div>
				<hr>
				<p style="font-size:12px; opacity: 0.6;"><i>Written on {[ dream.created_at ]}, by <b>{[ dream.dreamer ]}</b></i></p>				
			</div>
			<div class="clearfix"></div>

			<div class="dreams-bottom">
				<span> 
					<a href ng-click="getLatestDreams()">Latest</a> |
					<a href ng-click="getRandomDreams()">Random</a> |
					<a href ng-click="getAllDreams()">All</a>
				</span>
			</div>
		</div>	
		<div class="clearfix spacer"></div>

		<div class="container">
			<div class="col-md-1"></div>
			<div class="col-md-10">
				<h2>Add your own dream</h2>
				<br />
				<input ng-model="newDream.dreamer" type="text" class="form-control text-center" placeholder="What's your name?" style="padding-bottom:8px;"><br />
				<textarea ng-model="newDream.dream" class="form-control text-center" placeholder="Write your dream here.." style="min-height:130px;"></textarea><br />

				<button ng-click="addDream(newDream)" class="button saveDreamBtn">
				Add Your Dream
				</button> 
				<br /><br /><br />
			</div>
			<div class="col-md-1"></div>
		</div>	
		<br />
		<div class="clearfix spacer"></div>
		<br /><div class="clearfix spacer"></div>
		<br /><br /><div class="clearfix spacer"></div>
		<br /><br /><div class="clearfix spacer"></div>
		
		<footer>
			<div class="container"style="margin-top:40px; margin-bottom: 30px;">
				
				<div class="col-md-6">
					
					
	
					<a href="http://missio.dk/" target="_blank">
					
						<div class="clearfix"></div>
						<img style="height:30px; width:auto; float:left; margin-left:-3px" src="assets/style/missio/logoheart.jpg" />
						<img style="height:30px; width:auto; float:left; margin-left:5px" src="assets/style/missio/logo.jpg">
						<img style="height:60px; width:auto; float:left; margin-top:-25px" src="assets/style/missio/slogan.jpg" />
					</a>
				</div>
				
				<div class="col-md-6">
					<p style="float:right">Designed & Developed by</p>
					<div class="clearfix"></div>
					<a href="http://acesolutions.co" target="_blank">
						<img style="height:45px; width:auto; float:right; margin-right:-15px; margin-top:-10px" src="assets/style/img/logo.png">
					</a>
				</div>
			</div>
		</footer>
	</body>
</html>