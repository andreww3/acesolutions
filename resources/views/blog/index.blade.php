@extends('layouts.blog')

@section('title', 'Blog')

@section('header')

@endsection

@section('menu')
    
    @include('shared.header')

@endsection

@section('content')

    <div class="container" ng-controller="blogCtrl">
            
            <div class="contactForm">
                <div class="title">{{ config('blog.title') }}</div>
                
                <p class="description"> 
                    {{ config('blog.description') }}  
                </p>
            </div>

            <div class="text-left">
                <div class="col-md-12 text-center">
                    <div class="postBox">
                        <h4> 
                        
                        <span ng-repeat="category in categories">
                             <a class="categoryLink" href ng-click="changeCategory( category.id )">
                                {[ category.title ]}</a>
                        </span>

                        </h4>
                        <div class="clearfix spacer"></div>
                    </div>
                </div>

                <div class="col-md-6" ng-repeat ="post in posts">
                    <div class="postBox">
                        <a class="darkLink" ng-href="/blog/{[ post.slug ]}">
                            <h3>
                            {[ post.title | limitTo:40 ]}...
                            </h3>
                        </a>
                        <hr>

                        <p style="min-height:110px" ng-bind-html="(post.content_html | limitTo:300) + '...'">
                                            
                        </p>
                        <div class="clearfix"></div>
                        <hr>
                        <a class="coloredLink" ng-href="/blog/{[ post.slug ]}">Read more...</a>                      
                        <em style="float:right;">Published on {[ post.published_at | date:'longDate']}</em>
                    </div>
                </div>

              <div class="clearfix"></div>
            </div>
            <hr style="margin-bottom:6px; margin-top:2px">
            <div class="pageNumber"></div>
        </div>
        <div class="clearfix"></div>

    </div>
    <div class="clearfix spacer"></div>

@endsection

@section('footer')

    @include('shared.footer')
    
@endsection

@section('javascript')
    <script>
    $(document).ready(function(){
        // Add Script for scrolling to contact form            
    });
    </script>
@endsection
