@extends('layouts.blog')

@section('title', $post->title)

@section('header')

@endsection

@section('menu')
    
    @include('facade.shared.header')

@endsection

@section('content')

    <div class="container">
      <div class="col-md-8 text-left">
        <div class="inPostBox">
          <h2>{{ $post->title }}</h1>
          <hr>

            <p>{!! html_entity_decode($post->content) !!}</p>
                  
          <hr>
          <h5>Published on <em>the {{ $post->published_at->format('jS') }} of {{ $post->published_at->format('F Y') }} </em> by ..
          <span style="float:right">Share the article on..</span>
          </h5>
        </div>
      </div>

      <div class="col-md-4 sidebar">
        <div class="postBox">
          <h3>Related articles</h3>
          <i>In development...</i>
          <hr><br/>
          <h3>Latest articles</h3>
          <i>In development...</i>
          <hr><br/>
          <h3>Categories</h3>
          <i>In development...</i>
          <hr><br/>
        </div>
      </div>
    </div>
        <div class="clearfix"></div>

    </div>
    <div class="clearfix spacer"></div>

@endsection

@section('footer')

    @include('facade.shared.footer')
    
@endsection

@section('javascript')
    <script>
    $(document).ready(function(){
        // Add Script for scrolling to contact form            
    });
    </script>
@endsection

<head>
  <title></title>
  <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css"
        rel="stylesheet">
</head>
<body>

</body>
</html>