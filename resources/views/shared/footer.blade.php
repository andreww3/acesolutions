<div class="clearfix spacer"></div>
<footer style="border-top: 3px solid #34495e;">
    <div class="homeFooter text-right">
        <nav>
            <a href="/"><img class="footerMenuLogo" src="assets/style/img/white-logo.png" /></a>
        	@if ( Auth::user() ) 
                |<a href="dashboard"> Dashboard</a> 
                - -- {{strstr(Auth::user()->name, ' ', true)}} - -- 
            	<a href="logout"> Log out </a> |
            @endif

            @if ( Auth::guest() )
                <a href="about"> About </a> |
                <a href="blog"> Blog </a> |                
                <a href="contact"> Contact </a> |
                <a href="login"> Log in </a> |
            @endif
        </nav>        
    </div>
</footer>