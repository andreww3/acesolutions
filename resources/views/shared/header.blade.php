<header>
    <div class="mainHeader">
     
      <nav>
        <a href="/"><img class="menuLogo" src="assets/style/img/logo.png" /></a>
        <p class="slogan">. .. make the best out of iT.</p>

        @if ( Auth::guest() )
            <a href="about"> About </a> |
            <a href="services"> Services </a> |
            <a href="projects"> Projects </a> |
            <a href="blog"> Blog </a> |
        @endif

        @if ( Auth::check() ) 
            <!--Hi, {{strstr(Auth::user()->name, ' ', true)}}-->  
            @if ( Auth::user()->role == 'admin')
                <a href="dashboard"> Dash </a> |
                <a href="dashboard/users"> Users </a> |
                <a href="dashboard/tasks"> Tasks </a> |
                <a href="dashboard/blog"> Blog </a> |
            
            @elseif ( Auth::user()->role == 'client')
                <a href="tickets-dash"> Tickets </a> |
                <a href="services-dash"> Services </a> |
                <a href="blog-client"> Blog </a> |
            
            @elseif ( Auth::user()->role == 'member')
                <a href="projects-dash"> Projects </a> |
                <a href="tickets-dash"> Tickets </a> |
                <a href="services-dash"> Services </a> |
                <a href="blog-dash"> Blog </a> |
            @endif
            <a href="logout"> Log out </a>            
        @endif
      </nav>

    </div>
</header>