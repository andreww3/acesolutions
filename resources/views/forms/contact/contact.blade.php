{!! Form::open(array('route' => 'contact_store', 'class' => 'form')) !!}

    <div class="form-group row">
        {!! Form::text('name', null, 
            array( 
                  'class'=>'form-control', 
                  'placeholder'=>'Your full or first name')) !!}
    </div>

    <div class="form-group row">
        {!! Form::text('email', null, 
            array( 
                  'class'=>'form-control', 
                  'placeholder'=>'Your E-mail Address')) !!}
    </div>

    <div class="form-group row">
        {!! Form::text('subject', null, 
            array( 
                  'class'=>'form-control', 
                  'placeholder'=>'What is this about?')) !!}
    </div>

    <div class="spacer"></div>

    <div class="form-group row">
        {!! Form::textarea('message', null, 
            array( 
                  'class'=>'form-control', 
                  'placeholder'=>'What I can help you with? Would you like to give me any feedback on my work? Or would you like me to look through one of your projects and give you some feedback? ')) !!}
    </div>
    
    <div class="form-group row text-right">
        <h4><i>* If you'd like to get feedback on one of your projects include a link to your project. If you're writing about something you need to get done for acheiving your business objectives <a style="opacity:0.7" href="/quote">get a quote here</a>.</i></h4>
    </div>

    <div class="form-group text-right">

            {!! Form::submit('Send message!', 
              array('class'=>'btn btn-primary btn-lg')) !!}
    </div>


{!! Form::close() !!}