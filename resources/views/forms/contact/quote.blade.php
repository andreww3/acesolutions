{!! Form::open(array('route' => 'quote_store', 'class' => 'form')) !!}

    <div class="form-group row">
        {!! Form::text('name', null, 
            array( 
                  'class'=>'form-control', 
                  'placeholder'=>'Your first or fullname')) !!}
    </div>

    <div class="form-group row">
        {!! Form::text('email', null, 
            array( 
                  'class'=>'form-control', 
                  'placeholder'=>'What is your email address?')) !!}
    </div>
	<div class="spacer"></div>
    
    <div class="form-group row">
        {!! Form::text('projectName', null, 
            array( 
                  'class'=>'form-control', 
                  'placeholder'=>'What is the name of your Project/Business?')) !!}
    </div>

    <div class="form-group row">
	    {!! Form::textarea('description', null, 
	        array( 
	              'class'=>'form-control', 
	              'placeholder'=>'How would you shortly descripe your project/business/brand?')) !!}
    </div>

    <div class="form-group row">
	    {!! Form::textarea('task', null, 
	        array( 
	              'class'=>'form-control', 
	              'placeholder'=>'What is the task you are trying to accomplish?')) !!}
    </div>

    <div class="form-group row">
        {!! Form::text('budget', null, 
            array( 
                  'class'=>'form-control', 
                  'placeholder'=>'What is the budget for the task given?')) !!}
    </div>

	<div class="spacer"></div>

    <div class="form-group row">
	    {!! Form::textarea('comment', null, 
	        array( 
	              'class'=>'form-control', 
	              'placeholder'=>'Any aditional comments?')) !!}
    </div>
    
    <div class="form-group row">
    	<h4><i>* A quote represents an estimate in time and budget for a given task. If you'd like to get feedback on one of your projects send me a <a class="darkLink"style="opacity:0.7" href="contact">message here</a>, including a link to your project.</i></h4>
    </div>

    <div class="spacer"></div>
    <div class="form-group">

            {!! Form::submit('Get a quote!', 
              array('class'=>'btn btn-primary btn-lg')) !!}

    </div>
    <div class="spacer"></div>
{!! Form::close() !!}