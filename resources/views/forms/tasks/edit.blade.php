{!! Form::open(array('route' => 'addProject', 'class' => 'editProjectForm')) !!}

    <div class="form-group row">
        {!! Form::text('title', null, 
            array( 
                  'class'=>'form-control', 
                  'placeholder'=>'The title of the project',
                  'ng-value'=>'project.title')) !!}
    </div>

    <div class="form-group row">
        {!! Form::textarea('summary', null, 
            array( 
                  'class'=>'form-control', 
                  'placeholder'=>'A summary of the project. It could be the goal or a description of the problem you are trying to solve.',
                  'ng-model'=>'project.summary')) !!}
    </div>
    
    <div class="form-group row">
        {!! Form::text('link', null, 
            array( 
                  'class'=>'form-control', 
                  'placeholder'=>'The link if any',
                  'ng-model' => 'project.link',
                  'ng-value'=>'project.link')) !!}
    </div>

    <div class="clearfix"></div>

    <div ng-if="project.image" ng-hide="imageRemoved">
      <h3>Chosen image</h3>
      
      <img ng-src="/{[project.image]}" class="serviceIcon">
      
      <div class="clearfix"></div>
      <button type="button" class="btn btn-warning btn-sm" ng-click="removeProjectImage(project.id, project.image)">
          <span class="glyphicon glyphicon-trash"></span> Remove
      </button>
      <div class="clearfix spacer"></div>
    </div>

    <div class="form-group row addImageBox" ng-if="addImage" ng-show="addImage">
      <div ng-hide="uploader.queue.length || project.image" ng-show="imageRemoved">
          <div ng-show="uploader.isHTML5">
              <!-- Example: nv-file-drop="" uploader="{Object}" options="{Object}" filters="{String}" -->
              <div nv-file-drop="" uploader="uploader">
                  <div nv-file-over="" uploader="uploader" ng-model="projectImage" over-class="another-file-over-class" class="well my-drop-zone image-drop-zone" ng-click="fileChooser()">
                      <i>Drag in a image.. (500x600px)</i>
                  </div>
                  {[iconImage]}
              </div>
              <input class="hidden" type="file" id="fileSelect" nv-file-select="" uploader="uploader" multiple />
          </div>
          <div class="clearfix spacer"></div><div class="spacer"></div>
      </div>


      <div ng-show="uploader.queue.length">
          <h3>Chosen Image</h3>
          <!-- Uploaded Icon & Options -->
          <div ng-repeat="item in uploader.queue">
              <div class="iconBox" ng-show="uploader.isHTML5" ng-thumb="{ file: item._file, height: 150 }"></div>
              <div class="clearfix spacer"></div>

              <p><b>Filename: </b> {[ item.file.name ]}</p>
              <p><b>Filesize: </b> {[ item.file.size/1024/1024|number:2 ]} MB</p>
              <div class="clearfix spacer"></div>
              <input type="hidden" name="_token" value="{!! csrf_token() !!}" ng-model="item._file._token">
              
              <button type="button" class="btn btn-success btn-sm" ng-click="item.upload()" ng-hide="item.isReady || item.isUploading || item.isSuccess">
                  <span class="glyphicon glyphicon-upload"></span> Upload
              </button>
              <button type="button" class="btn btn-warning btn-sm" ng-click="item.remove()" >
                  <span class="glyphicon glyphicon-trash"></span> Remove
              </button>
              <div class="clearfix spacer"></div>

              <h4> 
                <span ng-show="item.isUploading">
                  <i class="glyphicon glyphicon-send"></i>

                  <div class="clearfix"></div>

                  <div class="form-control" ng-show="item.isUploading"
                    style="background-color:transparent; color:white;
                            border:0px; box-shadow: none;">
                      Progress:
                      <div class="progress" style="">
                          <div class="progress-bar" role="progressbar" ng-style="{ 'width': uploader.progress + '%' }"></div>
                      </div>
                      <div class="clearfix spacer"></div>
                  </div>
                </span>

                <span ng-show="item.isSuccess"> 
                  <i class="glyphicon glyphicon-ok"></i><br/>
                  <input type="hidden" name="image" ng-value="item.file.name" ng-model="project.image">
                  </span>
                <span ng-show="item.isCancel">
                  <i class="glyphicon glyphicon-ban-circle"></i></span>
                <span ng-show="item.isError">
                  <i class="glyphicon glyphicon-remove"></i></span>
              </h4>
          </div><!--end uploaded icon-->
        </div><!--end chosen icon-->
    </div>
    
    <div class="spacer"></div>
    <div class="form-group row" ng-show="addDescription">

        {!! Form::textarea('description', null, 
            array( 
                  'class'=>'editor form-control', 
                  'placeholder'=>'Contents of the project. This feature will be used when TinyMce will be implemented on the blog.',
                  'ng-model'=>'project.description')) !!}
    </div>

    <div class="form-group row" ng-show="selectDates" style="margin-bottom:25px;">
        <div class="datePickers">
        <!-- UI Bootstrap Datepicker -->
          <div style="width:43%; float:left;">
              <h4>Starting date</h4>
              <div class="input-group">
              <uib-datepicker ng-model="project.started_at" class="well well-sm" datepicker-options="inlineOptions"></uib-datepicker>
              <input type="hidden" name="started_at" uib-datepicker-popup="{[format]}" ng-model="project.started_at"> 
              </div>

          </div>

          <div style="width:43%; margin-left:4%; float:left;">
              <h4>Delivery date</h4>
              <div class="input-group">
              <uib-datepicker ng-model="project.delivered_at" class="well well-sm" datepicker-options="inlineOptions"></uib-datepicker>
              <input type="hidden" name="delivered_at" uib-datepicker-popup="{[format]}" ng-model="project.delivered_at">
              </div>
          </div>
        <br />
        </div>
        <div class="clearfix spacer"></div>
    </div>


    <div class="form-group row extra-options">
      <a class="extra-option-link" ng-click="openImageUploader()" ng-hide="addImage">
          + Image
      </a>

      <a class="extra-option-link" ng-click="selectDates = true" ng-hide="selectDates"> 
          + Dates
      </a>

      <a class="extra-option-link" ng-click="addDescription = true" ng-hide="addDescription"> 
          + Content
      </a>

    </div>
    <br />

    <div class="clearfix spacer"></div><div class="spacer"></div>
    <div class="form-group text-center" style="margin-bottom:0px !important;">
        <div class="spacer"></div>
        <div class="spacer clearfix"></div>        
        <a class="serviceBtn saveServiceBtn" ng-click="saveProject(project.id, project.title, project.summary, project.image, project.link, project.description, project.started_at, project.delivered_at)">
            <span class="glyphicon glyphicon-floppy-saved" style="font-size:20px;">
            </span>
        </a>
    </div>


{!! Form::close() !!}