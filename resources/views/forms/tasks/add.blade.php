{!! Form::open(array('route' => 'addTask', 'class' => 'addProjectForm')) !!}
	
    <div class="form-group row">
      <input type="hidden" name="initiator" value="{{ Auth::user()->id }}">

        {!! Form::text('title', null, 
            array( 
                  'class'=>'form-control', 
                  'placeholder'=>'The title or subject..')) !!}
    </div>

    <div class="form-group row">
      <select class="form-control" name="type">
    		<option value>Select the type of the ticket..</option>
    		<option>Website Update</option>
    		<option>Design</option>
    		<option>Bug fix</option>
    		<option>Consultance</option>
    	</select>
    </div>

    <div class="form-group row">
      <label class="form-label" for="description">Summary:</label>
        {!! Form::textarea('description', null, 
            array( 
                  'class'=>'form-control', 
                  'placeholder'=>'Describe in as much detail as possible what you would like to achieve, and how you think it should be done. Remember to add links and attach screenshots if necessary..')) !!}
    </div>

    <div class="form-group row extra-options">
    <br/>
      <a class="extra-option-link" ng-click="selectDates = true" ng-hide="selectDates"> 
          + Deadline
      </a>

      <a class="extra-option-link" ng-click="openImageUploader()" ng-hide="addImage">        
          + Attachment
      </a>

    </div>
    <div class="form-group row">
    <br />
        <div class="datePickers" ng-show="selectDates">
        <!-- UI Bootstrap Datepicker -->
            <h4>Deadline</h4>
            <div class="input-group">
            <uib-datepicker ng-model="task.deadline" class="well well-sm" datepicker-options="inlineOptions"></uib-datepicker>
            <input type="text" name="deadline" uib-datepicker-popup="{[format]}" ng-model="task.deadline">
            </div>
        <br />
        </div>
    </div>

    <div class="form-group row addImageBox" ng-if="addImage" ng-show="addImage">
      <div ng-hide="uploader.queue.length || project.image" ng-show="imageRemoved">
          <div ng-show="uploader.isHTML5">
            <div class="clearfix spacer"></div>
              <!-- Example: nv-file-drop="" uploader="{Object}" options="{Object}" filters="{String}" -->
              <div nv-file-drop="" uploader="uploader">
                  <div nv-file-over="" uploader="uploader" ng-model="projectImage" over-class="another-file-over-class" class="well my-drop-zone image-drop-zone" ng-click="fileChooser()">
                      <br />
                      <i>Click or drag in attachment.. </i>
                  </div>
                  {[iconImage]}
              </div>
              <input class="hidden" type="file" id="fileSelect" nv-file-select="" uploader="uploader" multiple />
          </div>
          <div class="clearfix spacer"></div><div class="spacer"></div>
      </div>

      <div ng-if="project.image" ng-hide="imageRemoved">
        <h3>Chosen image</h3>
        
        <img ng-src="/{[project.image]}" class="serviceIcon">
        
        <div class="clearfix"></div>
        <button type="button" class="btn btn-warning btn-sm" ng-click="removeProjectImage(project.id, project.icon)">
            <span class="glyphicon glyphicon-trash"></span> Remove
        </button>
        <div class="clearfix spacer"></div>
      </div>


      <div ng-show="uploader.queue.length">
          <h3>Chosen image</h3>
          <!-- Uploaded Icon & Options -->
          <div ng-repeat="item in uploader.queue">
              <div class="iconBox" ng-show="uploader.isHTML5" ng-thumb="{ file: item._file, height: 150 }"></div>
              
              <div class="clearfix spacer"></div>

              <button type="button" class="btn btn-success btn-sm" ng-click="item.upload()" ng-hide="item.isReady || item.isUploading || item.isSuccess">
                  <span class="glyphicon glyphicon-upload"></span> Upload
              </button>
              <button type="button" class="btn btn-warning btn-sm" ng-click="item.remove()" >
                  <span class="glyphicon glyphicon-trash"></span> Remove
              </button>
              <div class="clearfix spacer"></div>

              <h4> 
                <span ng-show="item.isUploading">
                  <i class="glyphicon glyphicon-send"></i>

                  <div class="clearfix"></div>

                  <div class="form-control" ng-show="item.isUploading"
                    style="background-color:transparent; color:white;
                            border:0px; box-shadow: none;">
                      Progress:
                      <div class="progress" style="">
                          <div class="progress-bar" role="progressbar" ng-style="{ 'width': uploader.progress + '%' }"></div>
                      </div>
                      <div class="clearfix spacer"></div>
                  </div>
                </span>

                <span ng-show="item.isSuccess"> 
                  <i class="glyphicon glyphicon-ok"></i><br/>
                  <input type="hidden" name="icon" ng-value="item.file.name" ng-model="project.image">
                  </span>
                <span ng-show="item.isCancel">
                  <i class="glyphicon glyphicon-ban-circle"></i></span>
                <span ng-show="item.isError">
                  <i class="glyphicon glyphicon-remove"></i></span>
              </h4>
          </div><!--end uploaded icon-->
        </div><!--end chosen icon-->
    </div>
    <br />
    
    <div class="form-group text-right" style="margin-bottom:0px !important;">

            {!! Form::submit('Add Ticket!', 
              array('class'=>'btn button btn-lg',
              'style'=>'width:100%;border-radius:0;')) !!}
    </div>


{!! Form::close() !!}