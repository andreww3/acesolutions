{!! Form::open(array('route' => 'addService', 'class' => 'addServiceForm')) !!}

    <div class="form-group row">
      <a class="extra-option-link" ng-click="openIconUploader()" ng-hide="addImage">        
          + Icon
      </a>
    </div>

    <div class="form-group row" ng-if="addIcon" ng-show="addIcon">
      <div ng-hide="uploader.queue.length || service.icon" ng-show="iconRemoved">
          <h3>Upload icon</h3>
          <div ng-show="uploader.isHTML5">
            <div class="clearfix spacer"></div>
              <!-- Example: nv-file-drop="" uploader="{Object}" options="{Object}" filters="{String}" -->
              <div nv-file-drop="" uploader="uploader">
                  <div nv-file-over="" uploader="uploader" ng-model="iconImage" over-class="another-file-over-class" class="well my-drop-zone icon-drop-zone" ng-click="fileChooser()">
                      <i>Drag icon image in.. (200x200px)</i>
                  </div>
                  {[iconImage]}
              </div>
              <input class="hidden" type="file" id="fileSelect" nv-file-select="" uploader="uploader" multiple />
          </div>
          <div class="clearfix spacer"></div><div class="spacer"></div>
      </div>

      <div ng-if="service.icon" ng-hide="iconRemoved">
        <h3>Chosen icon</h3>
        
        <img ng-src="/{[service.icon]}" class="serviceIcon">
        
        <div class="clearfix"></div>
        <button type="button" class="btn btn-warning btn-sm" ng-click="removeServiceIcon(service.id, service.icon)">
            <span class="glyphicon glyphicon-trash"></span> Remove
        </button>
        <div class="clearfix spacer"></div>
      </div>

      <div ng-show="uploader.queue.length">
          <h3>Chosen icon</h3>
          <!-- Uploaded Icon & Options -->
          <div ng-repeat="item in uploader.queue">
              <div class="iconBox" ng-show="uploader.isHTML5" ng-thumb="{ file: item._file, height: 150 }"></div>
              <div class="clearfix spacer"></div>

              <p><b>Filename: </b> {[ item.file.name ]}</p>
              <p><b>Filesize: </b> {[ item.file.size/1024/1024|number:2 ]} MB</p>
              <div class="clearfix spacer"></div>

              <button type="button" class="btn btn-success btn-sm" ng-click="item.upload()" ng-hide="item.isReady || item.isUploading || item.isSuccess">
                  <span class="glyphicon glyphicon-upload"></span> Upload
              </button>
              <button type="button" class="btn btn-warning btn-sm" ng-click="item.remove()" >
                  <span class="glyphicon glyphicon-trash"></span> Remove
              </button>
              <div class="clearfix spacer"></div>

              <h4> 
                <span ng-show="item.isUploading">
                  <i class="glyphicon glyphicon-send"></i>

                  <div class="clearfix"></div>

                  <div class="form-control" ng-show="item.isUploading"
                    style="background-color:transparent; color:white;
                            border:0px; box-shadow: none;">
                      Progress:
                      <div class="progress" style="">
                          <div class="progress-bar" role="progressbar" ng-style="{ 'width': uploader.progress + '%' }"></div>
                      </div>
                      <div class="clearfix spacer"></div>
                  </div>
                </span>

                <span ng-show="item.isSuccess"> 
                  <i class="glyphicon glyphicon-ok"></i><br/>
                  <input type="hidden" name="icon" ng-value="item.file.name" ng-model="service.icon">
                  </span>
                <span ng-show="item.isCancel">
                  <i class="glyphicon glyphicon-ban-circle"></i></span>
                <span ng-show="item.isError">
                  <i class="glyphicon glyphicon-remove"></i></span>
              </h4>
          </div><!--end uploaded icon-->
        </div><!--end chosen icon-->
    </div><!--end icon form-group-->

    <div class="form-group row">
        {!! Form::text('name', null, 
            array( 
                  'class'=>'form-control', 
                  'placeholder'=>'The name of the service')) !!}
    </div>

    <div class="form-group row">
        {!! Form::textarea('description', null, 
            array( 
                  'class'=>'form-control', 
                  'placeholder'=>'A short description of the service and/or the promblem/need it solves.')) !!}
    </div>

    <div class="spacer"></div>

    <div class="form-group row">
      <input type="hidden" name="icon" value="">
    </div>
    
    <div class="form-group text-center">

            {!! Form::submit('Add Service!', 
              array('class'=>'btn button btn-lg',
              'style'=>'width:100%;border-radius:0;')) !!}
    </div>


{!! Form::close() !!}