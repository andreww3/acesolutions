<form>
<p>Product Name</p>
<input class="form-control" name="name" type="text" ng-model="product.name">
<br />
<p>Select product type</p>
<select class="form-control" name="type" ng-model="product.type">
	<option value="necklace">Necklace</option>
	<option value="bracelet">Bracelet</option>
</select>
<br />
<p>Add image</p>
<div ng-if="product.image" ng-hide="imageRemoved">
  <h3>Chosen image</h3>
  
  <img ng-src="/{[product.image]}">

  <div class="clearfix spacer"></div>
  <button type="button" class="btn btn-warning btn-sm" ng-click="removeProductImage(product.id, product.image)">
      <span class="glyphicon glyphicon-trash"></span> Remove Picture
  </button>
  <div class="clearfix spacer"></div>
</div>

<div class="form-group row">
  <div ng-hide="uploader.queue.length || product.image" ng-show="imageRemoved">
      <div ng-show="uploader.isHTML5">
          <!-- Example: nv-file-drop="" uploader="{Object}" options="{Object}" filters="{String}" -->
          <div nv-file-drop="" uploader="uploader">
              <div nv-file-over="" uploader="uploader" ng-model="productImage" over-class="another-file-over-class" class="well my-drop-zone image-drop-zone" ng-click="fileChooser()"
                style="padding-top:25px">
                  <i style="margin-top: ">Click or Drag in 
                  <br/>image.. (max 1mb)</i>
              </div>
              {[iconImage]}
          </div>
          <input class="hidden" type="file" id="fileSelect" nv-file-select="" uploader="uploader" multiple />
      </div>
      <div class="clearfix spacer"></div><div class="spacer"></div>
  </div>


  <div ng-show="uploader.queue.length">
      <h3>Chosen Image</h3>
      <!-- Uploaded Icon & Options -->
      <div ng-repeat="item in uploader.queue">
          <div class="iconBox" ng-show="uploader.isHTML5" ng-thumb="{ file: item._file, height: 150 }"></div>
          <div class="clearfix spacer"></div>

          <p><b>Filename: </b> {[ item.file.name ]}</p>
          <p><b>Filesize: </b> {[ item.file.size/1024/1024|number:2 ]} MB</p>
          <div class="clearfix spacer"></div>
          
          <button type="button" class="btn btn-success btn-sm" ng-click="item.upload()" ng-hide="item.isReady || item.isUploading || item.isSuccess">
              <span class="glyphicon glyphicon-upload"></span> Upload
          </button>
          <button type="button" class="btn btn-warning btn-sm" ng-click="item.remove()" >
              <span class="glyphicon glyphicon-trash"></span> Remove
          </button>
          <div class="clearfix spacer"></div>

          <h4> 
            <span ng-show="item.isUploading">
              <i class="glyphicon glyphicon-send"></i>

              <div class="clearfix"></div>

              <div class="form-control" ng-show="item.isUploading"
                style="background-color:transparent; color:white;
                        border:0px; box-shadow: none;">
                  Progress:
                  <div class="progress" style="">
                      <div class="progress-bar" role="progressbar" ng-style="{ 'width': uploader.progress + '%' }"></div>
                  </div>
                  <div class="clearfix spacer"></div>
              </div>
            </span>

            <span ng-show="item.isSuccess"> 
              <i class="glyphicon glyphicon-ok"></i><br/>
              </span>
            <span ng-show="item.isCancel">
              <i class="glyphicon glyphicon-ban-circle"></i></span>
            <span ng-show="item.isError">
              <i class="glyphicon glyphicon-remove"></i></span>
          </h4>
          <div class="clearfix spacer"></div>
      </div><!--end uploaded icon-->
    </div><!--end chosen icon-->
</div>

<p>The price</p>
<input class="form-control" name="price" type="text" ng-model="product.price">

<div class="form-group text-center" style="width:80%;">
    <div class="spacer"></div>
    <div class="spacer clearfix"></div>        
    <a class="serviceBtn saveServiceBtn" 
        style="margin-left:2%; margin-bottom: 15px; width: 96%;"
        ng-click="addProduct(product.name, product.type, product.price, product.image, product.description);">
        <span class="glyphicon glyphicon-floppy-saved" style="font-size:20px;">
        </span>
    </a>
</div>
</form>
<br/>
<div class="clearfix spacer"></div>