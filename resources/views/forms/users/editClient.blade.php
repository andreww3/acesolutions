{!! Form::open(array('route' => 'updateUser', 'class' => 'editProjectForm')) !!}

    <div class="form-group row">
        {!! Form::text('name', null, 
            array( 
                  'class'=>'form-control', 
                  'placeholder'=>'The full name of the client/contact person',
                  'ng-value'=>'client.name')) !!}
    </div>

    <div class="form-group row">
        {!! Form::text('email', null, 
            array( 
                  'class'=>'form-control', 
                  'placeholder'=>'The email of the client',
                  'ng-value'=>'client.email')) !!}
    </div>

    <div class="spacer"></div>
    <div class="form-group row">
      <a ng-click="addProject = true" ng-hide="addProject">
        <h4> 
            <span class="glyphicon glyphicon-plus"></span> 
            Add Project </h4>
      </a>
      <div ng-show="addProject"> 

          {!! Form::textarea('description', null, 
              array( 
                    'class'=>'editor form-control', 
                    'placeholder'=>'Contents of the project. This feature will be used when TinyMce will be implemented on the blog.',
                    'ng-model'=>'project.description')) !!}
	      <a ng-click="selectDates = true" ng-hide="selectDates">        
	        <h4>
	            <span class="glyphicon glyphicon-plus"></span> 
	            Add Dates </h4>
	        </a>
	        <div class="datePickers" ng-show="selectDates">
	        <!-- UI Bootstrap Datepicker -->
	          <div style="width:44%; float:left;">
	              <h4>Starting date</h4>
	              <div class="input-group">
	              <uib-datepicker ng-model="date.started_at" class="well well-sm" datepicker-options="inlineOptions"></uib-datepicker>
	              <input type="hidden" name="started_at" ng-model="date.started_at"> 
	              </div>

	          </div>

	          <div style="width:44%; margin-left:4%; float:left;">
	              <h4>Delivery date</h4>
	              <div class="input-group">
	              <uib-datepicker name="delivered_at" ng-model="date.delivered_at" class="well well-sm" datepicker-options="inlineOptions"></uib-datepicker>
	              <input type="hidden" name="delivered_at" ng-model="date.delivered_at">
	              </div>
	          </div>
	        <br />
	        </div>
	        <div class="clearfix spacer"></div>
      </div>
    </div>
    <div class="form-group row" style="margin-bottom:25px;">

    </div>
    <div class="clearfix spacer"></div><div class="spacer"></div>
    <div class="form-group text-center" style="margin-bottom:0px !important;">
        <div class="spacer"></div>
        <div class="spacer clearfix"></div>        
        <a class="serviceBtn saveServiceBtn" ng-click="saveProject(project.id, project.title, project.summary, project.description)">
            <span class="glyphicon glyphicon-floppy-saved" style="font-size:20px;">
            </span>
        </a>
    </div>


{!! Form::close() !!}