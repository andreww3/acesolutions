{!! Form::open(array('route' => 'addUser', 'class' => 'editProjectForm')) !!}

    <div class="form-group row">
        {!! Form::text('name', null, 
            array( 
                  'class'=>'form-control', 
                  'placeholder'=>'The contact person')) !!}
    </div>

    <div class="form-group row">
        {!! Form::text('email', null, 
            array( 
                  'class'=>'form-control', 
                  'placeholder'=>'The email')) !!}
    	
    </div>

    <div class="form-group row">
        {!! Form::text('phone', null, 
            array( 
                  'class'=>'form-control', 
                  'placeholder'=>'The phone number')) !!}
        
        <div class="clearfix spacer"></div>
    </div>

    <input type="hidden" name="role" value="client">

    <div class="clearfix spacer"></div>
    <div class="form-group" style="margin-bottom:0px !important;">

            {!! Form::submit('Add Client!', 
              array('class'=>'btn button btn-lg',
              'style'=>'width:100%;border-radius:0;')) !!}
    </div>


{!! Form::close() !!}