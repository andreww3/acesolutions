{!! Form::open(array('route' => 'updateUser', 'class' => 'editProjectForm')) !!}


    <div class="form-group row">
        <select name="role" class="form-control" ng-model="member.role">
            <option>The role</option>
            <option value="author">Author</option>
            <option value="admin">Admin</option>
        </select>
        
    </div> 

    <div class="form-group row">
        {!! Form::text('name', null, 
            array( 
                  'class'=>'form-control', 
                  'placeholder'=>'The name',
                  'ng-value'=>'member.name',
                  'ng-model'=>'member.name')) !!}
    </div>

    <div class="form-group row">
        {!! Form::text('email', null, 
            array( 
                  'class'=>'form-control', 
                  'placeholder'=>'The email',
                  'ng-value'=>'member.email',
                  'ng-model'=>'member.email')) !!}
        
    </div>

    <div class="form-group row">
        {!! Form::text('phone', null, 
            array( 
                  'class'=>'form-control', 
                  'placeholder'=>'The phone number',
                  'ng-value'=>'member.phone',
                  'ng-model'=>'member.phone')) !!}
        <div class="clearfix spacer"></div>
    </div>

    <div class="clearfix spacer"></div><div class="spacer"></div>
    <div class="form-group text-center" style="margin-bottom:0px !important;">
        <div class="spacer"></div>
        <div class="spacer clearfix"></div>        
        <a class="serviceBtn saveServiceBtn" ng-click="updateMember(member.id, member.name, member.role, member.email, member.phone)">
            <span class="glyphicon glyphicon-floppy-saved" style="font-size:20px;">
            </span>
        </a>
    </div>


{!! Form::close() !!}