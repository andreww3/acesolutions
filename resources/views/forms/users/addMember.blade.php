{!! Form::open(array('route' => 'addUser', 'class' => 'editProjectForm')) !!}

    <div class="form-group row">

        <select name="role" class="form-control">
            <option>The role</option>
            <option value="author">Author</option>
            <option value="admin">Admin</option>
        </select>

    </div>

    <div class="form-group row">
        {!! Form::text('name', null, 
            array( 
                  'class'=>'form-control', 
                  'placeholder'=>'The name')) !!}
    </div>

    <div class="form-group row">
        {!! Form::text('email', null, 
            array( 
                  'class'=>'form-control', 
                  'placeholder'=>'The email')) !!}
    </div>

    <div class="form-group row">
        {!! Form::text('phone', null, 
            array( 
                  'class'=>'form-control', 
                  'placeholder'=>'The phone number')) !!}
        <div class="clearfix"></div>
    </div> 

    <div class="form-group" style="margin-bottom:0px !important;">

            {!! Form::submit('Add Author!', 
              array('class'=>'btn button btn-lg',
              'style'=>'width:100%;border-radius:0;')) !!}
    </div>


{!! Form::close() !!}