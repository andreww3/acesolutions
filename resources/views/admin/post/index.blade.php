@extends('layouts.blog')

@section('title', 'Posts')

@section('menu')
    {{-- Navigation Bar --}}
    <nav class="navbar navbar-default blogBar">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed"
                  data-toggle="collapse" data-target="#navbar-menu">
            <span class="sr-only">Toggle Navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        </div>
        <div class="collapse navbar-collapse" id="navbar-menu">
          @include('admin.partials.navbar')
        </div>
      </div>
    </nav>
@endsection

@section('content')
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="panel panel-default">
          <div class="panel-heading" style="padding-top:15px;">
          <div class="col-md-6">
            <h3>Posts <small>» Listing</small></h3>
          </div>
          <div class="col-md-6 text-right">
            <a href="dashboard/post/create" class="btn btn-success btn-md">
              <i class="fa fa-plus-circle"></i> New Post
            </a>
          </div>
          <div class="clearfix"></div>
          </div>
          <div class="panel-body">

            @include('admin.partials.errors')
            @include('admin.partials.success')

            <table id="posts-table" class="table table-striped table-bordered">
              <thead>
                <tr>
                  <th>Title</th>
                  <th>Category</th>

                  <th data-sortable="false">Actions</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($posts as $post)
                  <tr>
                    <td><b>{{ $post->title }}</b></td>
                    <td>{{ $post->category }}</td>

                    <td>
                      <a href="/dashboard/post/{{ $post->id }}/edit"
                         class="btn btn-xs btn-info">
                        <i class="fa fa-edit"></i> Edit
                      </a>
                      <a href="/blog/{{ $post->slug }}"
                         class="btn btn-xs btn-warning">
                        <i class="fa fa-eye"></i> View
                      </a>
                    </td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
@stop

@section('javascript')
  <script>
    $(function() {
      $("#posts-table").DataTable({
        order: [[0, "desc"]]
      });
    });
  </script>
@stop