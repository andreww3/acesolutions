@extends('layouts.blog')

@section('title', 'New Post')

@section('header')
  <link href="/assets/pickadate/themes/default.css" rel="stylesheet">
  <link href="/assets/pickadate/themes/default.date.css" rel="stylesheet">
  <link href="/assets/pickadate/themes/default.time.css" rel="stylesheet">
  <link href="/assets/selectize/css/selectize.css" rel="stylesheet">
  <link href="/assets/selectize/css/selectize.bootstrap3.css" rel="stylesheet">
@stop

@section('menu')
    {{-- Navigation Bar --}}
    <nav class="navbar navbar-default blogBar">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed"
                  data-toggle="collapse" data-target="#navbar-menu">
            <span class="sr-only">Toggle Navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        </div>
        <div class="collapse navbar-collapse" id="navbar-menu">
          @include('admin.partials.navbar')
        </div>
      </div>
    </nav>
@endsection

@section('content')
  <div class="container-fluid">
    <div class="row">
      <div style="width:1300px; margin: 0 auto;">
        <div class="panel panel-default">
          <div class="panel-heading" style="padding-top:15px;">
            <div class="col-md-12">
              <h3>Posts <small>» Add New Post</small></h3>
            </div>
            <div class="clearfix"></div>
          </div>
          <div class="panel-body">

            @include('admin.partials.errors')

            <form class="form-horizontal" role="form" method="POST"
                  action="{{ route('dashboard.post.store') }}">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">

              @include('admin.post._form')

              <div class="col-md-8">
                <div class="form-group">
                  <div class="col-md-10 col-md-offset-2">
                    <button type="submit" class="btn btn-primary btn-lg">
                      <i class="fa fa-disk-o"></i>
                      Save New Post
                    </button>
                  </div>
                </div>
              </div>
            </form>
            
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('javascript')
  <script src="/assets/pickadate/picker.js"></script>
  <script src="/assets/pickadate/picker.date.js"></script>
  <script src="/assets/pickadate/picker.time.js"></script>
  <script src="/assets/selectize/selectize.min.js"></script>
  
  <script>
    $(function() {

      $("#publish_date").pickadate({
        format: "mmm-d-yyyy"
      });

      $("#publish_time").pickatime({
        format: "h:i A"
      });

      $("#tags").selectize({
        create: true
      });

      $("#posts-table").DataTable({
        order: [[0, "desc"]]
      });
    });
  </script>
@stop