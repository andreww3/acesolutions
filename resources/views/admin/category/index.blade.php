@extends('layouts.blog')

@section('title', 'Categories')

@section('menu')
    {{-- Navigation Bar --}}
    <nav class="navbar navbar-default blogBar">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed"
                  data-toggle="collapse" data-target="#navbar-menu">
            <span class="sr-only">Toggle Navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        </div>
        <div class="collapse navbar-collapse" id="navbar-menu">
          @include('admin.partials.navbar')
        </div>
      </div>
    </nav>
@endsection


@section('content')
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-10 col-md-offset-1">
        <div class="panel panel-default">
          <div class="panel-heading" style="padding-top:15px;">
		      <div class="col-md-6">
		        <h3>Categories <small>» Listing</small></h3>
		      </div>
		      <div class="col-md-6 text-right">
		        <a href="dashboard/categories/create" class="btn btn-success btn-md">
		          <i class="fa fa-plus-circle"></i> New Category
		        </a>
		      </div>
		      <div class="clearfix"></div>
          </div>
          <div class="panel-body">

			@include('admin.partials.errors')
	        @include('admin.partials.success')

	        <table id="categories-table" class="table table-striped table-bordered">
	          <thead>
	          <tr>
	            <th>Title</th>
	            <th class="hidden-sm">Subtitle</th>
	            <th class="hidden-md">Page Image</th>
	            <th class="hidden-md">Meta Description</th>
	            <th class="hidden-sm">Direction</th>
	            <th data-sortable="false">Actions</th>
	          </tr>
	          </thead>
	          <tbody>
	          @foreach ($categories as $category)
	            <tr>
	              <td>{{ $category->title }}</td>
	              <td class="hidden-sm">{{ $category->subtitle }}</td>
	              <td class="hidden-md">{{ $category->page_image }}</td>
	              <td class="hidden-md">{{ $category->meta_description }}</td>
	              <td class="hidden-sm">
	                @if ($category->reverse_direction)
	                  Reverse
	                @else
	                  Normal
	                @endif
	              </td>
	              <td>
	                <a href="/dashboard/categories/{{ $category->id }}/edit"
	                   class="btn btn-xs btn-info" style="float:left;">
	                  <i class="fa fa-edit"></i> Edit
	                </a>

                  	<form method="POST" action="/dashboard/categories/{{ $category->id }}" style="float:left; margin-left:8px;">
	                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
	                    <input type="hidden" name="_method" value="DELETE">
	                    <button type="submit" class="btn btn-xs btn-danger">
	                      Delete
	                    </button>
	                </form>
	              </td>
	            </tr>
	          @endforeach
	          </tbody>
	        </table>

          </div>
        </div>
      </div>
    </div>
  </div>
@stop

@section('scripts')
  <script>
    $(function() {
      $("#categories-table").DataTable({
      });
    });
  </script>
@stop