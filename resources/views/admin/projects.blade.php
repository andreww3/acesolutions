@extends('layouts.main')

@section('title', 'Projects Dashboard')

@section('header')
    <style>
        .my-drop-zone { border: dotted 3px lightgray; opacity:0.8; cursor:pointer;}
        .my-drop-zone:hover { opacity:1;}
        .nv-file-over { border: dotted 3px red; } /* Default class applied to drop zones on over */
        .another-file-over-class { border: dotted 3px green; }
        html, body { height: 100%; }
        canvas {
            background-color: #f3f3f3;
            -webkit-box-shadow: 3px 3px 3px 0 #e3e3e3;
            -moz-box-shadow: 3px 3px 3px 0 #e3e3e3;
            box-shadow: 3px 3px 3px 0 #e3e3e3;
            border: 1px solid #c3c3c3;
            height: 100px;
            margin: 6px 0 0 6px;
        }
    </style>

@endsection

@section('menu')
    
    @include('shared.header')

@endsection

@section('content')
    <div class="container" ng-controller="adminCtrl">
        <div class="col-md-12">
    	   <h1 class="dashTitle borderBottom">
                Admin dashboard</h1>
            <div class="spacer clearfix"></div>  
        </div>

        <div class="col-md-7" ng-controller="projectCtrl">

            <!--<div class="text-left" style="padding-left:20px;">
                <h2>Projects:</h2>
                <a href="projects/extended">
                    <h4>open extended projects --- -- -</h4>
                </a>
                <div class="spacer clearfix"></div>
            </div
            <div class="spacer clearfix"></div>>--> 
                   
            <div class="projectBox">
                <div ng-hide="addNewProject">
                    <a class="serviceBtn editServiceBtn" ng-click="addNewProject = true">
                        <span class="glyphicon glyphicon-plus">
                        </span>
                    </a>
                    <h3>Add a new project!</h3>
                    <p>Surely there's never to many projects, so do go ahead and add one more. Make sure to include at least a short description of the project.</p>
                </div>
                <div class="newProjectBox" ng-show="addNewProject">
                    <div class="formTitle">
                        <a class="serviceBtn closeEditService" ng-click="addNewProject = false">
                            <span class="glyphicon glyphicon-remove">
                            </span>
                        </a>
                    </div>
                    @include('forms.projects.add')

                </div>
            </div>

                
            <div class="projectBox" style="margin-bottom:15px;" ng-repeat="project in projects | orderBy: 'delivered_at':true">
                <div ng-hide="editProject"> 
                    <a class="serviceBtn editServiceBtn" ng-click="editProject = true">
                        <span class="glyphicon glyphicon-pencil">
                        </span>
                    </a>
                    <h3>{[ project.title ]}</h3>
                    <p>{[ project.summary ]}</p>
                    <p>Delivered on {[ project.delivered_at ]}</p>

                    <p ng-if="project.description">{[ project.description ]}</p>

                </div>
                
                <div ng-show="editProject">
                    <div class="editServiceTitle">
                        <a class="serviceBtn closeEditService" ng-click="editProject = false">
                            <span class="glyphicon glyphicon-remove"></span></a>

                        <a class="serviceBtn deleteServiceBtn" ng-click="deleteProject(project.id)">
                            <span class="glyphicon glyphicon-trash"></span></a>
                    </div>
                
                    @include('forms.projects.edit')
                </div>
            </div>

        </div>

        <div class="col-md-5" ng-controller="serviceCtrl">

    		<!--<h2>Services</h2>
            <a href="services/extended"><h4> - -- --- open extended services --- -- - </h4></a>
            <div class="spacer clearfix"></div>-->

            <div class="col-md-12">
                <div class="serviceBox">
                    <a class="serviceBtn editServiceBtn"  ng-hide="showAddService"
                    ng-click="showAddService = true">
                        <span class="glyphicon glyphicon-plus">
                        </span>
                    </a>
                    <a ng-click="showAddService = true" ng-hide="showAddService" style="color:white;">
                        <h4 class="serviceTitle">Add new service</h4>
                    </a>

                    <div ng-show="showAddService">
                        <div class="editServiceTitle">
                            <a class="serviceBtn closeEditService" ng-click="showAddService = false">
                                <span class="glyphicon glyphicon-remove">
                                </span>
                            </a>
                        </div>
                        @include('forms.services.add')
                    </div>
                </div>
                <div class="spacer"></div>
            </div>

            <div class="col-md-12" ng-repeat="service in services">

                <div class="serviceBox" style="margin-bottom:15px;">
                    <div ng-hide="editService">

                        <!-- if service has icon -->
                        <!--<div ng-if="service.icon">
                            <div class="col-md-4">
                                <img ng-src="/{[service.icon]}" style="width:100px; text-align:left;"/>
                            </div>
                            <div class="col-md-6 text-left">
                                <h4 class="serviceTitle">{[ service.name ]}</h4>
                            </div>
                        </div>-->

                        <!-- if service doesn't have icon | ng-if="!service.icon" | -->
                        <div>
                            <div class="col-md-12 text-center">
                                <div class="col-md-4">
                                    <img style="width:100%; height:auto;" 
                                        ng-src="{[service.icon]}">    
                                </div>
                                <div class="col-md-4">
                                    <h4 class="serviceTitle">{[ service.name ]}</h4>
                                </div>
                                
                            </div>
                        </div>

                        <a class="serviceBtn editServiceBtn" ng-click="editService = true">
                            <span class="glyphicon glyphicon-pencil">
                            </span>
                        </a>
                        <div class="clearfix"></div>                        
                    </div>

                    <div ng-show="editService">
                        <div class="editServiceTitle">
                            <a class="serviceBtn closeEditService" ng-click="editService = false">
                                <span class="glyphicon glyphicon-remove">
                                </span>
                            </a>

                            <a class="serviceBtn deleteServiceBtn" ng-click="deleteService(service.id)">
                                <span class="glyphicon glyphicon-trash">
                                </span>
                            </a>
                        </div>
                        
                        @include('forms.services.edit')
                    </div>
                </div>

    	    </div>
		<div class="clearfix"></div>
		<br />
		</div><!--end services col-->

    </div><!--end .content-->
</div>
@endsection

@section('footer')

    @include('shared.footer')
    
@endsection

