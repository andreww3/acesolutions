@extends('layouts.blog')

@section('title', 'Blog Dashboard')

@section('menu')
    
    @include('shared.header')

@endsection

@section('content')
    <div class="container" style="max-width:1200px;" ng-controller="blogCtrl">
        <div class="col-md-12">
            <br /><div class="spacer clearfix"></div>
    	    <h1 class="dashTitle borderBottom">
                Blog dashboard</h1>
            <div class="spacer clearfix"></div>  

            {{-- Navigation Bar --}}
            <nav class="navbar navbar-default blogBar">
              <div class="container-fluid">
                <div class="navbar-header">
                  <button type="button" class="navbar-toggle collapsed"
                          data-toggle="collapse" data-target="#navbar-menu">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                  </button>
                </div>
                <div class="collapse navbar-collapse" id="navbar-menu">
                  @include('admin.partials.navbar')
                </div>
              </div>
            </nav>

            <br /><div class="spacer clearfix"></div>
            
            <hr>
            <br /><div class="spacer clearfix"></div>
        </div>

    </div><!--end .content-->
</div>
@endsection

@section('footer')

    @include('shared.footer')
    
@endsection

@section('javascript')

@endsection