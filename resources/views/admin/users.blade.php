@extends('layouts.main')

@section('title', 'Users Dashboard')

@section('menu')
    
    @include('shared.header')

@endsection

@section('content')
    <div class="container" ng-controller="adminCtrl">
        <div class="col-md-12">
    	   <h1 class="dashTitle borderBottom">
                Users dashboard</h1>
            <div class="spacer clearfix"></div>  
        </div>

        <div class="col-md-7">

      		<div class="projectBox">
      			<div ng-hide="showAddClient" ng-click="showAddClient = true" style="cursor:pointer;">
      		        <a class="serviceBtn editServiceBtn" ng-click="addNewProject = true">
                        <span class="glyphicon glyphicon-plus">
                        </span>
                    </a>
                    <h3>Add a new client!</h3>
                    <p>Surely there's never to many clients, so do go ahead and add one more.</p>
      			</div>

      			<div ng-show="showAddClient">
                    <div class="formTitle">
                    	<h4>New client info..</h4>
                        <a class="serviceBtn closeEditService" ng-click="showAddClient = false">

                            <span class="glyphicon glyphicon-remove">
                            </span>
                        </a>
                        <div class="clearfix"></div>
                    </div>

                    @include('forms.users.addClient')
      			</div>
      		</div>

            <div class="projectBox" ng-repeat="client in clients">
                <div ng-hide="editClient"> 
                    <a class="serviceBtn editServiceBtn" ng-click="editClient = true">
                        <span class="glyphicon glyphicon-pencil">
                        </span>
                    </a>
                    <h3>{[ client.name ]}</h3>
                    <p>Contact details: <i>{[ client.email ]}, {[ project.description ]}</i></p>
                    <div class="spacer"></div>

                </div>
                
                <div ng-show="editClient">
                    <div class="editServiceTitle">
                        <a class="serviceBtn closeEditService" ng-click="editClient = false">
                            <span class="glyphicon glyphicon-remove"></span></a>

                        <a class="serviceBtn deleteServiceBtn" ng-click="deleteClient(client.id)">
                            <span class="glyphicon glyphicon-trash"></span></a>
                    </div>
                
                    @include('forms.users.editClient')
                </div>
            </div>
        </div>

        <div class="col-md-5">

      		<div class="projectBox">
      			<div ng-hide="showAddAuthor" ng-click="showAddAuthor = true" style="cursor:pointer;">
      		        <a class="serviceBtn editServiceBtn" ng-click="showAddAuthor = true">
                        <span class="glyphicon glyphicon-plus">
                        </span>
                    </a>
                    <h3>Add a new member!</h3>
                    <p>Teamwork has much more to offer than working individually.</p>
      			</div>

      			<div ng-show="showAddAuthor">
                    <div class="formTitle">
                    	<h4>New collaborator info..</h4>
                        <a class="serviceBtn closeEditService" ng-click="showAddAuthor = false">

                            <span class="glyphicon glyphicon-remove">
                            </span>
                        </a>
                        <div class="clearfix"></div>
                    </div>

                    @include('forms.users.addMember')
      			</div>
      		</div>

            <div class="projectBox" ng-repeat="member in members">
                <div ng-hide="editMember"> 
                    <a class="serviceBtn editServiceBtn" ng-click="editMember = true">
                        <span class="glyphicon glyphicon-pencil">
                        </span>
                    </a>
                    <h3>{[ member.name ]}</h3>
                    <p>Role: <b>{[ member.role ]}</b><br />
                    Contact details: <i>{[ member.email ]}, {[ member.phone ]}</i></p>
                    <div class="spacer"></div>
                </div>
                
                <div ng-show="editMember">
                    <div class="formTitle">
                    	<h4 style="float:none; padding-left:0%;" class="text-center">{[ member.name ]}</h4>
                        <a class="serviceBtn closeEditService" ng-click="editMember = false">
                            <span class="glyphicon glyphicon-remove"></span></a>

                        <a class="serviceBtn deleteServiceBtn" ng-click="deleteMember(member.id)">
                            <span class="glyphicon glyphicon-trash"></span></a>
                    </div>
                
                    @include('forms.users.editMember')
                </div>
            </div>

			<div class="clearfix"></div>
			<br />
		</div>

    </div><!--end .content-->
</div>
@endsection

@section('footer')

    @include('shared.footer')
    
@endsection

@section('javascript')

@endsection