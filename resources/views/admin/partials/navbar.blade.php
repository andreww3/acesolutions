
  <div class="text-center">
    <a class="coloredLink" href="/blog">Blog Home</a>
    
    @if (Auth::guest())
      <a class="coloredLink" href="/auth/login">Login</a>
    @endif

    @if (Auth::check())
      <a class="coloredLink" href="/dashboard/post">Posts</a>
      
      <a class="coloredLink" href="/dashboard/categories">Categories</a>

      <a class="coloredLink" href="/dashboard/tag">Tags</a>

      <a class="coloredLink" href="/dashboard/media">Media</a>

    @endif
  </div>
