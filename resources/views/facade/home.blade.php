@extends('layouts.main')


@section('title', 'Make the best of IT')

@section('menu')

<header>
    <div class="mainHeader">
     
    <nav>
        <a href="/"><img class="menuLogo" src="assets/style/img/logo.png" /></a>
        <!--<p class="slogan"><span style="color:#199d7e;">..</span> make the best out of iT<span style="color:#199d7e;">.</span></p>-->

        @if ( Auth::guest() )
            <a id="toWebsight" href> Websight </a> |
            <a id="toProjects" href> Portfolio </a> |
            <a href="articles"> Articles </a> |
            <a href="services"> Services </a> 
        @endif

        @if ( Auth::check() ) 
            Hi, {{strstr(Auth::user()->name, ' ', true)}} -- - 
            @if ( Auth::user()->role == 'admin')
                <a href="dashboard/users"> Users </a> |
                <a href="dashboard/tasks"> Tasks </a> |
                <a href="dashboard/projects"> Projects </a> |
                <a href="articles"> Articles </a> |
            
            @elseif ( Auth::user()->role == 'client')
                <a href="tickets-dash"> Tickets </a> |
                <a href="services-dash"> Services </a> |
                <a href="articles"> Articles </a> |
            
            @elseif ( Auth::user()->role == 'member')
                <a href="projects-dash"> Projects </a> |
                <a href="tickets-dash"> Tickets </a> |
                <a href="services-dash"> Services </a> |
                <a href="articles"> Articles </a> |
            @endif
            <a href="logout"> Log out </a>           
        @endif
    </nav>

    </div>
</header>

@endsection


@section('content')
<div class="container" ng-controller="homeCtrl">
    <img src="assets/img/up.png" id="backUp" />
    <div class="content">
    <!--  -->
        <br /><div class="clearfix spacer"></div>
        <div class="col-md-2"></div>
        <div class="col-md-8">      
            <h3 style="font-size:46px; font-family: 'latoLight'">Make the best out of iT!</h3>
            <p class="description"> Having a website nowadays is a must. A user-friendly website is a plus.<br/>
                Ace Solutions aims to make the best out of information technology by designing and developing solutions that meet the needs of the visitors and achieve <br/>specific online business objectives.
                <br/>
            </p>               
            <!--
            <p class="description">
                The services revolve around strenghtening the effectivity and eficiency IT solutions by considering both the needs of the end-user and those of the brand.</p>-->
        </div>
        <div class="col-md-2"></div>
        <div class="clearfix spacer"></div>

    <div id="services">
        <br /><div class="clearfix spacer"></div>
        <div class="col-md-2"></div>
        <div class="col-md-8">      
            <h1 style="font-size:46px; font-family: 'latoLight'">Services</h1>
            <p class="description"> 
            </p>               
            <!--
            <p class="description">
                The services revolve around strenghtening the effectivity and eficiency IT solutions by considering both the needs of the end-user and those of the brand.</p>-->
        </div>
        <div class="col-md-2"></div>
        <div class="clearfix spacer"></div>
        <div class="clearfix spacer"></div>
        <hr>
        <div class="col-md-3" ng-repeat="service in services | limitTo:4">
            <div class="serviceBox">
                <h3 style="padding-top:12px; padding-bottom:12px;
                        margin-bottom:5px;
                        border-top: 3px solid #34495e;
                        border-bottom: 3px solid #34495e;">
                            {[ service.name ]}</h3>
            </div>
            <div class="serviceBox" style="margin-top:-17px;">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                    <div class="clearfix spacer"></div>
                    <img style="width:100%; height:auto;" 
                                    ng-src="{[service.icon]}"> 
                </div>
                <div class="col-md-3"></div>
                    <div class="clearfix spacer"></div><br />
            </div>
            <div class="serviceBox" style="margin-top:-17px; background-color: #34495e;
                                            border-bottom: 3px solid #5CC1AB;">
                <div class="col-md-12">
                    <p class="description" style="padding-top:7px;">
                    <!--<a href>.. read more ..</a>--></p>
                    <!--<p class="description"> {[ service.description ]}</p>-->
                </div>
                <div class="clearfix spacer"></div>
            </div>
        </div>
        <br /><div class="clearfix spacer"></div>
        <br /><div class="clearfix spacer"></div>
                
        <a class="darkLink" href="services" target="_self">
            <h4>. .. more services .. .</h4></a>
                
        <hr>

        <a href="quote" class="getQuote btn button btn-lg"
            style="min-width:250px;">Get a quote!</a>
        <br /><br />
        <p class="description"> 
           *<i> a quote is an estimate in time and budget for solving a task.</i>
        </p><br/><br /> 
         <br /><br /><div class="clearfix spacer"></div>
        <br /><br /><div class="clearfix spacer"></div>
    </div>


    <div id="websight">
        <br /><div class="clearfix spacer"></div>
        <br /><div class="clearfix spacer"></div>

        <div class="title">Websight</div>
        <p class="description"> 
           Implement the best suited online solution for achieving your objectives.
            <br/>Communicate, sell, or engage your audience to interact </br>using one of these tree frameworks.
            
        <br/><br />
        </p> 
        <br /><div class="clearfix spacer"></div>

        <div class="col-md-4">
            <h2>Communication</h2>
            <p>Wordpress is one of the most popular content management system. It provides a user-friendly interface for creating and managing content. <br/>
            Serves well as a communication platform for organizations and businesses.</p>
            <img style="width:80%;" src="assets/img/wordpress.png" />
        </div>

        <div class="col-md-4">
            <h2>Commerce</h2>
            <p>Magento is one of the most powerful ecommerce system. It's built from modules, which makes it highly customizable for your own needs. It provides an interface for managing orders, <br/>stocks and logistics.</p>
            <img style="width:75%; margin-top:-80px;" src="assets/img/magento.png" />
        </div>
                    
        <div class="col-md-4">
            <h2>Application</h2>
            <p>A web application involves visitors by offering them a membership and a chance to contribute to building the content of the website. A webapp should also be a platform for networking.</p>
            <img src="assets/img/angularjs.png" style="width:35%; margin-left:10%; float:left;"/>
            <img src="assets/img/laravel.png" style="width:45%; margin-left:10%; float:left;"/>
        </div>

        <br /><div class="clearfix spacer"></div>
        <div class="col-md-2"></div>
        <div class="col-md-8">      
            <p class="description"> 
               In order to create a complete concept around your website to help your business or cause you might to take a look at: brand identity, marketing strategy, content marketing, web/graphic design, development and deployment. 
            <br /><br />
        </div>
        <div class="col-md-2"></div>
        <br /><div class="clearfix spacer"></div>
        <a href="quote" class="btn button btn-lg"
           style="min-width:350px;">Get a quote!</a>
        <br /><div class="clearfix spacer"></div>
        <br /><div class="clearfix spacer"></div>
        <br /><div class="clearfix spacer"></div>
        <br /><div class="clearfix spacer"></div>
    </div>                           

    <div id="projects">
        <br /><div class="clearfix spacer"></div>
        <br /><div class="clearfix spacer"></div>
        <a class="darkLink" href="projects" target="_self">
        <h1 style="font-size:50px;">Portfolio</h1>
        </a>
        <hr>
        <div style="position:relative" ng-repeat="project in projects| limitTo:3" class="col-md-4">

            <div class="card-container">

                <div class="card">
                    <div class="side">
                        <img ng-if="project.image" ng-src="{[ project.image ]}">
                    </div>

                    <div class="side back">
                       <img ng-if="project.image" ng-src="{[ project.image ]}">
                        <h3>{[ project.title ]} <a ng-if="project.link" href="http://{[ project.link ]}" target="_blank" class="projectLink"><span class="glyphicon glyphicon-link"></span></a></h3>

                        <p>{[ project.summary ]}</p>
                    </div>
                </div>
            </div>
        </div>
        <br /><div class="clearfix spacer"></div>
    
        <a class="darkLink" href="projects" target="_self">
            <h4>. .. more projects here .. .</h4></a>
        
        <hr>
        <br/>
        <a href="quote" class="btn button btn-lg"
            style="min-width:400px; font-size:20px; font-family: latoLight;">Get a quote!</a>
        </div>
        <br /><br /><div class="clearfix spacer"></div>
        <br /><br /><div class="clearfix spacer"></div>
    </div>

    <div class="clearfix"></div>
</div>
@endsection

@section('footer')

    @include('shared.footer')

@endsection

@section('javascript')
<script>
    $(window).bind("scroll", function() {
        if ($(this).scrollTop() > 520) {
            $("#backUp").fadeIn();
        } else {
            $("#backUp").stop().fadeOut();
        }
    });
    $(document).ready(function(){   

        $("a#toWebsight").click(function(){
            $('html, body').animate({scrollTop:$('#websight').position().top}, 'slow');
        }); 

        $("a#toServices").click(function(){
            $('html, body').animate({scrollTop:$('#services').position().top}, 'slow');
        });  

        $("a#toProjects").click(function(){
            $('html, body').animate({scrollTop:$('#projects').position().top}, 'slow');
        }); 

        $("#backUp").click(function(){
            $('html, body').animate({scrollTop:$('header').position().top}, 'slow');
        });                   
    });
</script>
@endsection
