@extends('layouts.main')

@section('title', 'About')

@section('header')

@endsection

@section('menu')
    
    @include('shared.header')

@endsection

@section('content')

    <div class="container" ng-controller="homeCtrl">

        <div class="homeIntro">

            <!--  -->
            <div class="col-md-2"></div>
            <div class="col-md-8">
                <div class="title">About</div>
               <p class="description"> 
                    Much alike a full-service digital agency, Ace Solutions strives to design and develop custom-made websites and web applications, but also get involved in the earlier stages concept development. 
                    <br/><br />
 
                    In order to create a complete, well-thought-through, awsome website for your business or cause you might need some help with.
                </p>                  

            </div>
            <div class="col-md-2"></div>
            <div class="clearfix spacer"></div>
        </div>

        <div class="clearfix spacer"></div>
        <div class="title">Workforce</div> 
        <hr>
        <div class="clearfix spacer"></div>
            <!--<p class="description"> 
                The team behind Ace Solutions.. 
            </p>-->            
        <div class="col-md-4"></div>
        <div id="boxes">
        <div class="col-md-4">
            <div class="picture-box">
                <img width="100%" src="/assets/img/members/Andrei.png" />
                <div class="overlay">
                    <a href="/Andrei-Cristian"><h3>Andrei Cristian</h3></a>
                    <h4>Project Coordinator & Developer</h4>
                    <h4><a href="mailto:adm@acesolutions.co">adm@acesolutions.co</a></h4>
                    <a class="close-overlay hidden">x</a>
                </div>
            </div>
        </div>
        </div>
        <div class="col-md-4"></div>
        <div class="clearfix"></div>
        <hr>
    </div>

    <div class="clearfix"></div>

</div>
<div class="clearfix spacer"></div>

@endsection

@section('footer')

    @include('shared.footer')
    
@endsection

@section('javascript')
    <script>
        $(document).ready(function(){
        if (Modernizr.touch) {
            // show the close overlay button
            $(".close-overlay").removeClass("hidden");
            // handle the adding of hover class when clicked
            $(".picture-box").click(function(e){
                if (!$(this).hasClass("hover")) {
                    $(this).addClass("hover");
                }
            });
            // handle the closing of the overlay
            $(".close-overlay").click(function(e){
                e.preventDefault();
                e.stopPropagation();
                if ($(this).closest(".picture-box").hasClass("hover")) {
                    $(this).closest(".picture-box").removeClass("hover");
                }
            });
        } else {
            // handle the mouseenter functionality
            $(".picture-box").mouseenter(function(){
                $(this).addClass("hover");

            })
            // handle the mouseleave functionality
            .mouseleave(function(){
                $(this).removeClass("hover");
            });
        }
    });  

    $(document).ready(function(){
        // Add Script for scrolling to contact form            
    });
    </script>
@endsection
