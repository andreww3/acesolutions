@extends('layouts.main')


@section('title', 'Projects')

@section('menu')
    
    @include('shared.header')

@endsection


@section('content')

    <div class="container" ng-controller="homeCtrl">
            <div class="homeIntro text-center">
                <div class="title">Portfolio</div>
                <!--<p class="description"> 
                    Here are some of the projects created... 
                </p>-->
            </div>
            <br /><div class="clearfix spacer"></div>


            <div style="position:relative" ng-repeat="project in projects" class="col-md-6">

                <div class="card-container" style="width:500px; height:500px; margin-bottom:80px; perspective: 1800; -webkit-perspective: 1800;">

                    <div class="card">
                        <div class="side">
                            <img ng-if="project.image" ng-src="{[ project.image ]}">
                        </div>

                        <div class="side back">
                           <img ng-if="project.image" ng-src="{[ project.image ]}">
                            <h3>{[ project.title ]} <a ng-if="project.link" href="http://{[ project.link ]}" target="_blank" class="projectLink"><span class="glyphicon glyphicon-link"></span></a></h3>

                            <p>{[ project.summary ]}</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
            <hr>
            <br/>
            <div class="clearfix spacer"></div>
                <a href="/quote" class="btn button btn-lg"
                style="min-width:350px; font-size:20px; font-family: latoLight;">Let's start a project together!</a>
        </div>
        <div class="clearfix"></div>

@endsection

@section('footer')

    @include('shared.footer')

@endsection

@section('javascript')

@endsection
