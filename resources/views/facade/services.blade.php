@extends('layouts.main')

@section('title', 'Services')

@section('menu')
    @include('shared.header')
@endsection

@section('content')

    <div class="container" ng-controller="homeCtrl">
            <div class="homeIntro text-center">
                <div class="title">Services</div>
                <p class="description"> 
                    When it comes to the online media, there are a couple of areas you should consider using in order to atract, communicate and sell to your target audience..  
                </p>
            </div>
            <div class="clearfix spacer"></div>

            <div class="col-md-3" ng-repeat="service in services">
                <div class="serviceBox">
                    <h3 style="padding-top:15px; padding-bottom:10px;
                    margin-bottom:5px;">
                        {[ service.name ]}</h3>
                </div>
                <div class="serviceBox" style="margin-top:-17px;">
                    <div class="col-md-3"></div>
                    <div class="col-md-6">
                    <div class="clearfix spacer"></div>
                    <img style="width:100%; height:auto;" 
                                ng-src="{[service.icon]}"> 
                    </div>
                    <div class="col-md-3"></div>
                    <div class="clearfix spacer"></div><br />
                </div>
                <div class="serviceBox" style="margin-top:-17px;">
                    <div class="col-md-1"></div>
                    <div class="col-md-10">
                        <p class="description"> {[ service.description ]}</p>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="clearfix spacer"></div><br />
                </div>
            </div>
            <div class="clearfix"></div>

        </div>
        <div class="clearfix"></div>

@endsection

@section('footer')

    @include('shared.footer')

@endsection

@section('javascript')

@endsection
