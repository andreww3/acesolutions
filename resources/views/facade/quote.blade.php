@extends('layouts.main')

@section('title', 'Contact')

@section('header')

@endsection

@section('menu')
    
    @include('shared.header')

@endsection

@section('content')

    <div class="clearfix"></div>

    <div class="col-md-12" ng-controller="homeCtrl">

        <div class="container">
            
            <div class="clearfix"></div>
        	<div class="contactForm">
            <div class="title">Get a quote!</div>
            <p class="description"> 
                Tell me about your project and/or business and/or objectives and let's find a way to work together to achieve your goals.
            </p>
        
	        @if (count($errors) > 0)  
	            <div class="alert alert-danger" role="alert">
	                <ul style="list-style-type:none;">
	                    @foreach($errors->all() as $error)
	                        <li>
	                            <div class="col-md-3 text-left" style="padding-left:35px;">
	                                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
	                                <span class="sr-only">Error:</span>
	                            </div>
	                            <div class="col-md-9 text-left" style="padding-left:35px;">{{ $error }}</div>
	                            <div class="clearfix"></div>
	                        </li>
	                    @endforeach
	                </ul>
	            </div>
	        @endif
            

            @if(Session::has('message'))
                <div class="alert alert-info">
                  {{Session::get('message')}}
                </div>
            @endif                

            @include('forms.contact.quote')
			</div>


        </div>
        <div class="clearfix"></div>

    </div>
    <div class="clearfix spacer"></div>


@section('footer')

    @include('shared.footer')
    
@endsection

@section('javascript')
    <script>
    $(document).ready(function(){
         $("a.secondMenuBtn").click(function(){
            $('html, body').animate({scrollTop:$('#secondMenu').position().top}, 'slow');
        });            
    });
    </script>
@endsection
