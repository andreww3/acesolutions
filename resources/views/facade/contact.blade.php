@extends('layouts.main')

@section('title', 'About')

@section('header')

@endsection

@section('menu')
    
    @include('shared.header')

@endsection

@section('content')

    <div class="container" ng-controller="homeCtrl">

        <div class="contactForm">
            <div class="title">Contact Form</div>
            
            <p class="description"> 
                Here you can ask me questions about Ace Solutions or myself, and/or present your potential business proposal.   
            </p>
            <div class="clearfix spacer"></div>
            
            @if (count($errors) > 0)  
                <div class="alert alert-danger" role="alert">
                    <ul style="list-style-type:none;">
                        @foreach($errors->all() as $error)
                            <li>
                                <div class="col-md-3 text-left" style="padding-left:35px;">
                                    <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                    <span class="sr-only">Error:</span>
                                </div>
                                <div class="col-md-9 text-left" style="padding-left:35px;">{{ $error }}</div>
                                <div class="clearfix"></div>
                            </li>
                        @endforeach
                    </ul>
                </div>
            @endif
        

            @if(Session::has('message'))
                <div class="alert alert-info">
                  {{Session::get('message')}}
                </div>
            @endif                

            @include('forms.contact.contact')
        </div>
        <div class="clearfix"></div>
        <br/>
        <hr>
    </div>

    <div class="clearfix"></div>

</div>
<div class="clearfix spacer"></div>

@endsection

@section('footer')

    @include('shared.footer')
    
@endsection

@section('javascript')
    <script>
    $(document).ready(function(){
        // Add Script for scrolling to contact form            
    });
    </script>
@endsection
