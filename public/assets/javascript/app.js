var app = angular.module('acesolutions', [
	'ngRoute',
	'ngSanitize',
	'ui.bootstrap',
	'angularFileUpload'
	])

.config(['$routeProvider', '$interpolateProvider', '$sceDelegateProvider', function ($routeProvider, $interpolateProvider, $sceDelegateProvider) {

    $interpolateProvider.startSymbol('{[');
    $interpolateProvider.endSymbol(']}');


    $sceDelegateProvider.resourceUrlWhitelist([
        'self'
    ]);
}]);

app.directive('ngThumb', ['$window', function($window) {
        var helper = {
            support: !!($window.FileReader && $window.CanvasRenderingContext2D),
            isFile: function(item) {
                return angular.isObject(item) && item instanceof $window.File;
            },
            isImage: function(file) {
                var type =  '|' + file.type.slice(file.type.lastIndexOf('/') + 1) + '|';
                return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
            }
        };

        return {
            restrict: 'A',
            template: '<canvas/>',
            link: function(scope, element, attributes) {
                if (!helper.support) return;

                var params = scope.$eval(attributes.ngThumb);

                if (!helper.isFile(params.file)) return;
                if (!helper.isImage(params.file)) return;

                var canvas = element.find('canvas');
                var reader = new FileReader();

                reader.onload = onLoadFile;
                reader.readAsDataURL(params.file);

                function onLoadFile(event) {
                    var img = new Image();
                    img.onload = onLoadImage;
                    img.src = event.target.result;
                }

                function onLoadImage() {
                    var width = params.width || this.width / this.height * params.height;
                    var height = params.height || this.height / this.width * params.width;
                    canvas.attr({ width: width, height: height });
                    canvas[0].getContext('2d').drawImage(this, 0, 0, width, height);
                }
            }
        };
    }]);
app.controller('adminCtrl',
	['$rootScope', '$scope', '$route', '$http', 'FileUploader',
	 function ($rootScope, $scope, $route, $http, FileUploader) {
	    
	/* *
	  * * * Initialize projects/services * * * *
	   						                  */

    // ---> Get all projects <--- //
    $http({
        method: 'GET',
        url: '/api/projects'
    }).then(function (response) {
        $scope.projects = response.data;
        //console.log($scope.projects);
    }, function (response) {
        console.log(response)
    });

    // ---> Get all services <--- //
    $http({
        method: 'GET',
        url: '/api/services'
    }).then(function (response) {
        $scope.services = response.data;
        //console.log($scope.services);
    }, function (response) {
        console.log(response)
    });

    // ---> Get all clients <--- //
    $http({
        method: 'GET',
        url: '/api/clients'
    }).then(function (response) {
        $scope.clients = response.data;
        //console.log($scope.services);
    }, function (response) {
        console.log(response)
    });

    // ---> Get all members <--- //
    $http({
        method: 'GET',
        url: '/api/members'
    }).then(function (response) {
        $scope.members = response.data;
        //console.log($scope.services);
    }, function (response) {
        console.log(response)
    });
    
    /* *
      * End * *
             */ 


    /* *
      * * * Initialize scopes * * * *
                                   * */

    $scope.project = {
        title: '',
        started_at: '',
        delivered_at: '',
    };

    
    /* *
      * End * *
             */


	/* *
	  * * * Initialize datepicker * * * *
	   						           */
	    $scope.format = 'yyyy-MM-dd';

	    $scope.project.started_at = new Date();
        $scope.project.delivered_at = new Date();

        console.log($scope.project);
		$scope.today = function() {
		    $scope.dt = new Date();
		  };
		$scope.today();

		$scope.inlineOptions = {
		    customClass: getDayClass,
		    minDate: new Date(2014, 01, 01),
		    showWeeks: false
		  };


		var tomorrow = new Date();
		  tomorrow.setDate(tomorrow.getDate() + 1);

		var afterTomorrow = new Date();
		  afterTomorrow.setDate(tomorrow.getDate() + 1);

		$scope.events = [];
		    
		function getDayClass(data) {
			var date = data.date,
			  mode = data.mode;

			if (mode === 'day') {
			  var dayToCheck = new Date(date).setHours(0,0,0,0);

			  for (var i = 0; i < $scope.events.length; i++) {
			    var currentDay = new Date($scope.events[i].date).setHours(0,0,0,0);

			    if (dayToCheck === currentDay) {
			      return $scope.events[i].status;
			    }
			  }
			}

			return '';
		}

	/* *
	  * End datepicker initialization * *
	   		                           */	


    /* * 
      * * Delete Client function * *
                                  * */
    
    $scope.deleteClient = function ($id) {

        $scope.removeClient = {
            id : $id,
            _token: $('meta[name="csrf-token"]').attr('content')
        }

        var data = $scope.removeClient;

        console.log(data);
        $http.post("/deleteUser", data).then(
            function successCallback(response) {
                console.log('deleted')
                
                $http.get('api/clients/').then(
                    function successCallback(response) {
                        $scope.clients = response.data;
                        
                }, function errorCallback(response){
                    console.log("Awww Error!" + response);
                });                
            }, //error callback
            function errorCallback(response) {
                console.log("error: " + response);
            }
        );  
    }
    
    /* *
      * End * *
             */ 


    /* * 
      * * Update Member info * *
                              * */
    
    $scope.updateMember = function ($id, $name, $role, $email, $phone) {

        $scope.member = {
            id : $id,
            name: $name,
            role: $role,
            email: $email,
            phone: $phone
        }


        //posting the user changes
        var data = $scope.member;
        data['_token'] = $('meta[name="csrf-token"]').attr('content');

        $http.post("/updateUser", data).then(
            function successCallback(response) {

                $http.get('api/members/').then(
                    function successCallback(response) {

                        $scope.members = response.data;
                        
                }, function errorCallback(response){
                    console.log("Awww Error!" + response);
                });

            }, //error callback
            function errorCallback(response) {
                console.log("error: " + response);
            }
        );
        $scope.editProject = false;  
    }
    
    /* *
      * End * *
             */ 


    /* * 
      * * Delete Client function * *
                                  * */
    
    $scope.deleteMember = function ($id) {

        $scope.removeMember = {
            id : $id,
            _token: $('meta[name="csrf-token"]').attr('content')
        }

        var data = $scope.removeMember;

        console.log(data);
        $http.post("/deleteUser", data).then(
            function successCallback(response) {
                console.log('deleted')
                
                $http.get('api/members/').then(
                    function successCallback(response) {
                        $scope.members = response.data;
                        
                }, function errorCallback(response){
                    console.log("Awww Error!" + response);
                });                
            }, //error callback
            function errorCallback(response) {
                console.log("error: " + response);
            }
        );  
    }
    
    /* *
      * End * *
             */ 
   						           
	 }]);
app.controller('blogCtrl',
	['$rootScope', '$scope', '$route', '$http', 'FileUploader', 'filterFilter', '$sce',
	 function ($rootScope, $scope, $route, $http, FileUploader, filterFilter, $sce) {

	/* *
	  * * * Initialize Posts * * *
	   						    * */

	$scope.getPosts = function () {
	    $http({
	        method: 'GET',
	        url: 'api/posts'
	    }).then(function (response) {
	        $scope.posts = response.data;
	        console.log($scope.posts);
	    }, function (response) {
	        console.log(response)
	    });		
	}

	$scope.getPosts();

	$scope.renderHTML = function(html_code) {
		return $sce.trustAsHtml(html_code);
	}

	$scope.get6Posts = function () {
	    $http({
	        method: 'GET',
	        url: 'api/6/posts'
	    }).then(function (response) {
	        $scope.posts = response.data;
	        console.log($scope.posts);
	    }, function (response) {
	        console.log(response)
	    });		
	}

	$scope.getPosts();

	/* *
	  * * * Initialize Categories * * *
	   						         * */

	$scope.getCategories = function () {
	    $http({
	        method: 'GET',
	        url: '/api/categories'
	    }).then(function (response) {
	        $scope.categories = response.data;
	        //console.log($scope.categories);
	    }, function (response) {
	        console.log(response)
	    });		
	}

	$scope.getCategories();

	/* *
	  * * * Change Categories * * */

	$scope.changeCategory = function ($id) {
	    $http({
	        method: 'GET',
	        url: '/api/posts'
	    }).then(function (response) {
	        $scope.posts = response.data;
			$scope.posts = filterFilter($scope.posts, {category_id:$id});

			console.log($scope.posts);

	        //console.log($scope.categories);
	    }, function (response) {
	        console.log(response)
	    });
	}
    /* *
      * End * *
             */ 
   						           
	 }]);
app.controller('clientCtrl',
	['$rootScope', '$scope', '$route', '$http', 'FileUploader',
	 function ($rootScope, $scope, $route, $http, FileUploader) {
	    
	/* *
	  * * * Initialize tasks * * * *
	   						      */

    $http({
        method: 'GET',
        url: '/api/tasks'
    }).then(function (response) {
        $scope.tasks = response.data;

    }, function (response) {
        console.log(response)
    });
    
    /* *
      * End * *
             */ 


	/* *
	  * * * Initialize datepicker * * * *
	   						           */
    $scope.task = {};
    $scope.format = 'yyyy-MM-dd';

    $scope.task.deadline = new Date();

	$scope.today = function() {
	    $scope.dt = new Date();
	};
	$scope.today();

	$scope.inlineOptions = {
	    customClass: getDayClass,
	    minDate: new Date(2014, 01, 01),
	    showWeeks: false
	  };


	var tomorrow = new Date();
	  tomorrow.setDate(tomorrow.getDate() + 1);

	var afterTomorrow = new Date();
	  afterTomorrow.setDate(tomorrow.getDate() + 1);

	$scope.events = [];
	    
	function getDayClass(data) {
		var date = data.date,
		  mode = data.mode;

		if (mode === 'day') {
		  var dayToCheck = new Date(date).setHours(0,0,0,0);

		  for (var i = 0; i < $scope.events.length; i++) {
		    var currentDay = new Date($scope.events[i].date).setHours(0,0,0,0);

		    if (dayToCheck === currentDay) {
		      return $scope.events[i].status;
		    }
		  }
		}

		return '';
	}

	/* *
	  * End datepicker initialization * *
	   		                           */	

    /* *
      * * * Prepare uploader * * * *
                                  * */

    $scope.openImageUploader = function () {
       
        $scope.addImage = true;

        var token = $('meta[name="csrf-token"]').attr('content');

        var uploader = $scope.uploader = new FileUploader({
            url: '/taskAttachement',
            formData: [{
                '_token': token
            }]
        });

        // FILTERS
        uploader.filters.push({
            name: 'imageFilter',
            fn: function(item /*{File|FileLikeObject}*/, options) {
                var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
                return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
            }
        });

        // CALLBACKS
        uploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/, filter, options) {
            console.info('onWhenAddingFileFailed', item, filter, options);
        };
        uploader.onAfterAddingFile = function(fileItem) {
            console.info('onAfterAddingFile', fileItem);
        };
        uploader.onAfterAddingAll = function(addedFileItems) {
            console.info('onAfterAddingAll', addedFileItems);
        };
        uploader.onBeforeUploadItem = function(item) {
            console.info('onBeforeUploadItem', item);
            //posting the user changes
        };
        uploader.onProgressItem = function(fileItem, progress) {
            console.info('onProgressItem', fileItem, progress);
        };
        uploader.onProgressAll = function(progress) {
            console.info('onProgressAll', progress);
        };
        uploader.onSuccessItem = function(fileItem, response, status, headers) {
            //console.info('onSuccessItem', fileItem, response, status, headers);

            $scope.newAttachement = undefined;
            console.info('onAfterAddingFile', fileItem);
            $scope.newAttachement = 'assets/img/tasks/tmp/'+ fileItem._file.name;
            console.log($scope.newAttachement);

        };
        uploader.onErrorItem = function(fileItem, response, status, headers) {
            console.info('onErrorItem', fileItem, response, status, headers);
        };
        uploader.onCancelItem = function(fileItem, response, status, headers) {
            console.info('onCancelItem', fileItem, response, status, headers);
        };
        uploader.onCompleteItem = function(fileItem, response, status, headers) {
            console.info('onCompleteItem', fileItem, response, status, headers);
        };
        uploader.onCompleteAll = function() {
            console.info('onCompleteAll');
        };

        console.info('uploader', uploader);
    }

    /* * *
        * * Upload files on click * */  
        
    $scope.fileChooser = function () {
        angular.element('#fileSelect').trigger('click'); //angular way
    };
    
    /* *
      * End Uploader scripts * *
                              */    
}]);
app.controller('homeCtrl',
	['$rootScope', '$scope', '$route', '$http',
	 function ($rootScope, $scope, $route, $http) {

	/* *
	  * * * Initialize projects/services * * * *
	   						                  */

    // ---> Get all projects <--- //
    $http({
        method: 'GET',
        url: '/api/projects'
    }).then(function (response) {
        $scope.projects = response.data;
        //console.log($scope.projects);
    }, function (response) {
        console.log(response)
    });

    // ---> Get all services <--- //
    $http({
        method: 'GET',
        url: '/api/services'
    }).then(function (response) {
        $scope.services = response.data;
        //console.log($scope.services);
    }, function (response) {
        console.log(response)
    });

	/* *
	  * End * *
	   		 */	

	 	$scope.showProjects = function () {

	 		if ($scope.showServicesList == true) {
	 			$scope.showServicesList = false;
	 		}

	 		$scope.showProjectsList = true;
	 	}

	 	$scope.showServices = function () {
	 		if ($scope.showProjectsList == true) {
	 			$scope.showProjectsList = false;
	 		}

	 		$scope.showServicesList = true;
	 	}

	 }]);
app.controller('missioCtrl',
	['$scope', '$http',
		function ($scope, $http){
		
	/* *
	  * * * Initialize projects/services * * * *
	   						                  */

    // ---> Get dreams <--- //

    $scope.getAllDreams = function () {
        
        $scope.dreams = [];
        
        $http({
            method: 'GET',
            url: '/api/missio/dreams'
        }).then(function (response) {
            $scope.dreams = response.data;
            //console.log($scope.projects);
        }, function (response) {
            console.log(response)
        });        
    }

    $scope.getLatestDreams = function () {
        
        $scope.dreams = [];

        $http({
            method: 'GET',
            url: '/api/missio/latest'
        }).then(function (response) {
            $scope.dreams = response.data;
            //console.log($scope.projects);
        }, function (response) {
            console.log(response)
        });
    }

    $scope.getLatestDreams();

    $scope.getRandomDreams = function () {
        
        $scope.dreams = [];

        $http({
            method: 'GET',
            url: '/api/missio/random'
        }).then(function (response) {
            $scope.dreams = response.data;
            //console.log($scope.projects);
        }, function (response) {
            console.log(response)
        });
    }


    // ---> Add dreams <--- //

	$scope.addDream = function(dream){

        //posting the user changes
        var data = dream;
        data['_token'] = $('meta[name="csrf-token"]').attr('content');
		
		console.log(data);
        
        $http.post("missio/add", data).then(
            function successCallback(response) {

                $http.get('/api/missio/dreams').then(
                    function successCallback(response) {

                        $scope.dreams = response.data;
                        
                }, function errorCallback(response){
                    console.log("Awww Error!" + response);
                });

            }, //error callback
            function errorCallback(response) {
                console.log("error: " + response);
            }
        );

	}	

}]);
app.controller('productCtrl',
	['$rootScope', '$scope', '$route', '$http', '$window', '$location',
     'FileUploader', '$filter',
	 
    function ($rootScope, $scope, $route, $http, $window, $location,
                FileUploader, $filter) {
	    
	/* *
	  * * * Initialize projects * * * *
	   						         */

    $scope.getProducts = function () {

        $http({
            method: 'GET',
            url: '/api/products'
        }).then(function (response) {
            $scope.products = response.data;

        }, function (response) {
            console.log(response)
        });
    }

    /* *
      * End * *
             */ 

	/* *
	  * * * Prepare uploader * * * *
	   						      */
    $scope.addImage = false;
	$scope.openImageUploader = function () {

        $scope.addImage = true;

        var token = $('meta[name="csrf-token"]').attr('content');
        
	    var uploader = $scope.uploader = new FileUploader({
	        url: '/uploadProductImage',
            formData: [{
                '_token': token,
            }]
	        
        }); 

	    // FILTERS
	    uploader.filters.push({
	        name: 'imageFilter',
	        fn: function(item /*{File|FileLikeObject}*/, options) {
	            var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
	            return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
	        }
	    });
	    $scope.uploader = uploader;
        // CALLBACKS
        uploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/, filter, options) {
            console.info('onWhenAddingFileFailed', item, filter, options);
        };
        uploader.onAfterAddingFile = function(fileItem) {
            console.info('onAfterAddingFile', fileItem);
        };
        uploader.onAfterAddingAll = function(addedFileItems) {
            console.info('onAfterAddingAll', addedFileItems);
        };
        uploader.onBeforeUploadItem = function(item) {
            console.info('onBeforeUploadItem', item);
            //posting the user changes
        };
        uploader.onProgressItem = function(fileItem, progress) {
            console.info('onProgressItem', fileItem, progress);
        };
        uploader.onProgressAll = function(progress) {
            console.info('onProgressAll', progress);
        };
        uploader.onSuccessItem = function(fileItem, response, status, headers) {
            console.info('onSuccessItem', fileItem, response, status, headers);

            $scope.newProductImage = undefined;
            console.info('onAfterAddingFile', fileItem);
            $scope.newProductImage = 'assets/img/products/'+ fileItem._file.name;
            console.log($scope.newProductImage);

        };
        uploader.onErrorItem = function(fileItem, response, status, headers) {
            //console.info('onErrorItem', fileItem, response, status, headers);
        };
        uploader.onCancelItem = function(fileItem, response, status, headers) {
            //console.info('onCancelItem', fileItem, response, status, headers);
        };
        uploader.onCompleteItem = function(fileItem, response, status, headers) {
            //console.info('onCompleteItem', fileItem, response, status, headers);
        };
        uploader.onCompleteAll = function() {
            //console.info('onCompleteAll');
        };

        console.info('uploader', uploader);

	}

    /* * *
        * * Upload files on click * */  
        
    $scope.fileChooser = function () {
        angular.element('#fileSelect').trigger('click'); //angular way
    };
    
	/* *
	  * End Uploader scripts * *
	   		                  */	
                              
    /* * 
      * * Add product function * *
                                 * */
                                 
    $scope.addProduct = function ($name, $type, $price, $image, $description) {

        var imageLink = '';

        if ($scope.newProductImage != undefined) {
            var imageLink = $scope.newProductImage;
        }

        $scope.product = {
            name: $name,
            type: $type,
            price: $price,
            image: imageLink,
            description: $description
        }

        //posting the user changes
        var data = $scope.product;

        data['_token'] = $('meta[name="csrf-token"]').attr('content');

        $http.post("/addProduct", data).then(
            function successCallback(response) {

                $http.get('api/products/').then(
                    function successCallback(response) {
                        $scope.addingProduct = false;
                        $window.location.reload();

                }, function errorCallback(response){
                    console.log("Awww Error!" + response);
                });

            }, //error callback
            function errorCallback(response) {
                console.log("error: " + response);
            }
        );

    }


    /* * 
      * * Save product function * *
                                 * */
                                 
    $scope.saveProduct = function ($id, $name, $type, $price, $image, $description) {

        
        var imageLink = '';
        
        if ($image != undefined) {
            imageLink = $image;
        }

        if ($scope.newProductImage != undefined) {
            imageLink = $scope.newProductImage;
        } 

        $scope.product = {
            id : $id,
            name: $name,
            type: $type,
            price: $price,
            image: imageLink,
            description: $description
        }

        //posting the user changes
        var data = $scope.product;

        data['_token'] = $('meta[name="csrf-token"]').attr('content');

        $http.post("/editProduct", data).then(
            function successCallback(response) {

                $http.get('api/products/').then(
                    function successCallback(response) {

                        $scope.editingProduct = false;
                        $window.location.reload();
                        //$scope.products = response.data;
                        
                }, function errorCallback(response){
                    console.log("Awww Error!" + response);
                });

            }, //error callback
            function errorCallback(response) {
                console.log("error: " + response);
            }
        ); 
    }



    /* * 
      * * Remove product icon * *
                               * */

    $scope.removeProductImage = function ($id, $image) {

        $scope.removeImage = {
            id : $id,
            image: $image
        }

        //posting the user changes
        var data = $scope.removeImage;
        data['_token'] = $('meta[name="csrf-token"]').attr('content');

        $http.post("/removeProductImage", data).then(
            function successCallback(response) {
                $scope.imageRemoved = true; 

            }, //error callback
            function errorCallback(response) {
                console.log("error: " + response);
            }
        );          
    }

    /* *
      * End * *
             */ 

    /* * 
      * * Delete product function * *
                                   * */
    
    $scope.deleteProduct = function ($id) {

        $scope.removeProduct = {
            id : $id,
            _token: $('meta[name="csrf-token"]').attr('content')
        }

        var data = $scope.removeProduct;

        $http.post("/deleteProduct", data).then(
            function successCallback(response) {
                
                $http.get('api/products/').then(
                    function successCallback(response) {
                        $scope.products = response.data;
                        
                }, function errorCallback(response){
                    console.log("Awww Error!" + response);
                });                
            }, //error callback
            function errorCallback(response) {
                console.log("error: " + response);
            }
        );  
    }
    /* *
      * * Add to cart * *
                       */       

    $scope.addToCart = function ($name, $price, $image) {
        $scope.hideCart = false;
        var item = {
            name: $name,
            price: $price,
            image: $image,
        }
        $price = parseInt($price);
        $scope.cart.total += $price;
        $scope.cart.count += 1;
        $scope.cart.items.push(item);

        console.log($scope.cart);
    }

    $scope.rmItem = function ($image, $price) {
        
        var item = $filter('filter')($scope.cart.items, {image:$image}, true);
        if (item.length){
            var index = $scope.cart.items.indexOf(item);
            $scope.cart.items.splice(index, 1);
            $price = parseInt($price);
            $scope.cart.total -= $price;
            $scope.cart.count -= 1;
        }
    }

    /* *
      * * Paypal API  * *
                       */ 
    $scope.pApi = {
        username: 'adm_api1.acesolutions.co',
        pass: 'MA9ZJU8FFSC6WWPY',
        sign: 'AFcWxV21C7fd0v3bYYYRCpSSRl31AT3svlcTwFfLsoF9A5YCwPfJ7lj0',
    }

    $scope.getCheckout = function() {
        
        var token = $('meta[name="csrf-token"]').attr('content');
        
        $scope.data = {
            _token: token,
            pay: $scope.cart.total,
            items: $scope.cart.items,
        }

        var data = $scope.data;

        console.log(data);

        $http.post("/getCheckout", data).then(
            function successCallback(response) {
               
            }, //error callback
            function errorCallback(response) {
                console.log("error: " + response.error);
            }
        );         
    }
    
    /* *
      * * Open app * *
                    */     

    $scope.getProducts();
    $scope.openImageUploader();
    $scope.showNecklaces = true;
    $scope.showBracelets = false;
    $scope.showAll = false;
    $scope.cart = {};
    $scope.cart.total = 0;
    $scope.cart.count = 0;
    $scope.cart.items = [];
    
    /* *
      * End * *
             */ 

	   						           
}]);
app.controller('projectCtrl',
	['$rootScope', '$scope', '$route', '$http', 'FileUploader',
	 function ($rootScope, $scope, $route, $http, FileUploader) {
	    
	/* *
	  * * * Initialize projects * * * *
	   						         */

    $scope.get6Projects = function () {
        $http({
            method: 'GET',
            url: '/api/6/projects'
        }).then(function (response) {
            $scope.projects = response.data;
            //console.log($scope.projects);
        }, function (response) {
            console.log(response)
        });
       $scope.showingAllProjects = false;
    }

    $scope.get6Projects();

    $scope.getAllProjects = function () {

        $http({
            method: 'GET',
            url: '/api/projects'
        }).then(function (response) {
            $scope.projects = response.data;
            //console.log($scope.projects);
        }, function (response) {
            console.log(response)
        });
       $scope.showingAllProjects = true;
    }
    

    /* *
      * End * *
             */ 

    $scope.addImage = false;

	/* *
	  * * * Prepare uploader * * * *
	   						      */

	$scope.openImageUploader = function () {

        $scope.addImage = true;

        var token = $('meta[name="csrf-token"]').attr('content');
        
	    var uploader = $scope.uploader = new FileUploader({
	        url: '/uploadProjectImage',
            formData: [{
                '_token': token
            }]
	        
        }); 

	    // FILTERS
	    uploader.filters.push({
	        name: 'imageFilter',
	        fn: function(item /*{File|FileLikeObject}*/, options) {
	            var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
	            return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
	        }
	    });

        // CALLBACKS
        uploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/, filter, options) {
            console.info('onWhenAddingFileFailed', item, filter, options);
        };
        uploader.onAfterAddingFile = function(fileItem) {
            console.info('onAfterAddingFile', fileItem);
        };
        uploader.onAfterAddingAll = function(addedFileItems) {
            console.info('onAfterAddingAll', addedFileItems);
        };
        uploader.onBeforeUploadItem = function(item) {
            console.info('onBeforeUploadItem', item);
            //posting the user changes
        };
        uploader.onProgressItem = function(fileItem, progress) {
            console.info('onProgressItem', fileItem, progress);
        };
        uploader.onProgressAll = function(progress) {
            console.info('onProgressAll', progress);
        };
        uploader.onSuccessItem = function(fileItem, response, status, headers) {
            console.info('onSuccessItem', fileItem, response, status, headers);

            $scope.newProjectImage = undefined;
            console.info('onAfterAddingFile', fileItem);
            $scope.newProjectImage = 'assets/img/projects/'+ fileItem._file.name;
            console.log($scope.newProjectImage);

        };
        uploader.onErrorItem = function(fileItem, response, status, headers) {
            //console.info('onErrorItem', fileItem, response, status, headers);
        };
        uploader.onCancelItem = function(fileItem, response, status, headers) {
            //console.info('onCancelItem', fileItem, response, status, headers);
        };
        uploader.onCompleteItem = function(fileItem, response, status, headers) {
            //console.info('onCompleteItem', fileItem, response, status, headers);
        };
        uploader.onCompleteAll = function() {
            //console.info('onCompleteAll');
        };

        console.info('uploader', uploader);

	}
    
    /* * *
        * * Upload files on click * */  
        
    $scope.fileChooser = function () {
        angular.element('#fileSelect').trigger('click'); //angular way
    };
    
	/* *
	  * End Uploader scripts * *
	   		                  */	
                              
    /* * 
      * * Save project function * *
                                 * */
                                 
    $scope.addProject = function ($id, $title, $summary, $image, $link, $description, $started_at, $delivered_at) {

        var imageLink = '';

        if ($scope.newProjectImage != undefined) {
            var imageLink = $scope.newProjectImage;
        }

        $scope.project = {
            title: $title,
            summary: $summary,
            image: imageLink,
            link: $link,
            description: $description,
            started_at: $started_at,
            delivered_at: $delivered_at
        }

        //posting the user changes
        var data = $scope.project;

        data['_token'] = $('meta[name="csrf-token"]').attr('content');

        $http.post("/addProject", data).then(
            function successCallback(response) {

                $http.get('api/projects/').then(
                    function successCallback(response) {
                        
                        $scope.projects = response.data;
                        
                }, function errorCallback(response){
                    console.log("Awww Error!" + response);
                });

            }, //error callback
            function errorCallback(response) {
                console.log("error: " + response);
            }
        );
        var uploader = undefined;
        $scope.editProject = false; 

    }


    /* * 
      * * Save project function * *
                                 * */
                                 
    $scope.saveProject = function ($id, $title, $summary, $image, $link, $description, $started_at, $delivered_at) {

        var imageLink = '';

        if ($scope.newProjectImage != undefined) {
            var imageLink = $scope.newProjectImage;
        }

        $scope.project = {
            id : $id,
            title: $title,
            summary: $summary,
            image: imageLink,
            link: $link,
            description: $description,
            started_at: $started_at,
            delivered_at: $delivered_at
        }

        //posting the user changes
        var data = $scope.project;

        data['_token'] = $('meta[name="csrf-token"]').attr('content');

        $http.post("/editProject", data).then(
            function successCallback(response) {

                $http.get('api/projects/').then(
                    function successCallback(response) {
                        
                        $scope.projects = response.data;
                        
                }, function errorCallback(response){
                    console.log("Awww Error!" + response);
                });

            }, //error callback
            function errorCallback(response) {
                console.log("error: " + response);
            }
        );
        var uploader = undefined;
        $scope.editProject = false; 

    }



    /* * 
      * * Remove service icon * *
                               * */

    $scope.removeProjectImage = function ($id, $image) {

        console.log('Image..');
        console.log($image);

        $scope.removeImage = {
            id : $id,
            image: $image
        }

        //posting the user changes
        var data = $scope.removeImage;
        data['_token'] = $('meta[name="csrf-token"]').attr('content');

        $http.post("/removeProjectImage", data).then(
            function successCallback(response) {
                $scope.imageRemoved = true; 

            }, //error callback
            function errorCallback(response) {
                console.log("error: " + response);
            }
        );          
    }

    /* *
      * End * *
             */ 

    /* * 
      * * Delete Project function * *
                                   * */
    
    $scope.deleteProject = function ($id) {

        $scope.removeProject = {
            id : $id,
            _token: $('meta[name="csrf-token"]').attr('content')
        }

        var data = $scope.removeProject;

        console.log(data);
        $http.post("/deleteProject", data).then(
            function successCallback(response) {
                console.log('deleted')
                
                $http.get('api/projects/').then(
                    function successCallback(response) {
                        $scope.projects = response.data;
                        
                }, function errorCallback(response){
                    console.log("Awww Error!" + response);
                });                
            }, //error callback
            function errorCallback(response) {
                console.log("error: " + response);
            }
        );  
    }
    
    /* *
      * End * *
             */ 

	   						           
	 }]);
app.controller('serviceCtrl',
	['$rootScope', '$scope', '$route', '$http', 'FileUploader',
	 function ($rootScope, $scope, $route, $http, FileUploader) {
	    
	/* *
	  * * * Initialize services * * * *
	   						         */

    $http({
        method: 'GET',
        url: '/api/services'
    }).then(function (response) {
        $scope.services = response.data;
        //console.log($scope.services);
    }, function (response) {
        console.log(response)
    });


	/* *
	  * * * Prepare uploader * * * *
	   						      * */
       
    $scope.addIcon = false;

	$scope.openIconUploader = function () {
	   
        $scope.addIcon = true;

	    var uploader = $scope.uploader = new FileUploader({
	        url: '/uploadServiceIcon'
	    });

	    // FILTERS
	    uploader.filters.push({
	        name: 'imageFilter',
	        fn: function(item /*{File|FileLikeObject}*/, options) {
	            var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
	            return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
	        }
	    });

        // CALLBACKS
        uploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/, filter, options) {
            console.info('onWhenAddingFileFailed', item, filter, options);
        };
        uploader.onAfterAddingFile = function(fileItem) {
            console.info('onAfterAddingFile', fileItem);
        };
        uploader.onAfterAddingAll = function(addedFileItems) {
            console.info('onAfterAddingAll', addedFileItems);
        };
        uploader.onBeforeUploadItem = function(item) {
            item['_token'] = $('meta[name="csrf-token"]').attr('content');
            console.info('onBeforeUploadItem', item);
            //posting the user changes
        };
        uploader.onProgressItem = function(fileItem, progress) {
            console.info('onProgressItem', fileItem, progress);
        };
        uploader.onProgressAll = function(progress) {
            console.info('onProgressAll', progress);
        };
        uploader.onSuccessItem = function(fileItem, response, status, headers) {
            //console.info('onSuccessItem', fileItem, response, status, headers);

            $scope.newServiceIcon = undefined;
            console.info('onAfterAddingFile', fileItem);
            $scope.newServiceIcon = 'assets/img/services/'+ fileItem._file.name;
            console.log($scope.newServiceIcon);

        };
        uploader.onErrorItem = function(fileItem, response, status, headers) {
            console.info('onErrorItem', fileItem, response, status, headers);
        };
        uploader.onCancelItem = function(fileItem, response, status, headers) {
            console.info('onCancelItem', fileItem, response, status, headers);
        };
        uploader.onCompleteItem = function(fileItem, response, status, headers) {
            console.info('onCompleteItem', fileItem, response, status, headers);
        };
        uploader.onCompleteAll = function() {
            console.info('onCompleteAll');
        };

        console.info('uploader', uploader);
	}

    /* * *
        * * Upload files on click * */  
        
    $scope.fileChooser = function () {
        angular.element('#fileSelect').trigger('click'); //angular way
    };
   	
	/* *
	  * End Uploader scripts * *
	   		                  */	



    /* * 
      * * Save service function * *
      						     * */

   	$scope.saveService = function ($id, $icon, $name, $description) {

   		console.log($icon);

        var iconLink = '';

   		if ($scope.newServiceIcon != undefined) {
   			var iconLink = $scope.newServiceIcon;
   		}

   		console.log('Link:');
   		console.log(iconLink);

        $scope.service = {
            id : $id,
            name: $name,
            description: $description,
            icon: iconLink
        }

        //posting the user changes
        var data = $scope.service;
        data['_token'] = $('meta[name="csrf-token"]').attr('content');

        $http.post("/editService", data).then(
            function successCallback(response) {

                $http.get('api/services/').then(
                    function successCallback(response) {
                        
                        console.log(response.data);
                        $scope.services = response.data;
                        //$scope.user.experiences = angular.copy(response.data.experiences);

                        //$scope.userChanges.experiences = angular.copy($scope.user.experiences);

                        //Clear the user changes to avoid entry repetition
                        /*if( $scope.userChanges.newExperience ){
                            delete $scope.userChanges.newExperience;
                        }*/

                        
                }, function errorCallback(response){
                    console.log("Awww Error!" + response);
                });
            }, //error callback
            function errorCallback(response) {
                console.log("error: " + response);
            }
        );
        var uploader = undefined;
        $scope.editService = false;   		
   	}

    /* * 
      * * Delete service function * *
                                   * */
    
    $scope.deleteService = function ($id) {

        $scope.removeService = {
            id : $id,
            _token: $('meta[name="csrf-token"]').attr('content')
        }

        var data = $scope.removeService;

        console.log(data);
        $http.post("/deleteService", data).then(
            function successCallback(response) {
                console.log('deleted')

                $http.get('api/services/').then(
                    function successCallback(response) {
                        
                        console.log(response.data);
                        $scope.services = response.data;
                        //$scope.user.experiences = angular.copy(response.data.experiences);

                        //$scope.userChanges.experiences = angular.copy($scope.user.experiences);

                        //Clear the user changes to avoid entry repetition
                        /*if( $scope.userChanges.newExperience ){
                            delete $scope.userChanges.newExperience;
                        }*/

                        
                }, function errorCallback(response){
                    console.log("Awww Error!" + response);
                });                
            }, //error callback
            function errorCallback(response) {
                console.log("error: " + response);
            }
        );  
    }
   	
	/* *
	  * End * *
	   		 */	


    /* * 
      * * Remove service icon * *
      						   * */

   	$scope.removeServiceIcon = function ($id, $icon) {

   		console.log('ServiceIcon:');
   		console.log($icon);

        $scope.removeIcon = {
            id : $id,
            icon: $icon
        }

        //posting the user changes
        var data = $scope.removeIcon;
        data['_token'] = $('meta[name="csrf-token"]').attr('content');

        $http.post("/removeServiceIcon", data).then(
            function successCallback(response) {
            	$scope.iconRemoved = true; 

            }, //error callback
            function errorCallback(response) {
                console.log("error: " + response);
            }
        );   		
   	}

	/* *
	  * End * *
	   		 */		   						           
	 }]);
//# sourceMappingURL=app.js.map
