<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Model::unguard();

        // ---> Commented out because Cities & Coutnries are already seeded
        //$this->call('Alakkad\WorldCountriesCities\CitiesSeeder');
        //$this->call('Alakkad\WorldCountriesCities\CountriesSeeder');
        $this->call('PostTableSeeder');
    }
}

class PostTableSeeder extends Seeder
{
  public function run()
  {
    App\Post::truncate();

    factory(App\Post::class, 20)->create();
  }
}
